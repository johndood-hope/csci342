﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Illustrating_Reflections
{
    public partial class ReflectionForm : Form
    {
        private Polyline LineToReflect { get; set; }
        private Polyline R { get; set; }
        private Polyline arrow = new Polyline();
        private Polyline reflection = null;

        private LineDrawingEventHandler _recordFirstLineHandler;
        private LineDrawingEventHandler _recordRHandler;

        private Timer t;

        public ReflectionForm()
        {
            InitializeComponent();

            setup();
        }

        private void setup()
        {
            LineToReflect = new Polyline();
            R = new Polyline();
            reflection = null;
            arrow = new Polyline();
            

            _recordFirstLineHandler = new LineDrawingEventHandler(LineToReflect);
            _recordRHandler = new LineDrawingEventHandler(R);

            _recordFirstLineHandler.Activate(canvas);
            _recordFirstLineHandler.LineCompleted += delegate
            {
                createArrow();
                _recordRHandler.Activate(canvas);
            };

            _recordRHandler.LineCompleted += delegate
            {
                createReflection();
                createAnimation();
            };
            if (t != null)
                t.Enabled = false;
        }

        private void createAnimation()
        {
            double startX1 = R.FirstPoint.X;
            double startY1 = R.FirstPoint.Y;
            double startX2 = R.LastPoint.X;
            double startY2 = R.LastPoint.Y;

            double endX2 = (LineToReflect.FirstPoint.X + LineToReflect.LastPoint.X) / 2;
            double endY2 = (LineToReflect.FirstPoint.Y + LineToReflect.LastPoint.Y) / 2;

            double endX1 = endX2 - startX2 + startX1;
            double endY1 = endY2 - startY2 + startY1;

            var animatedR = new Queue<Polyline>();
            for (int i = 0; i < 100; i++)
            {
                Polyline line = new Polyline();
                line.AddPoints(startX1 + i*(endX1 - startX1)/100, startY1 + i*(endY1 - startY1)/100,
                    startX2 + i*(endX2 - startX2)/100, startY2 + i*(endY2 - startY2)/100);
                animatedR.Enqueue(line);
            }

            t = new Timer {Interval = 10};
            t.Tick += (sender, args) =>
            {
                R = animatedR.Dequeue();
                if (animatedR.Count <= 0)
                {
                    t.Enabled = false;
                }
                canvas.Invalidate();
            };
            t.Enabled = true;

        }

        private void canvas_Resize(object sender, EventArgs e)
        {
            GL.Viewport(0, 0, canvas.Width, canvas.Height);
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            if (!canvas.IsHandleCreated) return;

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.Color3(Color.Black);

            if (LineToReflect.Count == 2)
            {
                LineToReflect.Draw(PrimitiveType.Lines);
                arrow.Draw(PrimitiveType.LineStrip);
            }

            GL.Color3(Color.Blue);

            if (R.Count == 2)
            {
                
                R.Draw(PrimitiveType.Lines);
                
                GL.Color3(Color.Red);
                reflection?.Draw(PrimitiveType.Lines);
            }

            canvas.SwapBuffers();
        }

        private void canvas_Load(object sender, EventArgs e)
        {
            if (!canvas.IsHandleCreated) return;

            Utilities.SetWindow(0, canvas.Width, 0, canvas.Height);

            GL.ClearColor(Color.White);

            DrawingTools.enableDefaultAntiAliasing();
        }

        private void createArrow()
        {
            Point2D middle = new Point2D((LineToReflect.FirstPoint.X + LineToReflect.LastPoint.X) / 2,
                                         (LineToReflect.FirstPoint.Y + LineToReflect.LastPoint.Y) / 2);
            Point2D parallelVector = new Point2D(LineToReflect.LastPoint.X - LineToReflect.FirstPoint.X,
                                                 LineToReflect.LastPoint.Y - LineToReflect.FirstPoint.Y);
            double pvmag = Math.Sqrt(
                parallelVector.X * parallelVector.X + parallelVector.Y * parallelVector.Y);
            Point2D parallel100 = new Point2D(100 * parallelVector.X / pvmag,
                                              100 * parallelVector.Y / pvmag);
            Point2D parallel5 = new Point2D(parallel100.X / 20, parallel100.Y / 20);
            Point2D perp50 = new Point2D(0 - parallel100.Y / 2, parallel100.X / 2);

            arrow.AddPoint(middle);
            arrow.AddPoint(new Point2D(middle.X + perp50.X, middle.Y + perp50.Y));
            arrow.AddPoint(middle.X + perp50.X + parallel5.X, middle.Y + perp50.Y + parallel5.Y);
            arrow.AddPoint(middle.X + 1.2 * perp50.X, middle.Y + 1.2 * perp50.Y);
            arrow.AddPoint(middle.X + perp50.X - parallel5.X, middle.Y + perp50.Y - parallel5.Y);
            arrow.AddPoint(new Point2D(middle.X + perp50.X, middle.Y + perp50.Y));
        }

        private void createReflection()
        {
            reflection = new Polyline();
            Vector2D a = new Vector2D(R.LastPoint.X - R.FirstPoint.X, R.LastPoint.Y - R.FirstPoint.Y);
            Vector2D p = new Vector2D(LineToReflect.LastPoint.X-LineToReflect.FirstPoint.X, 
                                      LineToReflect.LastPoint.Y - LineToReflect.FirstPoint.Y);
            Vector2D n = new Vector2D(-p.Y, p.X);
            Vector2D r = a + n.Scale(-2*a.Dot(n)/n.Dot(n));

            Point2D middle = new Point2D((LineToReflect.FirstPoint.X + LineToReflect.LastPoint.X) / 2,
                                         (LineToReflect.FirstPoint.Y + LineToReflect.LastPoint.Y) / 2);
            Point2D tip = r + middle;
            reflection.AddPoint(middle);
            reflection.AddPoint(tip);
            
        }

        private void canvas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 's')
            {
                setup();
                canvas.Invalidate();
            }
        }
    }
}
