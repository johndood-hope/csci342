﻿using System;
using csci342;

namespace Illustrating_Reflections
{
    public class Vector2D
    {
        public double X { get; }
        public double Y { get; }

        public Vector2D(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double Dot(Vector2D other)
        {
            return X*other.X + Y*other.Y;
        }

        public Vector2D AsUnitVector()
        {
            double mag = Math.Sqrt(X*X + Y*Y);
            return new Vector2D(X/mag, Y/mag);
        }

        public Vector2D Scale(double c)
        {
            return new Vector2D(X*c, Y*c);
        }

        public static Vector2D operator +(Vector2D v1, Vector2D v2)
        {
            return new Vector2D(v1.X+v2.X, v1.Y+v2.Y);
        }

        public static Point2D operator +(Vector2D v1, Point2D v2)
        {
            return new Point2D(v1.X + v2.X, v1.Y + v2.Y);
        }

    }
}