﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using MyGraphicsLibrary;
using OpenTK;

namespace Illustrating_Reflections
{
    
    public class LineDrawingEventHandler
    {
        public delegate void LineCompletedHandler(Polyline line);

        public event LineCompletedHandler LineCompleted;

        private Polyline line { get; set; }
        private GLControl glControl;

        public Boolean IsComplete { get; private set; }

        public LineDrawingEventHandler(Polyline _line)
        {
            line = _line;
            IsComplete = false;
        }

        public void Activate(GLControl control)
        {
            glControl = control;
            glControl.MouseClick += SelectFirstEndPointOfLine;

        }

        private void SelectFirstEndPointOfLine(object sender, MouseEventArgs e)
        {
            line.AddPoint(new Point2D(e.X, glControl.Height - e.Y));
            glControl.MouseClick -= SelectFirstEndPointOfLine;
            glControl.MouseMove += MouseMotionWhileSettingUpLine;
            glControl.Invalidate();
        }

        private void MouseMotionWhileSettingUpLine(object sender, MouseEventArgs e)
        {
            if (line.Count == 2)
            {
                line.RemoveLast();
            }
            else
            {
                //  First time we've moved the mouse after the first click, so allow the second click to occur
                glControl.MouseClick += SelectLastPointOfLine;
            }
            line.AddPoint(new Point2D(e.X, glControl.Height - e.Y));
            glControl.Invalidate();
        }

        private void SelectLastPointOfLine(object sender, MouseEventArgs e)
        {
            line.RemoveLast();
            line.AddPoint(new Point2D(e.X, glControl.Height - e.Y));

            
            glControl.MouseClick -= SelectLastPointOfLine;
            glControl.MouseMove -= MouseMotionWhileSettingUpLine;
            
            IsComplete = true;
            if (LineCompleted != null)
            {
                LineCompleted(line);
            }
            

            glControl.Invalidate();
        }
    }
}
