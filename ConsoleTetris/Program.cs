﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTetris
{
    class Program
    {
        static void Main(string[] args)
        {
            TetrisLibrary.TetrisGame game = new TetrisLibrary.TetrisGame(false);
            game.PrintBoard();
            Console.ReadLine();
            game.MoveDown();
            game.PrintBoard();
            Console.ReadLine();
            game.MoveAllDown();
            game.PrintBoard();
            Console.ReadLine();
            game.MoveDown();
            game.PrintBoard();
            Console.ReadLine();
            game.MoveAllDown();
            game.PrintBoard();
            Console.ReadLine();
            
        }
    }
}
