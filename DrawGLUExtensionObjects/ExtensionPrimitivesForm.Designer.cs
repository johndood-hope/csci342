﻿namespace DrawGLUExtensionObjects
{
    partial class ExtensionPrimitivesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpSphere = new System.Windows.Forms.GroupBox();
            this.canvas = new OpenTK.GLControl();
            this.grpLighting = new System.Windows.Forms.GroupBox();
            this.lblLightPosition = new System.Windows.Forms.Label();
            this.lightPositionX = new System.Windows.Forms.NumericUpDown();
            this.lightPositionY = new System.Windows.Forms.NumericUpDown();
            this.lightPositionZ = new System.Windows.Forms.NumericUpDown();
            this.btnSphereSolid = new System.Windows.Forms.RadioButton();
            this.btnSphereWireframe = new System.Windows.Forms.RadioButton();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lblVerticalDivisions = new System.Windows.Forms.Label();
            this.verticalDivisions = new System.Windows.Forms.NumericUpDown();
            this.horizontalDivisions = new System.Windows.Forms.NumericUpDown();
            this.lblHorizontalDivisions = new System.Windows.Forms.Label();
            this.scaleSphereZ = new System.Windows.Forms.NumericUpDown();
            this.scaleSphereY = new System.Windows.Forms.NumericUpDown();
            this.scaleSphereX = new System.Windows.Forms.NumericUpDown();
            this.lblScaleSphere = new System.Windows.Forms.Label();
            this.radius = new System.Windows.Forms.NumericUpDown();
            this.lblRadius = new System.Windows.Forms.Label();
            this.ambientBlue = new System.Windows.Forms.NumericUpDown();
            this.ambientGreen = new System.Windows.Forms.NumericUpDown();
            this.ambientRed = new System.Windows.Forms.NumericUpDown();
            this.lblAmbient = new System.Windows.Forms.Label();
            this.diffuseBlue = new System.Windows.Forms.NumericUpDown();
            this.diffuseGreen = new System.Windows.Forms.NumericUpDown();
            this.diffuseRed = new System.Windows.Forms.NumericUpDown();
            this.lblDiffuse = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.grpSphere.SuspendLayout();
            this.grpLighting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lightPositionX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightPositionY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightPositionZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.verticalDivisions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.horizontalDivisions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleSphereZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleSphereY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleSphereX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ambientBlue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ambientGreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ambientRed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diffuseBlue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diffuseGreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diffuseRed)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.grpLighting);
            this.panel1.Controls.Add(this.grpSphere);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 506);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(784, 255);
            this.panel1.TabIndex = 0;
            // 
            // grpSphere
            // 
            this.grpSphere.Controls.Add(this.radius);
            this.grpSphere.Controls.Add(this.lblRadius);
            this.grpSphere.Controls.Add(this.scaleSphereZ);
            this.grpSphere.Controls.Add(this.horizontalDivisions);
            this.grpSphere.Controls.Add(this.scaleSphereY);
            this.grpSphere.Controls.Add(this.lblHorizontalDivisions);
            this.grpSphere.Controls.Add(this.scaleSphereX);
            this.grpSphere.Controls.Add(this.lblScaleSphere);
            this.grpSphere.Controls.Add(this.verticalDivisions);
            this.grpSphere.Controls.Add(this.lblVerticalDivisions);
            this.grpSphere.Controls.Add(this.btnSphereWireframe);
            this.grpSphere.Controls.Add(this.btnSphereSolid);
            this.grpSphere.Location = new System.Drawing.Point(13, 4);
            this.grpSphere.Name = "grpSphere";
            this.grpSphere.Size = new System.Drawing.Size(288, 179);
            this.grpSphere.TabIndex = 0;
            this.grpSphere.TabStop = false;
            this.grpSphere.Text = "Sphere Parameters";
            // 
            // canvas
            // 
            this.canvas.BackColor = System.Drawing.Color.Black;
            this.canvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.canvas.Location = new System.Drawing.Point(0, 0);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(784, 506);
            this.canvas.TabIndex = 1;
            this.canvas.VSync = false;
            this.canvas.Load += new System.EventHandler(this.canvas_Load);
            this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
            this.canvas.Resize += new System.EventHandler(this.canvas_Resize);
            // 
            // grpLighting
            // 
            this.grpLighting.Controls.Add(this.diffuseBlue);
            this.grpLighting.Controls.Add(this.diffuseGreen);
            this.grpLighting.Controls.Add(this.diffuseRed);
            this.grpLighting.Controls.Add(this.lblDiffuse);
            this.grpLighting.Controls.Add(this.ambientBlue);
            this.grpLighting.Controls.Add(this.ambientGreen);
            this.grpLighting.Controls.Add(this.ambientRed);
            this.grpLighting.Controls.Add(this.lblAmbient);
            this.grpLighting.Controls.Add(this.lightPositionZ);
            this.grpLighting.Controls.Add(this.lightPositionY);
            this.grpLighting.Controls.Add(this.lightPositionX);
            this.grpLighting.Controls.Add(this.lblLightPosition);
            this.grpLighting.Location = new System.Drawing.Point(332, 7);
            this.grpLighting.Name = "grpLighting";
            this.grpLighting.Size = new System.Drawing.Size(440, 176);
            this.grpLighting.TabIndex = 1;
            this.grpLighting.TabStop = false;
            this.grpLighting.Text = "Lighting Information";
            // 
            // lblLightPosition
            // 
            this.lblLightPosition.AutoSize = true;
            this.lblLightPosition.Location = new System.Drawing.Point(7, 20);
            this.lblLightPosition.Name = "lblLightPosition";
            this.lblLightPosition.Size = new System.Drawing.Size(44, 13);
            this.lblLightPosition.TabIndex = 0;
            this.lblLightPosition.Text = "Position";
            // 
            // lightPositionX
            // 
            this.lightPositionX.DecimalPlaces = 2;
            this.lightPositionX.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.lightPositionX.Location = new System.Drawing.Point(58, 20);
            this.lightPositionX.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.lightPositionX.Name = "lightPositionX";
            this.lightPositionX.Size = new System.Drawing.Size(51, 20);
            this.lightPositionX.TabIndex = 1;
            this.lightPositionX.Value = new decimal(new int[] {
            2,
            0,
            0,
            -2147483648});
            // 
            // lightPositionY
            // 
            this.lightPositionY.DecimalPlaces = 2;
            this.lightPositionY.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.lightPositionY.Location = new System.Drawing.Point(115, 20);
            this.lightPositionY.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.lightPositionY.Name = "lightPositionY";
            this.lightPositionY.Size = new System.Drawing.Size(51, 20);
            this.lightPositionY.TabIndex = 2;
            this.lightPositionY.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // lightPositionZ
            // 
            this.lightPositionZ.DecimalPlaces = 2;
            this.lightPositionZ.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.lightPositionZ.Location = new System.Drawing.Point(172, 20);
            this.lightPositionZ.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.lightPositionZ.Name = "lightPositionZ";
            this.lightPositionZ.Size = new System.Drawing.Size(51, 20);
            this.lightPositionZ.TabIndex = 3;
            this.lightPositionZ.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // btnSphereSolid
            // 
            this.btnSphereSolid.AutoSize = true;
            this.btnSphereSolid.Checked = true;
            this.btnSphereSolid.Location = new System.Drawing.Point(7, 20);
            this.btnSphereSolid.Name = "btnSphereSolid";
            this.btnSphereSolid.Size = new System.Drawing.Size(48, 17);
            this.btnSphereSolid.TabIndex = 0;
            this.btnSphereSolid.TabStop = true;
            this.btnSphereSolid.Text = "Solid";
            this.btnSphereSolid.UseVisualStyleBackColor = true;
            // 
            // btnSphereWireframe
            // 
            this.btnSphereWireframe.AutoSize = true;
            this.btnSphereWireframe.Location = new System.Drawing.Point(61, 19);
            this.btnSphereWireframe.Name = "btnSphereWireframe";
            this.btnSphereWireframe.Size = new System.Drawing.Size(47, 17);
            this.btnSphereWireframe.TabIndex = 1;
            this.btnSphereWireframe.Text = "Wire";
            this.btnSphereWireframe.UseVisualStyleBackColor = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(332, 220);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Redraw";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lblVerticalDivisions
            // 
            this.lblVerticalDivisions.AutoSize = true;
            this.lblVerticalDivisions.Location = new System.Drawing.Point(5, 73);
            this.lblVerticalDivisions.Name = "lblVerticalDivisions";
            this.lblVerticalDivisions.Size = new System.Drawing.Size(87, 13);
            this.lblVerticalDivisions.TabIndex = 2;
            this.lblVerticalDivisions.Text = "Vertical Divisions";
            // 
            // verticalDivisions
            // 
            this.verticalDivisions.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.verticalDivisions.Location = new System.Drawing.Point(111, 71);
            this.verticalDivisions.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.verticalDivisions.Name = "verticalDivisions";
            this.verticalDivisions.Size = new System.Drawing.Size(120, 20);
            this.verticalDivisions.TabIndex = 3;
            this.verticalDivisions.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // horizontalDivisions
            // 
            this.horizontalDivisions.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.horizontalDivisions.Location = new System.Drawing.Point(111, 98);
            this.horizontalDivisions.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.horizontalDivisions.Name = "horizontalDivisions";
            this.horizontalDivisions.Size = new System.Drawing.Size(120, 20);
            this.horizontalDivisions.TabIndex = 5;
            this.horizontalDivisions.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // lblHorizontalDivisions
            // 
            this.lblHorizontalDivisions.AutoSize = true;
            this.lblHorizontalDivisions.Location = new System.Drawing.Point(5, 100);
            this.lblHorizontalDivisions.Name = "lblHorizontalDivisions";
            this.lblHorizontalDivisions.Size = new System.Drawing.Size(99, 13);
            this.lblHorizontalDivisions.TabIndex = 4;
            this.lblHorizontalDivisions.Text = "Horizontal Divisions";
            // 
            // scaleSphereZ
            // 
            this.scaleSphereZ.DecimalPlaces = 2;
            this.scaleSphereZ.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.scaleSphereZ.Location = new System.Drawing.Point(173, 127);
            this.scaleSphereZ.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.scaleSphereZ.Name = "scaleSphereZ";
            this.scaleSphereZ.Size = new System.Drawing.Size(51, 20);
            this.scaleSphereZ.TabIndex = 7;
            this.scaleSphereZ.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // scaleSphereY
            // 
            this.scaleSphereY.DecimalPlaces = 2;
            this.scaleSphereY.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.scaleSphereY.Location = new System.Drawing.Point(116, 127);
            this.scaleSphereY.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.scaleSphereY.Name = "scaleSphereY";
            this.scaleSphereY.Size = new System.Drawing.Size(51, 20);
            this.scaleSphereY.TabIndex = 6;
            this.scaleSphereY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // scaleSphereX
            // 
            this.scaleSphereX.DecimalPlaces = 2;
            this.scaleSphereX.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.scaleSphereX.Location = new System.Drawing.Point(59, 127);
            this.scaleSphereX.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.scaleSphereX.Name = "scaleSphereX";
            this.scaleSphereX.Size = new System.Drawing.Size(51, 20);
            this.scaleSphereX.TabIndex = 5;
            this.scaleSphereX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblScaleSphere
            // 
            this.lblScaleSphere.AutoSize = true;
            this.lblScaleSphere.Location = new System.Drawing.Point(2, 129);
            this.lblScaleSphere.Name = "lblScaleSphere";
            this.lblScaleSphere.Size = new System.Drawing.Size(34, 13);
            this.lblScaleSphere.TabIndex = 4;
            this.lblScaleSphere.Text = "Scale";
            // 
            // radius
            // 
            this.radius.DecimalPlaces = 2;
            this.radius.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.radius.Location = new System.Drawing.Point(61, 43);
            this.radius.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.radius.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.radius.Name = "radius";
            this.radius.Size = new System.Drawing.Size(51, 20);
            this.radius.TabIndex = 9;
            this.radius.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblRadius
            // 
            this.lblRadius.AutoSize = true;
            this.lblRadius.Location = new System.Drawing.Point(5, 45);
            this.lblRadius.Name = "lblRadius";
            this.lblRadius.Size = new System.Drawing.Size(40, 13);
            this.lblRadius.TabIndex = 8;
            this.lblRadius.Text = "Radius";
            // 
            // ambientBlue
            // 
            this.ambientBlue.DecimalPlaces = 2;
            this.ambientBlue.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.ambientBlue.Location = new System.Drawing.Point(172, 46);
            this.ambientBlue.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ambientBlue.Name = "ambientBlue";
            this.ambientBlue.Size = new System.Drawing.Size(51, 20);
            this.ambientBlue.TabIndex = 7;
            this.ambientBlue.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // ambientGreen
            // 
            this.ambientGreen.DecimalPlaces = 2;
            this.ambientGreen.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.ambientGreen.Location = new System.Drawing.Point(115, 46);
            this.ambientGreen.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ambientGreen.Name = "ambientGreen";
            this.ambientGreen.Size = new System.Drawing.Size(51, 20);
            this.ambientGreen.TabIndex = 6;
            this.ambientGreen.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // ambientRed
            // 
            this.ambientRed.DecimalPlaces = 2;
            this.ambientRed.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.ambientRed.Location = new System.Drawing.Point(58, 46);
            this.ambientRed.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ambientRed.Name = "ambientRed";
            this.ambientRed.Size = new System.Drawing.Size(51, 20);
            this.ambientRed.TabIndex = 5;
            this.ambientRed.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // lblAmbient
            // 
            this.lblAmbient.AutoSize = true;
            this.lblAmbient.Location = new System.Drawing.Point(7, 46);
            this.lblAmbient.Name = "lblAmbient";
            this.lblAmbient.Size = new System.Drawing.Size(45, 13);
            this.lblAmbient.TabIndex = 4;
            this.lblAmbient.Text = "Ambient";
            // 
            // diffuseBlue
            // 
            this.diffuseBlue.DecimalPlaces = 2;
            this.diffuseBlue.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.diffuseBlue.Location = new System.Drawing.Point(172, 72);
            this.diffuseBlue.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.diffuseBlue.Name = "diffuseBlue";
            this.diffuseBlue.Size = new System.Drawing.Size(51, 20);
            this.diffuseBlue.TabIndex = 11;
            this.diffuseBlue.Value = new decimal(new int[] {
            9,
            0,
            0,
            65536});
            // 
            // diffuseGreen
            // 
            this.diffuseGreen.DecimalPlaces = 2;
            this.diffuseGreen.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.diffuseGreen.Location = new System.Drawing.Point(115, 72);
            this.diffuseGreen.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.diffuseGreen.Name = "diffuseGreen";
            this.diffuseGreen.Size = new System.Drawing.Size(51, 20);
            this.diffuseGreen.TabIndex = 10;
            this.diffuseGreen.Value = new decimal(new int[] {
            9,
            0,
            0,
            65536});
            // 
            // diffuseRed
            // 
            this.diffuseRed.DecimalPlaces = 2;
            this.diffuseRed.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.diffuseRed.Location = new System.Drawing.Point(58, 72);
            this.diffuseRed.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.diffuseRed.Name = "diffuseRed";
            this.diffuseRed.Size = new System.Drawing.Size(51, 20);
            this.diffuseRed.TabIndex = 9;
            this.diffuseRed.Value = new decimal(new int[] {
            9,
            0,
            0,
            65536});
            // 
            // lblDiffuse
            // 
            this.lblDiffuse.AutoSize = true;
            this.lblDiffuse.Location = new System.Drawing.Point(7, 72);
            this.lblDiffuse.Name = "lblDiffuse";
            this.lblDiffuse.Size = new System.Drawing.Size(40, 13);
            this.lblDiffuse.TabIndex = 8;
            this.lblDiffuse.Text = "Diffuse";
            // 
            // ExtensionPrimitivesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 761);
            this.Controls.Add(this.canvas);
            this.Controls.Add(this.panel1);
            this.Name = "ExtensionPrimitivesForm";
            this.Text = "GLU Extension Primitives";
            this.panel1.ResumeLayout(false);
            this.grpSphere.ResumeLayout(false);
            this.grpSphere.PerformLayout();
            this.grpLighting.ResumeLayout(false);
            this.grpLighting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lightPositionX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightPositionY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lightPositionZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.verticalDivisions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.horizontalDivisions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleSphereZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleSphereY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scaleSphereX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ambientBlue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ambientGreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ambientRed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diffuseBlue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diffuseGreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diffuseRed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox grpSphere;
        private System.Windows.Forms.GroupBox grpLighting;
        private System.Windows.Forms.NumericUpDown lightPositionX;
        private System.Windows.Forms.Label lblLightPosition;
        private OpenTK.GLControl canvas;
        private System.Windows.Forms.NumericUpDown lightPositionY;
        private System.Windows.Forms.NumericUpDown lightPositionZ;
        private System.Windows.Forms.RadioButton btnSphereWireframe;
        private System.Windows.Forms.RadioButton btnSphereSolid;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.NumericUpDown horizontalDivisions;
        private System.Windows.Forms.Label lblHorizontalDivisions;
        private System.Windows.Forms.NumericUpDown verticalDivisions;
        private System.Windows.Forms.Label lblVerticalDivisions;
        private System.Windows.Forms.NumericUpDown scaleSphereZ;
        private System.Windows.Forms.NumericUpDown scaleSphereY;
        private System.Windows.Forms.NumericUpDown scaleSphereX;
        private System.Windows.Forms.Label lblScaleSphere;
        private System.Windows.Forms.NumericUpDown radius;
        private System.Windows.Forms.Label lblRadius;
        private System.Windows.Forms.NumericUpDown ambientBlue;
        private System.Windows.Forms.NumericUpDown ambientGreen;
        private System.Windows.Forms.NumericUpDown ambientRed;
        private System.Windows.Forms.Label lblAmbient;
        private System.Windows.Forms.NumericUpDown diffuseBlue;
        private System.Windows.Forms.NumericUpDown diffuseGreen;
        private System.Windows.Forms.NumericUpDown diffuseRed;
        private System.Windows.Forms.Label lblDiffuse;
    }
}

