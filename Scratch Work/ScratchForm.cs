﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using mygraphicslib;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Scratch_Work
{
    public partial class ScratchForm : Form
    {
        private bool controlLoaded;
        private double MaxT { get; set; }

        public ScratchForm()
        {
            InitializeComponent();

        }

        private void drawingArea_Load(object sender, EventArgs e)
        {
            GL.ClearColor(Color.White);

            Utilities.SetWindow(-2.2, 2.2, -1.65, 1.65);

            DrawingTools.enableDefaultAntiAliasing();

            SetViewport();

            Timer t = new Timer();
            t.Tick += (eArgs, s) =>
            {
                MaxT += 0.02;
                if (MaxT > 1)
                {
                    MaxT = 1;
                    t.Enabled = false;
                    Timer wait = new Timer();
                    wait.Tick += (eArgs2, s2) =>
                    {
                        MaxT = 0;
                        t.Enabled = true;
                        wait.Enabled = false;
                        
                    };
                    wait.Interval = 500;
                    wait.Enabled = true;
                }
                drawingArea.Invalidate();

            };
            t.Interval = 50;
            t.Enabled = true;

            controlLoaded = true;
        }

        private void SetViewport()
        {
            int width = 0, height = 0;
            if (3 * drawingArea.Width < 4 * drawingArea.Height)
            {
                width = drawingArea.Width;
                height = (3 * width) / 4;
            }
            else
            {
                height = drawingArea.Height;
                width = (4 * height) / 3;
            }
            GL.Viewport((drawingArea.Width - width) / 2, (drawingArea.Height - height) / 2, width, height);
        }

        private void drawingArea_Paint(object sender, PaintEventArgs e)
        {
            if (!controlLoaded)
                return;
            GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit);
            List<Point2D> points = new List<Point2D>
            {
                new Point2D(-1, -1),
                new Point2D(-0.5, 1),
                new Point2D(1, 0),
                new Point2D(-1.5, 0)
            };
            double r = 1300.0/drawingArea.Width/4.2*0.05;
            ICurve curve = new BezierCurve(points);
            curve.DrawConstruction(MaxT, r);
            GL.Color3(Color.Red);
            curve.DrawTo(MaxT, r);

            drawingArea.SwapBuffers();
        }

        private void drawingArea_Resize(object sender, EventArgs e)
        {
            SetViewport();
            drawingArea.Invalidate();
        }
    }
}
