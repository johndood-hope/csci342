﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tetris
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main(string[] args)
        {
            Queue<char> commands = new Queue<char>();
            bool testMode = false;
            if (args.Length >= 1)
            {
                Console.WriteLine(args[0]);
                testMode = args[0].Equals("test");
                if (args.Length >= 2)
                {
                    string text = System.IO.File.ReadAllText(args[1]);
                    foreach (char c in text)
                    {
                        commands.Enqueue(c);
                    }
                }
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new TetrisForm(commands, testMode));
        }
    }
}
