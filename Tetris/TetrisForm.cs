﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;
using TetrisLibrary;
using Utilities = MyGraphicsLibrary.Utilities;


namespace Tetris
{
    public partial class TetrisForm : Form
    {
        private TetrisGame tetrisGame;
        private bool controlLoaded = false;

        private Timer timer;
        private bool testMode;

        private bool Paused { get; set; }

        public TetrisForm(IEnumerable<char> commands, bool testMode)
        {
            InitializeComponent();
            tetrisGame = new TetrisGame(testMode);
            timer = new Timer();
            this.testMode = testMode;
            tetrisGame.LineCleared += delegate
            {
                linesClearedLabel.Text = "Lines Cleared: "+tetrisGame.LinesCleared;
            };
            foreach (char command in commands)
            {
                ProcessCommand(command);
            }
        }

        private void ProcessCommand(char command)
        {
            bool handled = true;
            switch (command)
            {
                case 'd':
                case 'j':
                    tetrisGame.MoveDown();
                    break;
                case 'D':
                    tetrisGame.MoveAllDown();
                    break;
                case 'h':
                    tetrisGame.MoveLeft();
                    break;
                case 'l':
                    tetrisGame.MoveRight();
                    break;
                case 'r':
                    tetrisGame.RotateClockwise();
                    break;
                case 'R':
                    tetrisGame.RotateCounterClockwise();
                    break;
                case 'p':
                case 'P':
                    if (!testMode)
                    {
                        timer.Enabled = Paused;
                        Paused = !Paused;
                    }
                    break;
                case ' ':
                    if (testMode)
                    {
                        tetrisGame.Reflect();
                    }
                    break;
                default:
                    handled = false;
                    break;
            }
            if(handled && controlLoaded)
                gameArea.Invalidate();
        }

        private void gameArea_Load(object sender, EventArgs e)
        {
            GL.ClearColor(Color.Black);

            Utilities.SetWindow(0, 20, 0, 20);

            DrawingTools.enableDefaultAntiAliasing();

            SetViewport();

            if (!testMode)
            {
                timer.Interval = 1000;
                timer.Tick += delegate
                {
                    tetrisGame.MoveDown();
                    gameArea.Invalidate();
                };
                tetrisGame.LineCleared += delegate
                {
                    timer.Interval = tetrisGame.LinesCleared > 50 ? 500 : 1000 - 100* (tetrisGame.LinesCleared/10);
                };
                timer.Enabled = true;
            }

            controlLoaded = true;
        }

        private void DrawMat()
        {
            GL.Color3(Color.Blue);
            Utilities.DrawRectangle(10, 10, 20, 20, true);

        }

        private void gameArea_Paint(object sender, PaintEventArgs e)
        {
            if (!controlLoaded)
                return;
            GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit);
            
            DrawMat();
            
            tetrisGame.Draw(Draw);

            gameArea.SwapBuffers();
        }

        private void gameArea_Resize(object sender, EventArgs e)
        {
            if (!controlLoaded)
                return;
            SetViewport();
            gameArea.Invalidate();
        }

        private void SetViewport()
        {
            int width = gameArea.Width;
            int height = gameArea.Height;
            if (width < height)
                height = width;
            else if (height < width)
                width = height;
            GL.Viewport((gameArea.Width - width) / 2, (gameArea.Height - height) / 2, width, height);
        }

        private void gameArea_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch(e.KeyChar)
            {
                case 'd':
                case 'j':
                case 'D':
                case 'h':
                case 'l':
                case 'r':
                case 'R':
                case 'p':
                case 'P':
                case ' ':
                    ProcessCommand(e.KeyChar);
                    break;
            }
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Up)
                ProcessCommand('R');
            if (keyData == Keys.Down)
                ProcessCommand('r');
            if (keyData == Keys.Left)
                ProcessCommand('h');
            if (keyData == Keys.Right)
                ProcessCommand('l');
            return base.ProcessDialogKey(keyData);
        }

        private void Draw(int row, int col, char c)
        {
            switch (c)
            {
                case '%':
                case '@':
                case '#':
                case '&':
                case '*':
                    DrawPiece(row, col, c);
                    break;
                case '|':
                    DrawSideBorder(row, col);
                    break;
                case '_':
                    DrawTopBorder(row, col);
                    break;
                case '-':
                    DrawBottomBorder(row, col);
                    break;
                case ' ':
                    break;
                case '.':
                    DrawGrid(row, col);
                    break;
                default:
                    GL.Color3(Color.White);
                    Utilities.DrawRectangle(col+0.5, row+0.5, 1, 1, true);
                    break;
            }
        }

        private void DrawPiece(double row, double col, char c)
        {
            Color color = Color.Green;
            switch (c)
            {
                case '#':
                    color = Color.Red;
                    break;
                case '&':
                    color = Color.Brown;
                    break;
                case '@':
                    color = Color.Purple;
                    break;
                case '%':
                    color = Color.Yellow;
                    break;
            }
            DrawPiece(row, col, color);
        }

        private void DrawPiece(double row, double col, Color color)
        {
            GL.Color3(color);
            Utilities.DrawRoundedRectangle(col + 0.5, row + 0.5, 1, 1, 0.2, true);
        }

        private void DrawGrid(double row, double col)
        {
            GL.Color3(Color.Gray);
            Utilities.DrawRectangle(col + 0.5, row + 0.5, 1, 1, true);
            GL.Color3(Color.Blue);
            Utilities.DrawRectangle(col + 0.5, row + 0.5, 0.9, 0.9, true);

        }

        private void DrawSideBorder(double row, double col)
        {
            GL.Color3(Color.Gray);
            Utilities.DrawRectangle(col + 0.5, row + 0.5, 1, 1, true);
        }

        private void DrawTopBorder(double row, double col)
        {
            GL.Color3(Color.Gray);
            Utilities.DrawArc(col,row,1,90,90,true);
            Utilities.DrawRectangle(col + 0.5, row + 0.5, 1, 1, true);
            Utilities.DrawArc(col+1, row, 1, 0, 90, true);

        }

        private void DrawBottomBorder(double row, double col)
        {
            GL.Color3(Color.Gray);
            Utilities.DrawArc(col, row+1, 1, 180, 90, true);
            Utilities.DrawRectangle(col + 0.5, row + 0.5, 1, 1, true);
            Utilities.DrawArc(col + 1, row+1, 1, 270, 90, true);
        }
    }
}
