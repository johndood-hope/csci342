﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using MyGraphicsLibrary;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Cyrus_Beck_Illustration
{
    public partial class CyrusBeckForm : Form
    {
        private Polyline shape = new Polyline();
        private Polyline lineToClip = new Polyline();
        private Polyline clippedLine;
        private bool controlLoaded;
        private LineDrawingEventHandler linedrawing = new LineDrawingEventHandler();

        public CyrusBeckForm()
        {
            InitializeComponent();
            shape.AddPoints(.5, .866, -.5, .866, -1, 0, -.5, -.866, .5, -.866, 1, 0, .5, .866);
            lineToClip.AddPoints(1.5, -.5, -1.5, -.5);
            linedrawing.Activate(plottingArea);
            linedrawing.LineCompleted += (A, D) =>
            {
                lineToClip = new Polyline();
                double awx = (A.X/plottingArea.Width)*4 - 2;
                double awy = (A.Y/plottingArea.Height)*4 -2;
                double dwx = (D.X / plottingArea.Width) * 4 - 2;
                double dwy = (D.Y / plottingArea.Height) * 4 - 2;
                lineToClip.AddPoints(awx, awy, dwx, dwy);
                clippedLine = CBUtilities.Clip(shape, lineToClip);
            };
            clippedLine = CBUtilities.Clip(shape, lineToClip);
      
        }

        private void plottingArea_Load(object sender, EventArgs e)
        {
            controlLoaded = true;
            GL.ClearColor(Color.White);
            GL.Color3(Color.Black);

            Utilities.SetWindow(-2, 2, -2, 2);

            GL.PointSize(5);
            SetViewport();
        }

        private void SetViewport()
        {
            int min = plottingArea.Width < plottingArea.Height ? plottingArea.Width : plottingArea.Height;

            GL.Viewport((plottingArea.Width - min) / 2, (plottingArea.Height - min) / 2, min, min);
        }

        private void plottingArea_Paint(object sender, PaintEventArgs e)
        {
            if (!controlLoaded)
                return;
            GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit);
            GL.Color3(Color.Gray);
            lineToClip.Draw(PrimitiveType.LineStrip);

            GL.Color3(Color.Black);

            shape.Draw(PrimitiveType.LineStrip);

            GL.Color3(Color.Red);

            clippedLine?.Draw(PrimitiveType.LineStrip);

            plottingArea.SwapBuffers();
        }

        private void plottingArea_Resize(object sender, EventArgs e)
        {
            SetViewport();
            plottingArea.Invalidate();
        }
    }
}
