﻿namespace Cyrus_Beck_Illustration
{
    partial class CyrusBeckForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.plottingArea = new OpenTK.GLControl();
            this.SuspendLayout();
            // 
            // plottingArea
            // 
            this.plottingArea.BackColor = System.Drawing.Color.Black;
            this.plottingArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plottingArea.Location = new System.Drawing.Point(0, 0);
            this.plottingArea.Name = "plottingArea";
            this.plottingArea.Size = new System.Drawing.Size(284, 261);
            this.plottingArea.TabIndex = 0;
            this.plottingArea.VSync = false;
            this.plottingArea.Load += new System.EventHandler(this.plottingArea_Load);
            this.plottingArea.Paint += new System.Windows.Forms.PaintEventHandler(this.plottingArea_Paint);
            this.plottingArea.Resize += new System.EventHandler(this.plottingArea_Resize);
            // 
            // CyrusBeckForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.plottingArea);
            this.Name = "CyrusBeckForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private OpenTK.GLControl plottingArea;
    }
}

