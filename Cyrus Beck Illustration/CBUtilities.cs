﻿using System;
using System.Linq;
using System.Windows.Forms;
using csci342;
using MyGraphicsLibrary;

namespace Cyrus_Beck_Illustration
{
    public static class CBUtilities
    {
        public static Polyline Clip(Polyline shape, Polyline lineToClip)
        {
            double max_enter = 0;
            double min_exit = 1;
            Vector3 c = new Vector3(lineToClip.LastPoint.X - lineToClip.FirstPoint.X, lineToClip.LastPoint.Y - lineToClip.FirstPoint.Y,0);

            for (int line = 0; line < shape.Count - 1; line++)
            {
                Vector3 normal = new Vector3();
                Point2D firstPoint = shape.ElementAt(line);
                Point2D lastPoint = shape.ElementAt(line + 1);


                Vector3 bminusA = new Vector3(firstPoint.X - lineToClip.FirstPoint.X, firstPoint.Y - lineToClip.FirstPoint.Y, 0);

                double linex = lastPoint.X - firstPoint.X;
                double liney = lastPoint.Y - firstPoint.Y;
                normal.Y = -1 * linex;
                normal.X = liney;
                double numerator = normal.Dot(bminusA);
                double denomenator = normal.Dot(c);
                if (denomenator == 0)
                {
                    double theta = Math.Acos(numerator/bminusA.Length/normal.Length);
                    if (theta > Math.PI/2)
                    {
                        return null;
                    }
                }
                else
                {
                    double thit = numerator / denomenator;
                    if (denomenator < 0)
                    {
                        //Entering code
                        if (max_enter < thit)
                            max_enter = thit;
                    }
                    else
                    {
                        //Exiting code
                        if (thit < min_exit)
                            min_exit = thit;
                    }
                }
   
            }
            Polyline toReturn = new Polyline();
            toReturn.AddPoints(lineToClip.FirstPoint.X + c.X * max_enter, lineToClip.FirstPoint.Y + c.Y * max_enter);
            toReturn.AddPoints(lineToClip.FirstPoint.X + c.X * min_exit, lineToClip.FirstPoint.Y + c.Y * min_exit);
            return toReturn;
        }
    }
}