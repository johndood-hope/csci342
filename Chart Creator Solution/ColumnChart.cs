﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using csci342.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Chart_Creator
{
    public class ColumnChart : AbstractChart
    {

        public const char TypeSpecifier = 'c';

        public override char ChartTypeSpecifier => TypeSpecifier;


        public class Series
        {
            public string Name { get; }

            public readonly ColumnChart Container;

            public readonly int ColorIndex;
            public int Count;

            public Color3 Color
            {
                get
                {
                    byte r = Container.Colors[ColorIndex, 0];
                    byte g = Container.Colors[ColorIndex, 1];
                    byte b = Container.Colors[ColorIndex, 2];
                    return new Color3(r, g, b);
                }
            }

            public Series(ColumnChart container, string name, int colorIndex)
            {
                Container = container;
                Name = name;
                ColorIndex = colorIndex;
            }

            private readonly IDictionary<string, double> points = new Dictionary<string, double>();

            public double this[string category]
            {
                get { return points[category]; }
                set { points[category] = value; }
            }
        }

        private IList<Series> SeriesList { get; } = new List<Series>();

        private IList<string> Categories { get; } = new List<string>();

        public override ICollection<string> LegendEntries => SeriesList.Select(series => series.Name).ToList();

        private const double MinimumValue = 0;

        private double MaximumValue { get; set; }

        private double RangeValue => MaximumValue - MinimumValue;

        public override CoordinateSystem World => new CoordinateSystem(
                (0 - 0.05) * Categories.Count,
                (1 + 0.05) * Categories.Count,
                MinimumValue - 0.05 * RangeValue,
                MaximumValue + 0.05 * RangeValue
            );

        private Series selectedSeries;
        private const int TickLengthPixels = 10;

        public override void ClearSelection()
        {
            selectedSeries = null;
        }

        public override void DoExplosion(Selection exploded, double dwx, double dwy)
        {
            // Does not explode
        }

        public override void Draw(GLControl c)
        {
            DrawChartAxes();
            
            GL.PushMatrix();
            foreach (string category in Categories)
            {
                GL.PushMatrix();
                GL.Scale(1.0/(SeriesList.Count+1), 1, 1);
                GL.Translate(0.5, 0, 0);
                foreach (var series in SeriesList)
                {
                    var color = series.Color;
                    GL.Color3(color.Red, color.Green, color.Blue);
                    GL.Rect(0, 0, 1, series[category]);
                    GL.Translate(1, 0, 0);
                }
                GL.PopMatrix();
                GL.Translate(1, 0, 0);
            }
            GL.PopMatrix();
            double x = 0, dx = 1.0 / (SeriesList.Count + 1);
            foreach (string category in Categories)
            {
                x += dx/2;
                foreach (var series in SeriesList)
                {
                    if (series.Equals(selectedSeries))
                    {
                        DrawSelectionHandle(x, 0);
                        DrawSelectionHandle(x+dx, 0);
                        DrawSelectionHandle(x, series[category]);
                        DrawSelectionHandle(x+dx, series[category]);
                    }
                    x += dx;
                }
                x += dx/2;
            }


        }

        private void DrawSelectionHandle(double x, double y)
        {
            var viewport = new int[4];
            GL.GetInteger(GetPName.Viewport, viewport);
            double width = viewport[2], height = viewport[3];
            GL.PushMatrix();
            {
                GL.Translate(x, y, 0);
                GL.Scale(1, 1, 1);
                GL.Scale(World.Width, World.Height, 1);
                
                double max = Math.Max(width, height);
                GL.Scale(height/max, width/max, 1);
                
                GL.Scale(1/2.4, 1/2.4, 1);
                ChartCreator.DrawSelectionHandle(0, 0);
            }
            GL.PopMatrix();
        }

        private void DrawChartAxes()
        {
            GL.Color3(Color.Black);
            GL.Begin(PrimitiveType.Lines);
            {
                //  Category Axis
                GL.Vertex2(0, MinimumValue);
                GL.Vertex2(Categories.Count, MinimumValue);

                //  Primary Value Axis
                GL.Vertex2(0, MinimumValue);
                GL.Vertex2(0, MaximumValue);

                //  Secondary Value Axis
                GL.Vertex2(Categories.Count, MinimumValue);
                GL.Vertex2(Categories.Count, MaximumValue);

                //  Top
                GL.Vertex2(0, MaximumValue);
                GL.Vertex2(Categories.Count, MaximumValue);
            }
            GL.End();

            double tickLength = TickLengthPixels * RangeValue * 1.0 / Viewport.Height;
            var world = World;
            var viewport = new int[4];
            GL.GetInteger(GetPName.Viewport, viewport);

            //  Horizontal tick marks
            for (int i = 0; i <= Categories.Count; i++)
            {
                GL.Begin(PrimitiveType.Lines);
                {
                    double x = i;
                    GL.Vertex2(x, MinimumValue);
                    GL.Vertex2(x, MinimumValue + tickLength);
                }
                GL.End();
                if (i == Categories.Count)
                    continue;
                GL.PushMatrix();
                {
                    var textSize = FontGenerator.GetSize(Categories[i]);
                    GL.Translate(i+0.5,world.Bottom,0);
                    GL.Scale(world.Width/viewport[2], world.Height/viewport[3], 1);
                    GL.Translate(textSize.Width/-2.0, 0, 1);
                    TextDrawer.DrawString(Categories[i], 0, 0, ChartCreator.FontID);
                }
                GL.PopMatrix();
            }


            //  Vertical tick marks            
            double increment = RangeValue / 10;
            tickLength = TickLengthPixels * Categories.Count * 1.0 / Viewport.Width;

            GL.Begin(PrimitiveType.Lines);
            {
                for (int i = 1; i <= 10 + 1; i++)
                {
                    GL.Vertex2(-tickLength, MinimumValue + i * increment);
                    GL.Vertex2(0, MinimumValue + i * increment);
                }
            }
            GL.End();
        }

        public override void DrawLegendMarker(string seriesName, double x, double y)
        {
            GL.Rect((int)x, (int)y, (int)x + ChartCreator.BoxWidth, (int)y + ChartCreator.BoxHeight);
        }

        public override Color3 GetLegendColor(string item)
            => SeriesList.First(series => series.Name.Equals(item)).Color;

        public override Selection GetSelection(string seriesName)
        {
            selectedSeries = SeriesList.First(series => series.Name.Equals(seriesName));
            return new ColumnSeriesSelection(selectedSeries);
        }

        public override Selection GetSelection(double wx, double wy)
        {
            int category = (int) wx;
            wx = wx - category;
            wx -= 0.5/(SeriesList.Count+1);
            wx *= (SeriesList.Count + 1);
            int series = (int) wx;
            if (series >= SeriesList.Count || wy < 0 || wy > SeriesList[series][Categories[category]])
            {
                selectedSeries = null;
                return null;
            }
            selectedSeries = SeriesList[series];
            return new ColumnSeriesSelection(selectedSeries);

        }

        public override void ReadFile(string firstLine, StreamReader reader)
        {
            string line = firstLine;
            while (!(line = line.Trim()).Equals(""))
            {
                Categories.Add(line);
                line = reader.ReadLine();
                if (line == null)
                {
                    throw new FormatException("Expected category but reached end of file");
                }
            }
            int colorIndex = 0;
            while ((line = reader.ReadLine()) != null)
            {
                line = line.Trim();
                if(line.Equals(""))
                    continue;
                if(!line.StartsWith("DS,"))
                    throw new FormatException($"Expected beginning of new data series but found {line}");
                line = line.Substring(3).Trim();
                var series = new Series(this, line, colorIndex++);
                SeriesList.Add(series);
                foreach (string category in Categories)
                {
                    if ((line = reader.ReadLine()) == null)
                    {
                        throw new FormatException($"Expected entry for {category} in {series.Name}");
                    }
                    line = line.Trim();
                    try
                    {
                        double value = series[category] = double.Parse(line);

                        MaximumValue = Math.Max(MaximumValue, value);
                    }
                    catch (FormatException)
                    {
                        throw new FormatException($"Could not read number {line} for category {category} in series {series.Name}");
                    }
                }
            }
        }

        public override void SaveFile(StreamWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}