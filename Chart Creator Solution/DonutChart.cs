﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using ArcDrawer;
using csci342;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Chart_Creator
{
    [SuppressMessage("ReSharper", "StringLastIndexOfIsCultureSpecific.1")]
    public class DonutChart : AbstractChart
    {

        public virtual double InnerRadius => 0.7;

        private Dictionary<string, SliceInfo> items;
        private string selectedItem;

        private string ExplodedSlice { get; set; }
        private double explosionRadius = 0.15;

        public const char TypeSpecifier = 'd';

        public override char ChartTypeSpecifier => TypeSpecifier;

        public DonutChart()
        {
            items = new Dictionary<string, SliceInfo>();
            selectedItem = null;
        }

        public class SliceInfo
        {
            public DonutChart Chart;

            public int ColorIndex;
            public int Count;

            public Color3 Color
            {
                get
                {
                    byte r = Chart.Colors[ColorIndex, 0];
                    byte g = Chart.Colors[ColorIndex, 1];
                    byte b = Chart.Colors[ColorIndex, 2];
                    return new Color3(r, g, b);
                }
            }
            public double BeginAngle;
            public double EndAngle;

            public double Sweep => EndAngle - BeginAngle;

            public double BeginRadians => BeginAngle * Math.PI / 180.0;
            public double EndRadians => EndAngle * Math.PI / 180.0;
            public double MidRadians => (BeginRadians + EndRadians) / 2;


            public SliceInfo(DonutChart chart)
            {
                Chart = chart;
                Count = 0;
            }
        }

        public override void ReadFile(string firstLine, StreamReader reader)
        {
            int totalItems = 0;
            if (firstLine.Length >= 14 && firstLine.Substring(0, 14).Equals("Explode slice "))
            {
                int till = firstLine.LastIndexOf(" out by ");
                if (till == -1)
                    throw new FormatException("Could not find end of exploded category name");
                ExplodedSlice = firstLine.Substring(14, till - 14).Trim();
                if (!double.TryParse(firstLine.Substring(till + 8), out explosionRadius))
                {
                    throw new FormatException("Could not read explosion radius");
                }
                firstLine = reader.ReadLine();
                if (firstLine == null)
                    throw new FormatException("Expected category after explosion radius");
            }
            string line = firstLine;

            while (line != null)
            {
                line = line.Trim();
                SliceInfo sliceInfo;
                if (items.ContainsKey(line))
                {
                    sliceInfo = items[line];
                }
                else
                {
                    sliceInfo = new SliceInfo(this);
                    items[line] = sliceInfo;

                    if (items.Keys.Count > Colors.GetLength(0))
                    {
                        throw new FormatException($"Too many values specified for pie chart; limit is {Colors.GetLength(0)}");
                    }
                }

                sliceInfo.Count++;
                totalItems++;

                line = reader.ReadLine();
            }

            if (ExplodedSlice != null && !items.ContainsKey(ExplodedSlice))
                throw new FormatException("Could not find exploded slice in list of categories");

            double beginAngle = 0;
            int i = 0;

            foreach (var key in items.Keys)
            {
                SliceInfo slice = items[key];
                double percentage = 1.0 * slice.Count / totalItems;
                slice.BeginAngle = beginAngle;
                slice.EndAngle = beginAngle + percentage * 360;
                slice.ColorIndex = i;
                beginAngle = slice.EndAngle;
                i++;
            }
        }

        public override void SaveFile(StreamWriter writer)
        {
            if (ExplodedSlice != null)
            {
                writer.WriteLine($"Explode slice {ExplodedSlice} out by {explosionRadius}");
            }
            foreach (var sliceInfo in items)
            {
                var text = sliceInfo.Key;
                var data = sliceInfo.Value;
                for (int i = 0; i < data.Count; i++)
                {
                    writer.WriteLine(text);
                }
            }
        }

        public override void Draw(GLControl c)
        {
            foreach (string item in items.Keys)
            {
                var slice = items[item];

                GL.Color3(slice.Color.Red, slice.Color.Green, slice.Color.Blue);

                GL.PushMatrix();
                {
                    if (item.Equals(ExplodedSlice))
                    {
                        GL.Translate(explosionRadius * Math.Cos(slice.MidRadians), explosionRadius * Math.Sin(slice.MidRadians),
                            0);
                    }


                    GL.Begin(PrimitiveType.Polygon);
                    {
                        GL.Vertex2(0.0, 0.0);
                        Arc.DrawArc(0, 0, 1.0, slice.BeginAngle, slice.Sweep);
                    }
                    GL.End();

                    GL.Color3(Color.White);
                    GL.Begin(PrimitiveType.Polygon);
                    {
                        GL.Vertex2(0.0, 0.0);
                        Arc.DrawArc(0, 0, InnerRadius, slice.BeginAngle, slice.Sweep);
                    }
                    GL.End();

                    Utilities.SetForegroundBlack();
                    GL.Begin(PrimitiveType.LineLoop);
                    {
                        Arc.DrawArc(0, 0, 1.0, slice.BeginAngle, slice.Sweep);
                        Arc.DrawArc(0, 0, InnerRadius, slice.EndAngle, -slice.Sweep);
                    }
                    GL.End();
                }
                GL.PopMatrix();
            }

            if (selectedItem != null)
            {
                GL.PushMatrix();
                {
                    var slice = items[selectedItem];
                    if (selectedItem.Equals(ExplodedSlice))
                    {
                        GL.Translate(explosionRadius * Math.Cos(slice.MidRadians), explosionRadius * Math.Sin(slice.MidRadians),
                            0);
                    }
                    ChartCreator.DrawSelectionHandle(InnerRadius * Math.Cos(slice.BeginRadians), InnerRadius * Math.Sin(slice.BeginRadians));
                    ChartCreator.DrawSelectionHandle(InnerRadius * Math.Cos(slice.EndRadians), InnerRadius * Math.Sin(slice.EndRadians));
                    ChartCreator.DrawSelectionHandle(Math.Cos(slice.BeginRadians), Math.Sin(slice.BeginRadians));
                    ChartCreator.DrawSelectionHandle(Math.Cos(slice.EndRadians), Math.Sin(slice.EndRadians));
                }
                GL.PopMatrix();
            }
        }

        public override ICollection<string> LegendEntries => items.Keys;

        public override Color3 GetLegendColor(string item)
        {
            return items[item].Color;
        }

        public override void ClearSelection()
        {
            selectedItem = null;
        }

        public override Selection GetSelection(string seriesName)
        {
            var item = items[seriesName];
            if (item == null)
            {
                selectedItem = null;
                return null;
            }
            selectedItem = seriesName;
            return new SliceSelection(seriesName, item);

        }

        public override Selection GetSelection(double wx, double wy)
        {
            double r = Math.Sqrt(wx * wx + wy * wy);
            double theta = Math.Atan2(wy, wx) * (180 / Math.PI) + (wy < 0 ? 360 : 0);

            foreach (string key in items.Keys)
            {
                // check radius with explosion in mind
                double emod = key.Equals(ExplodedSlice) ? explosionRadius : 0;
                if (r < emod + InnerRadius+0.05 || r > 1 + emod)
                    continue;
                var slice = items[key];
                if (slice.BeginAngle <= theta && slice.EndAngle >= theta)
                {
                    selectedItem = key;
                    return new SliceSelection(selectedItem, items[selectedItem]);
                }
            }


            selectedItem = null;
            return null;
        }

        public override CoordinateSystem World => new CoordinateSystem(-1.2, 1.2, -1.2, 1.2);

        public override Rectangle GetViewport(double widthAvailable, double heightAvailable)
        {
            Rectangle chartViewport = new Rectangle();
            chartViewport.Location = new Point(0, 0);

            if (widthAvailable > heightAvailable)
            {
                chartViewport.Height = (int)heightAvailable;
                chartViewport.Width = (int)heightAvailable;
            }
            else
            {
                chartViewport.Width = (int)widthAvailable;
                chartViewport.Height = (int)widthAvailable;
            }
            return chartViewport;
        }

        public override void DrawLegendMarker(string seriesName, double x, double y)
        {
            GL.Rect((int)x, (int)y, (int)x + ChartCreator.BoxWidth, (int)y + ChartCreator.BoxHeight);
        }

        public override void DoExplosion(Selection exploded, double wdx, double wdy)
        {
            ExplodedSlice = exploded.Name;
            double midRadians = items[ExplodedSlice].MidRadians;
            double dirR = wdx * Math.Cos(midRadians) + wdy * Math.Sin(midRadians);
            explosionRadius = Math.Min(Math.Max(dirR, 0), 0.2);
        }

    }
}
