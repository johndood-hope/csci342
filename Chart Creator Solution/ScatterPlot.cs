﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using ArcDrawer;
using csci342;
using csci342.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Boolean = System.Boolean;
using Vector3 = csci342.Vector3;

namespace Chart_Creator
{
    public class ScatterPlot : AbstractChart
    {
        enum Axis
        {
            Primary, Secondary, Category
        };

        public delegate void MarkerDrawer(double x, double y);

        private class DataSeries
        {
            public readonly ScatterPlot Chart;

            public int ColorIndex { get; }

            public string Name { get; }

            public Color3 Color
            {
                get
                {
                    byte red = Chart.Colors[ColorIndex, 0];
                    byte green = Chart.Colors[ColorIndex, 1];
                    byte blue = Chart.Colors[ColorIndex, 2];
                    return new Color3(red, green, blue);
                }
            }


            public Axis Axis { get; }
            public readonly SortedList<double, double> points;
            public Marker Marker { get; set; }

            public IList<double> X => points.Keys;

            public double this[double x] => points[x];

            public DataSeries(ScatterPlot chart, string name, int colorIndex, Axis axis)
            {
                Chart = chart;
                Name = name;
                ColorIndex = colorIndex;
                Axis = axis;

                points = new SortedList<double, double>();
            }

            public void AddPoint(double x, double y)
            {
                points.Add(x, y);
            }

        }

        private class Interval
        {
            public Interval()
            {
                Minimum = double.MaxValue;
                Maximum = double.MinValue;
            }

            public double Minimum { get; private set; }
            public double Maximum { get; private set; }

            public double Range => Maximum - Minimum;

            public void Check(double d)
            {
                Minimum = Math.Min(Minimum, d);
                Maximum = Math.Max(Maximum, d);
            }
        }

        private class DataSeriesSelection : Selection
        {
            private readonly DataSeries series;

            public string Name => series.Name;

            public DataSeriesSelection(DataSeries selectedSeries)
            {
                if (selectedSeries == null)
                    throw new NullReferenceException("Cannot select a null series!");
                series = selectedSeries;
            }

            public bool CanChangeColor => true;

            public bool CanChangeFillColor => false;

            public void ChangeColor(byte r, byte g, byte b)
            {
                series.Chart.Colors[series.ColorIndex, 0] = r;
                series.Chart.Colors[series.ColorIndex, 1] = g;
                series.Chart.Colors[series.ColorIndex, 2] = b;
            }

            public void ChangeFillColor(byte r, byte g, byte b)
            {
                throw new NotImplementedException();
            }

            public Color3 CurrentColor => series.Color;

            public Color3 CurrentFillColor
            {
                get { throw new NotImplementedException(); }
            }

            public Action DefaultAction => Action.ChangeSelectionColor;

            public override bool Equals(object toCompareTo) => (toCompareTo as DataSeriesSelection)?.series.Equals(series) ?? false;

            public override int GetHashCode() => series.GetHashCode();

            public bool CanExplode => false;
        }

        static ScatterPlot()
        {
            GL.Enable(EnableCap.LineStipple);
            GL.LineStipple(1, 0xffff);
        }

        private readonly Dictionary<string, DataSeries> dataSeries;
        private readonly Dictionary<Axis, Interval> axisBounds;

        private const int NumberOfTicks = 10;
        private const double TickLengthPixels = 10;
        private const int MarkerSizePixels = 6;

        private DataSeries selectedSeries;

        public const char TypeSpecifier = 'l';

        public override char ChartTypeSpecifier => TypeSpecifier;

        #region Markers
        class Marker
        {
            public Marker(MarkerDrawer drawer, PrimitiveType defaultPrimitive = PrimitiveType.Polygon, PrimitiveType selectedPrimitive = PrimitiveType.LineLoop)
            {
                Drawer = drawer;
                DefaultPrimitive = defaultPrimitive;
                SelectedPrimitive = selectedPrimitive;
            }

            public MarkerDrawer Drawer { get; set; }
            public PrimitiveType DefaultPrimitive { get; set; }
            public PrimitiveType SelectedPrimitive { get; set; }
        }

        public static void DrawCircle(double x, double y)
        {
            Arc.DrawArc(x, y, MarkerSizePixels, 0, 360);
        }

        public static void DrawRectangle(double x, double y)
        {
            GL.Vertex2(x - MarkerSizePixels, y - MarkerSizePixels);
            GL.Vertex2(x + MarkerSizePixels, y - MarkerSizePixels);
            GL.Vertex2(x + MarkerSizePixels, y + MarkerSizePixels);
            GL.Vertex2(x - MarkerSizePixels, y + MarkerSizePixels);
        }

        public static void DrawDiamond(double x, double y)
        {
            GL.Vertex2(x, y + MarkerSizePixels);
            GL.Vertex2(x - MarkerSizePixels, y);
            GL.Vertex2(x, y - MarkerSizePixels);
            GL.Vertex2(x + MarkerSizePixels, y);
        }

        public static void drawPlus(double x, double y)
        {
            GL.Vertex2(x - MarkerSizePixels, y);
            GL.Vertex2(x + MarkerSizePixels, y);

            GL.Vertex2(x, y - MarkerSizePixels);
            GL.Vertex2(x, y + MarkerSizePixels);
        }

        public static void drawX(double x, double y)
        {
            GL.Vertex2(x - MarkerSizePixels, y + MarkerSizePixels);
            GL.Vertex2(x + MarkerSizePixels, y - MarkerSizePixels);

            GL.Vertex2(x + MarkerSizePixels, y + MarkerSizePixels);
            GL.Vertex2(x - MarkerSizePixels, y - MarkerSizePixels);
        }

        public static void drawTriangle(double x, double y)
        {
            double r = MarkerSizePixels;

            double oneTwentyDegreesInRadians = Utilities.DegreesToRadians(120);
            double ninetyDegreesInRadians = Math.PI / 2;
            GL.Vertex2(x + r * Math.Cos(ninetyDegreesInRadians), y + r * Math.Sin(ninetyDegreesInRadians));
            GL.Vertex2(x + r * Math.Cos(oneTwentyDegreesInRadians + ninetyDegreesInRadians), y + r * Math.Sin(oneTwentyDegreesInRadians + ninetyDegreesInRadians));
            GL.Vertex2(x + r * Math.Cos(2 * oneTwentyDegreesInRadians + ninetyDegreesInRadians), y + r * Math.Sin(2 * oneTwentyDegreesInRadians + ninetyDegreesInRadians));
        }

        Marker[] markers =
        {
            new Marker(new MarkerDrawer(DrawCircle)),
            new Marker(new MarkerDrawer(DrawDiamond)),
            new Marker(new MarkerDrawer(drawTriangle)),
            new Marker(new MarkerDrawer(DrawRectangle)),
            new Marker(new MarkerDrawer(drawX), PrimitiveType.Lines, PrimitiveType.Lines),
            new Marker(new MarkerDrawer(drawPlus), PrimitiveType.Lines, PrimitiveType.Lines)

        };

        #endregion

        public ScatterPlot()
        {
            dataSeries = new Dictionary<string, DataSeries>();
            axisBounds = new Dictionary<Axis, Interval>();
            axisBounds[Axis.Primary] = new Interval();
            axisBounds[Axis.Secondary] = new Interval();
            axisBounds[Axis.Category] = new Interval();
        }

        #region Open and Save file
        public override void ReadFile(string firstLine, StreamReader reader)
        {
            char[] delimiters = { ',' };
            int dataSeriesIndex = 0;
            string currentLine = firstLine;

            while (currentLine != null && !reader.EndOfStream)
            {
                var tokens = currentLine.Split(delimiters);

                if (tokens.Length != 3)
                {
                    throw new FormatException("Data series description must be in format DS,Name,[p|s]");
                }

                if (tokens[0] != "DS")
                {
                    throw new FormatException($"First portion of data series specification must be DS (was {tokens[0]})");
                }

                Axis axis;

                try
                {
                    axis = (Axis)Enum.Parse(typeof(Axis), tokens[2]);
                }
                catch (ArgumentException)
                {
                    throw new FormatException(
                        $"Invalid value for axis; must be either p (primary) or s (secondary); was {tokens[2]}");
                }

                string dataseriesName = tokens[1];
                var series = new DataSeries(this, dataseriesName, dataSeriesIndex, axis)
                {
                    Marker = markers[dataSeriesIndex % markers.Length]
                };

                //  Re-use markers if more data series than markers are available

                dataSeries.Add(dataseriesName, series);

                //  Read the points in the series
                bool done = false;
                while (!done)
                {
                    string pointAsString = (reader.ReadLine() ?? "").Trim();
                    if (pointAsString != "-1")
                    {
                        var components = pointAsString.Split(delimiters);
                        if (components.Length != 2)
                        {
                            throw new FormatException("Point must contain both an X and a Y");
                        }
                        try
                        {
                            double x = Convert.ToDouble(components[0]);
                            double y = Convert.ToDouble(components[1]);
                            series.AddPoint(x, y);
                            axisBounds[Axis.Category].Check(x);
                            axisBounds[axis].Check(y);
                        }
                        catch (FormatException)
                        {
                            throw new FormatException("Invalid number in " + pointAsString);
                        }
                    }
                    else
                    {
                        done = true;
                    }
                }

                dataSeriesIndex++;

                if (dataSeriesIndex > Colors.GetLength(0))
                {
                    throw new FormatException($"Too many data series specified; maximum is {Colors.GetLength(0)}");
                }

                currentLine = reader.ReadLine();
            }

            if (dataSeriesIndex == 0)
            {
                throw new FormatException("At least one data series must be specified for a line chart");
            }
        }

        public override void SaveFile(StreamWriter writer)
        {
            foreach (string seriesName in dataSeries.Keys)
            {
                var series = dataSeries[seriesName];
                writer.WriteLine($"DS,{series.Name},{series.Axis}");
                foreach (var xValue in series.X)
                {
                    writer.WriteLine($"{xValue},{series[xValue]}");
                }
                writer.WriteLine("-1");
            }
        }
        #endregion

        #region Drawing the chart
        public override void Draw(GLControl c)
        {
            DrawChartAxes();
            int[] viewport = new int[4];
            GL.GetInteger(GetPName.Viewport, viewport);

            //Console.WriteLine($"Draw => X: {viewport[0]}, Y: {viewport[1]}, Width: {viewport[2]}, Height: {viewport[3]}");

            //Console.WriteLine($"{World.Bottom}, {World.Height}");

            foreach (var series in dataSeries.Values)
            {
                var world = CreateWorld(series.Axis);
                //Console.WriteLine($"{series.Name}, {world.Bottom}, {world.Height}");
                bool isSelectedSeries = selectedSeries != null && selectedSeries.Name.Equals(series.Name);
                short stipplePattern = (short)(isSelectedSeries ? 0xF0F0 : 0xFFFF);
                GL.LineStipple(1, stipplePattern);

                GL.Color3(series.Color.Red, series.Color.Green, series.Color.Blue);
                GL.PushMatrix();
                {
                    GL.Translate(0, World.Bottom, 0);
                    GL.Scale(1, World.Height / world.Height, 1);
                    GL.Translate(0, -world.Bottom, 0);

                    GL.Begin(PrimitiveType.LineStrip);
                    {
                        foreach (double x in series.X)
                        {
                            GL.Vertex2(x, series[x]);
                            //Console.WriteLine($"{x}, {series[x]}");
                        }
                    }
                    GL.End();
                }
                GL.PopMatrix();

                GL.LineStipple(1, 0xFFFF);

                //  Use pixel coordinates to draw the markers so they are a fixed size
                GL.MatrixMode(MatrixMode.Projection);

                foreach (double x in series.X)
                {
                    var viewportCoordinates = Utilities.WorldToViewport(x, series[x], world.Left, world.Right, world.Bottom, world.Top);
                    var operation = isSelectedSeries ? series.Marker.SelectedPrimitive : series.Marker.DefaultPrimitive;
                    GL.PushMatrix();
                    GL.LoadIdentity();
                    GL.Ortho(0, viewport[2], 0, viewport[3], -1, 1);
                    GL.Begin(operation);
                    {
                        series.Marker.Drawer(viewportCoordinates.X, viewportCoordinates.Y);
                    }
                    GL.End();
                    GL.PopMatrix();
                }

            }
        }

        private void DrawChartAxes()
        {
            GL.Color3(Color.Black);
            GL.Begin(PrimitiveType.Lines);
            {
                //  Category Axis
                GL.Vertex2(axisBounds[Axis.Category].Minimum, axisBounds[Axis.Primary].Minimum);
                GL.Vertex2(axisBounds[Axis.Category].Maximum, axisBounds[Axis.Primary].Minimum);

                //  Primary Value Axis
                GL.Vertex2(axisBounds[Axis.Category].Minimum, axisBounds[Axis.Primary].Minimum);
                GL.Vertex2(axisBounds[Axis.Category].Minimum, axisBounds[Axis.Primary].Maximum);

                //  Secondary Value Axis
                GL.Vertex2(axisBounds[Axis.Category].Maximum, axisBounds[Axis.Primary].Minimum);
                GL.Vertex2(axisBounds[Axis.Category].Maximum, axisBounds[Axis.Primary].Maximum);

                //  Top
                GL.Vertex2(axisBounds[Axis.Category].Maximum, axisBounds[Axis.Primary].Maximum);
                GL.Vertex2(axisBounds[Axis.Category].Minimum, axisBounds[Axis.Primary].Maximum);
            }
            GL.End();

            double increment = axisBounds[Axis.Category].Range / (NumberOfTicks);
            double tickLength = TickLengthPixels * axisBounds[Axis.Primary].Range / Viewport.Height;

            //  Horizontal tick marks
            GL.Begin(PrimitiveType.Lines);
            {
                for (int i = 0; i <= NumberOfTicks; i++)
                {
                    var x = axisBounds[Axis.Category].Minimum + i * increment;
                    GL.Vertex2(x, axisBounds[Axis.Primary].Minimum);
                    GL.Vertex2(axisBounds[Axis.Category].Minimum + i * increment, axisBounds[Axis.Primary].Minimum + tickLength);
                }
            }
            GL.End();

            //  Vertical tick marks            
            increment = axisBounds[Axis.Primary].Range / (NumberOfTicks);
            tickLength = TickLengthPixels * axisBounds[Axis.Category].Range / Viewport.Width;

            GL.Begin(PrimitiveType.Lines);
            {
                for (int i = 1; i <= NumberOfTicks + 1; i++)
                {
                    GL.Vertex2(axisBounds[Axis.Category].Minimum - tickLength, axisBounds[Axis.Primary].Minimum + i * increment);
                    GL.Vertex2(axisBounds[Axis.Category].Minimum, axisBounds[Axis.Primary].Minimum + i * increment);
                }
            }
            GL.End();
        }

        #endregion

        public override ICollection<string> LegendEntries => dataSeries.Keys;


        public override Color3 GetLegendColor(string item)
        {
            return dataSeries[item].Color;
        }

        public override void ClearSelection()
        {
            selectedSeries = null;
        }

        public override Selection GetSelection(string seriesName)
        {
            selectedSeries = dataSeries[seriesName];
            return selectedSeries == null ? null : new DataSeriesSelection(selectedSeries);

        }

        public override Selection GetSelection(double wx, double wy)
        {
            int xFudge = 5;
            double distanceThreshold = 5;
            var clickLocation = Utilities.WorldToViewport(wx, wy, World.Left, World.Right, World.Bottom, World.Top);

            foreach (var ds in dataSeries.Values)
            {
                var world = CreateWorld(ds.Axis);

                for (int ptIndex = 0; ptIndex < ds.points.Count - 1; ptIndex++)
                {
                    double x = ds.X[ptIndex];
                    double y = ds[x];
                    var start = Utilities.WorldToViewport(x, y, world.Left, world.Right, world.Bottom, world.Top);
                    x = ds.X[ptIndex + 1];
                    y = ds[x];
                    var end = Utilities.WorldToViewport(x, y, world.Left, world.Right, world.Bottom, world.Top);


                    if (clickLocation.X >= (start.X - xFudge) && clickLocation.X <= (end.X + xFudge))
                    {
                        var v = new Vector3(start, end);
                        var vPerp = v.Perp;

                        var c = new Vector3(start, clickLocation);
                        double distance = Math.Abs(vPerp.Dot(c) / vPerp.Length);
                        if (distance <= distanceThreshold)
                        {
                            selectedSeries = ds;
                            return new DataSeriesSelection(ds);
                        }
                    }
                }

            }

            selectedSeries = null;
            return null;
        }

        public override CoordinateSystem World => CreateWorld(Axis.Primary);

        private CoordinateSystem CreateWorld(Axis axis)
        {
            double width = axisBounds[Axis.Category].Maximum - axisBounds[Axis.Category].Minimum;
            double extraWidth = 0.05 * width;

            double height = axisBounds[axis].Maximum - axisBounds[axis].Minimum;
            double extraHeight = 0.05 * height;

            return new CoordinateSystem(
                axisBounds[Axis.Category].Minimum - extraWidth,
                axisBounds[Axis.Category].Maximum + extraWidth,
                axisBounds[axis].Minimum - extraHeight,
                axisBounds[axis].Maximum + extraHeight
            );
        }

        public override void DrawLegendMarker(string seriesName, double dx, double dy)
        {
            int x = (int)dx;
            int y = (int)dy;

            GL.Begin(PrimitiveType.Lines);
            {
                GL.Vertex2(x, y + ChartCreator.BoxHeight / 2);
                GL.Vertex2(x + ChartCreator.BoxWidth, y + ChartCreator.BoxHeight / 2);
            }
            GL.End();

            DataSeries series = dataSeries[seriesName];

            GL.Begin(series.Marker.DefaultPrimitive);
            {
                series.Marker.Drawer(x + ChartCreator.BoxWidth / 2, y + ChartCreator.BoxHeight / 2);
            }
            GL.End();
        }

        public override void DoExplosion(Selection exploded, double wdx, double wdy)
        {
            // Do nothing, scatter plots cannot be exploded
        }
    }
}
