﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chart_Creator
{
    public class SliceSelection : Selection
    {

        private readonly DonutChart.SliceInfo slice;

        public string Name { get; }

        public Color3 CurrentColor
        {
            get
            {
                throw new NotImplementedException("SliceSelection does not implement CurrentColor");
            }
        }

        public Color3 CurrentFillColor => slice.Color;

        public bool CanChangeColor => false;

        public bool CanChangeFillColor => true;

        public SliceSelection(string name, DonutChart.SliceInfo slice)
        {
            if(slice == null)
                throw new NullReferenceException("Selected slice cannot be null");
            this.slice = slice;
            Name = name;
        }

        public void ChangeColor(byte r, byte g, byte b)
        {
            throw new NotImplementedException("Pie charts do not support changing color, only fill color");
        }

        public void ChangeFillColor(byte r, byte g, byte b)
        {
            slice.Chart.Colors[slice.ColorIndex, 0] = r;
            slice.Chart.Colors[slice.ColorIndex, 1] = g;
            slice.Chart.Colors[slice.ColorIndex, 2] = b;
        }

        public Action DefaultAction => Action.ChangeFillColor;

        public override bool Equals(object toCompareTo) => (toCompareTo as SliceSelection)?.slice.Equals(slice) ?? false;

        public override int GetHashCode() => slice.GetHashCode();

        public bool CanExplode => true;
    }
}
