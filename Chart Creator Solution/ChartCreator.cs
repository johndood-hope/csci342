using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using ArcDrawer;
using csci342;
using csci342.Text;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Chart_Creator
{
    public partial class ChartCreator : Form
    {

        private Selection _currentSelection;

        private static readonly Color3 SelectionHandleColor = new Color3(200, 200, 255);

        private const int LegendMarginY = 10;
        private const int LegendMarginX = 10;
        private const int LegendTextMargin = 10;
        public const int BoxWidth = 20;
        public const int BoxHeight = 10;


        public static int FontID { get; private set; }
        private int _legendWidth;

        //  The maximum width, in pixels, of an item label
        //  NOT the width of the string that occurs most frequently
        private int _maxItemWidth;

        private Point2D _chartTitleLocation;
        private Size _titleSize;

        private int TitleWidth => _titleSize.Width;
        private int TitleHeight => _titleSize.Height;

        private bool _chartTitleSelected;

        private AbstractChart _chart;

        private Rectangle _chartViewport;

        public bool FileOpened => _chart != null;

        private string _fileToOpenOnLoad;

        public static readonly byte[,] DefaultColors =
        {
            {69, 114, 167},
            {170, 70, 67},
            {137, 165, 78},
            {113, 88, 143},
            {65, 152, 175},
            {219, 132, 61},
            {147, 169, 207},
            {209, 147, 146}
        };

        private string _chartFileName;

        public ChartCreator()
        {
            InitializeComponent();
            _fileToOpenOnLoad = null;
            _chartFileName = null;
        }

        public ChartCreator(string fileToOpen) : this()
        {
            _fileToOpenOnLoad = fileToOpen;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowOpenFileDialog();
        }

        private void ShowOpenFileDialog()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                LoadFile(openFileDialog1.FileName);
            }
        }

        private void DrawChartTitle()
        {
            int offset = 2;

            var titleColor = _chart.TitleColor;
            GL.Color3(titleColor.Red, titleColor.Green, titleColor.Blue);

            GL.Viewport(0, 0, canvas.Width, canvas.Height);
            Utilities.SetWindow(0, canvas.Width, 0, canvas.Height);

            _chartTitleLocation = new Point2D(canvas.Width / 2 - TitleWidth / 2, canvas.Height - 1.5 * TitleHeight);
            TextDrawer.DrawString(_chart.Title, _chartTitleLocation.X, _chartTitleLocation.Y, FontID);

            if (_chartTitleSelected)
            {
                GL.Color3(SelectionHandleColor.Red, SelectionHandleColor.Green, SelectionHandleColor.Blue);
                GL.Begin(PrimitiveType.LineLoop);
                {
                    GL.Vertex2(_chartTitleLocation.X - offset, _chartTitleLocation.Y - offset);
                    GL.Vertex2(_chartTitleLocation.X + TitleWidth + offset, _chartTitleLocation.Y - offset);
                    GL.Vertex2(_chartTitleLocation.X + TitleWidth + offset, _chartTitleLocation.Y + TitleHeight + offset);
                    GL.Vertex2(_chartTitleLocation.X - offset, _chartTitleLocation.Y + TitleHeight + offset);
                }
                GL.End();

                DrawSelectionHandle(_chartTitleLocation.X - offset, _chartTitleLocation.Y - offset, 2);
                DrawSelectionHandle(_chartTitleLocation.X + TitleWidth + offset, _chartTitleLocation.Y - offset, 2);
                DrawSelectionHandle(_chartTitleLocation.X + TitleWidth + offset, _chartTitleLocation.Y + TitleHeight + offset, 2);
                DrawSelectionHandle(_chartTitleLocation.X - offset, _chartTitleLocation.Y + TitleHeight + offset, 2);

            }
        }

        private void LoadFile(string filename)
        {
            try
            {
                _chart = AbstractChart.LoadChartFromFile(filename);
            }
            catch (FormatException e)
            {
                MessageBox.Show(this, e.Message, $"Unable to load file {filename}");
                return;
            }

            _titleSize = FontGenerator.GetSize(_chart.Title);
            _maxItemWidth = -1;
            _chartFileName = filename;
            ExplosionState = Explosion_State.None;
            saveToolStripMenuItem.Enabled = false;
            _chartTitleSelected = false;

            canvas.Invalidate();
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            if (!canvas.IsHandleCreated) return;

            GL.ClearColor(Color.White);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            if (!FileOpened)
            {
                return;
            }

            if (_maxItemWidth == -1)
            {
                _maxItemWidth = _chart.LegendEntries.Select((entry) => FontGenerator.GetSize(entry).Width).Max();

                _legendWidth = LegendMarginX + BoxWidth + LegendTextMargin + _maxItemWidth + LegendMarginX;
            }

            double widthAvailable = canvas.Width - _legendWidth;
            double heightAvailable = canvas.Height - 1.5 * TitleHeight;

            _chartViewport = _chart.GetViewport(widthAvailable, heightAvailable);

            var availableCenter = new Point2D(widthAvailable / 2, heightAvailable / 2);
            int viewportLeft = (int)availableCenter.X - _chartViewport.Size.Width / 2;
            int viewportBottom = (int)availableCenter.Y - _chartViewport.Size.Height / 2;
            _chartViewport.Location = new Point(viewportLeft, viewportBottom);

            DrawLegend();
            DrawChartTitle();

            Utilities.SetWindow(_chart.World.Left, _chart.World.Right, _chart.World.Bottom, _chart.World.Top);
            GL.Viewport(_chartViewport.Location.X, _chartViewport.Location.Y, _chartViewport.Width, _chartViewport.Height);
            //Console.WriteLine($"Paint => X: {_chartViewport.Location.X}, Y: {_chartViewport.Location.Y}, Width: {_chartViewport.Width}, Height: {_chartViewport.Height}");
            _chart.Draw(canvas);

            canvas.SwapBuffers();
        }

        public static void DrawSelectionHandle(double x, double y, double selectionRadius)
        {
            //  Save the current drawing color
            double[] currentColor = new double[4];
            GL.GetDouble(GetPName.CurrentColor, currentColor);

            GL.Color3(SelectionHandleColor.Red, SelectionHandleColor.Green, SelectionHandleColor.Blue);

            GL.Begin(PrimitiveType.Polygon);
            {
                Arc.DrawCircle(x, y, selectionRadius);
            }
            GL.End();

            GL.Color3(0.1, 0.1, 0.1);
            GL.Begin(PrimitiveType.LineLoop);
            {
                Arc.DrawCircle(x, y, selectionRadius);
            }
            GL.End();

            //  Restore the previous drawing color
            GL.Color3(currentColor[0], currentColor[1], currentColor[2]);
        }

        public static void DrawSelectionHandle(double x, double y)
        {
            DrawSelectionHandle(x, y, 0.05);
        }

        private void DrawLegend()
        {
            int viewportLeft = canvas.Width - _legendWidth - 10;
            int viewportHeight = 10 + _chart.LegendEntries.Count * (BoxHeight + 10);

            GL.Viewport(viewportLeft, canvas.Height / 2 - viewportHeight / 2, _legendWidth, viewportHeight);

            Utilities.SetWindow(0, _legendWidth, 0, viewportHeight);

            int y = 10;

            foreach (string item in _chart.LegendEntries)
            {
                var c = _chart.GetLegendColor(item);
                GL.Color3(c.Red, c.Green, c.Blue);
                _chart.DrawLegendMarker(item, LegendMarginX, y);

                TextDrawer.DrawString(item, LegendMarginX + BoxWidth + LegendTextMargin, y - 5, FontID);
                y += BoxHeight + 10;
            }

            Utilities.SetForegroundBlack();
            GL.Begin(PrimitiveType.LineLoop);
            {
                GL.Vertex2(2, 2);
                GL.Vertex2(_legendWidth - 2, 2);
                GL.Vertex2(_legendWidth - 2, y - 2);
                GL.Vertex2(2, y - 2);
            }
            GL.End();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            Application.Exit();
        }

        private enum Explosion_State
        {
            None, Exploding
        }

        private Explosion_State _explosionState = Explosion_State.None;

        private Explosion_State ExplosionState
        {
            get { return _explosionState; }
            set
            {
                _explosionState = value;
                if (!FileOpened) return;
                if (value == Explosion_State.None)
                {
                    statusBar1.Text = "Click to select";
                    if (_currentSelection != null && _currentSelection.CanExplode)
                    {
                        statusBar1.Text += ", Click again to explode";
                    }
                    statusBar1.Text += ".";
                }
                else
                {
                    statusBar1.Text = "Click to place exploded slice. ";
                }
            }
        }

        private double _explosionStartX, _explosionStartY;

        private Selection _previousSelection;

        private void simpleOpenGlControl1_MouseClick(object sender, MouseEventArgs e)
        {
            if (!FileOpened) return;
            int sx = e.X;
            int sy = canvas.Height - e.Y;
            if (ExplosionState == Explosion_State.Exploding)
            {
                canvas_MouseMove(sender, e);
                ExplosionState = Explosion_State.None;
                canvas.Invalidate();
                return;
            }
            _previousSelection = _currentSelection;
            _currentSelection = null;
            _chartTitleSelected = false;
            _chart.ClearSelection();

            if (InChartArea(sx, sy))
            {
                _chartTitleSelected = false;
                double worldWidth = _chart.World.Right - _chart.World.Left;
                double worldHeight = _chart.World.Top - _chart.World.Bottom;

                double wx = (sx - _chartViewport.Location.X) / (_chartViewport.Width * 1.0) * worldWidth + _chart.World.Left;
                double wy = (sy - _chartViewport.Location.Y) / (_chartViewport.Height * 1.0) * worldHeight + _chart.World.Bottom;
                _currentSelection = _chart.GetSelection(wx, wy);
            }
            else
            {
                if (InChartTitle(sx, sy))
                {
                    _currentSelection = new ChartTitleSelection(_chart.TitleColor);
                    _chartTitleSelected = true;
                }
                else
                {
                    _previousSelection = null;
                    _currentSelection = GetLegendSelection(sx, sy);
                }
            }

            if (_currentSelection != null)
            {
                menuChangeColor.Enabled = _currentSelection.CanChangeColor;
                menuChangeFillColor.Enabled = _currentSelection.CanChangeFillColor;
                formatMenu.Enabled = true;

                if (_currentSelection.Equals(_previousSelection) && _currentSelection.CanExplode)
                {
                    _explosionStartX = sx;
                    _explosionStartY = sy;
                    ExplosionState = Explosion_State.Exploding;
                    saveToolStripMenuItem.Enabled = true;

                }
                else
                {
                    ExplosionState = Explosion_State.None;
                }
            }
            else
            {
                formatMenu.Enabled = false;
                ExplosionState = Explosion_State.None;
            }

            canvas.Invalidate();
        }

        private void canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (!FileOpened) return;

            if (ExplosionState == Explosion_State.Exploding)
            {
                double wdx = (e.X - _explosionStartX) * _chart.World.Width / _chartViewport.Width;
                double wdy = (canvas.Height - e.Y - _explosionStartY) * _chart.World.Height / _chartViewport.Height;
                _chart.DoExplosion(_currentSelection, wdx, wdy);
                canvas.Invalidate();
            }

        }

        private Selection GetLegendSelection(int sx, int sy)
        {
            sx -= canvas.Width - _legendWidth - 10;
            if (sx < 0)
            {
                return null;
            }
            int legendHeight = 10 + _chart.LegendEntries.Count * (BoxHeight + 10);
            sy -= (canvas.Height - legendHeight) / 2;
            if (sy < 0)
            {
                return null;
            }
            sy -= 10;
            int entry = sy / (BoxHeight + 10);
            //Console.WriteLine($"{entry}");

            return entry < _chart.LegendEntries.Count ? _chart.GetSelection(new List<string>(_chart.LegendEntries)[entry]) : null;
        }

        private bool InChartTitle(int sx, int sy)
        {
            return sx >= _chartTitleLocation.X && sx <= _chartTitleLocation.X + TitleWidth &&
                   sy >= _chartTitleLocation.Y;
        }

        private bool InChartArea(int sx, int sy)
        {

            return
                sx >= _chartViewport.Location.X && sx <= _chartViewport.Location.X + _chartViewport.Width &&
                sy >= _chartViewport.Location.Y && sy <= _chartViewport.Location.Y + _chartViewport.Height;
        }

        public void ChangeSelectionColor()
        {
            Color3 initialColor = _currentSelection.CurrentColor;
            colorDialog1.Color = Color.FromArgb(initialColor.Red, initialColor.Green, initialColor.Blue);
            DialogResult result = colorDialog1.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                Color selectedColor = colorDialog1.Color;
                //ChangeColor(_currentSelection.CurrentColor, selectedColor);
                _currentSelection.ChangeColor(selectedColor.R, selectedColor.G, selectedColor.B);
                canvas.Invalidate();
                _chart.ColorsSpecified = true;
                saveToolStripMenuItem.Enabled = true;
            }
        }

        public void ChangeFillColor()
        {
            Color3 initialColor = _currentSelection.CurrentFillColor;
            colorDialog1.Color = Color.FromArgb(initialColor.Red, initialColor.Green, initialColor.Blue);
            DialogResult result = colorDialog1.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                Color selectedColor = colorDialog1.Color;
                _currentSelection.ChangeFillColor(selectedColor.R, selectedColor.G, selectedColor.B);
                _chart.ColorsSpecified = true;
                saveToolStripMenuItem.Enabled = true;
                canvas.Invalidate();
            }
        }

        private void simpleOpenGlControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            simpleOpenGlControl1_MouseClick(sender, e);

            if (_currentSelection != null)
            {
                switch (_currentSelection.DefaultAction)
                {
                    case Action.ChangeFillColor:
                        ChangeFillColor();
                        break;

                    case Action.ChangeSelectionColor:
                        ChangeSelectionColor();
                        break;

                    default:
                        throw new Exception("Unhandled case " + _currentSelection.DefaultAction + " in MouseDoubleClick");
                }
            }
        }

        private void canvas_Resize(object sender, EventArgs e)
        {
            canvas.Invalidate();
        }

        private void colorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeSelectionColor();
        }

        private void fillColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeFillColor();
        }

        private int dWidth, dHeight;

        private void ChartCreator_Resize(object sender, EventArgs e)
        {
            canvas.Size = new Size(Width - dWidth, Height - dHeight);
        }

        private void canvas_Load(object sender, EventArgs e)
        {
            dWidth = Width - canvas.Width;
            dHeight = Height - canvas.Height;

            DrawingTools.EnableDefaultAntiAliasing();
            FontID = FontGenerator.LoadTexture("consolas");

            if (_fileToOpenOnLoad != null)
            {
                LoadFile(_fileToOpenOnLoad);
            }
            else
            {
                ShowOpenFileDialog();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var writer = new StreamWriter(_chartFileName))
            {
                writer.WriteLine(_chart.ChartTypeSpecifier);
                writer.WriteLine(_chart.Title);
                if (_chart.ColorsSpecified)
                {
                    writer.WriteLine(AbstractChart.ColorString);
                    for (int i = 0; i < _chart.Colors.GetLength(0); i++)
                    {
                        writer.WriteLine($"{_chart.Colors[i, 0]} {_chart.Colors[i, 1]} {_chart.Colors[i, 2]}");
                    }
                }
                _chart.SaveFile(writer);
            }
        }


    }
}