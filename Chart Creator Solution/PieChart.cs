﻿namespace Chart_Creator
{
    public class PieChart : DonutChart
    {
        public override double InnerRadius => 0;

        public new const char TypeSpecifier = 'p';

        public override char ChartTypeSpecifier => TypeSpecifier;
    }
}