﻿namespace Chart_Creator
{
    public class ColumnSeriesSelection : Selection
    {

        private readonly ColumnChart.Series Series;

        public ColumnSeriesSelection(ColumnChart.Series series)
        {
            Series = series;
        }

        public bool CanChangeColor => true;
        public bool CanChangeFillColor => true;
        public void ChangeColor(byte r, byte g, byte b)
        {
            Series.Container.Colors[Series.ColorIndex, 0] = r;
            Series.Container.Colors[Series.ColorIndex, 1] = g;
            Series.Container.Colors[Series.ColorIndex, 2] = b;
        }

        public void ChangeFillColor(byte r, byte g, byte b)
        {
            ChangeColor(r, g, b);
        }

        public Color3 CurrentColor => Series.Color;
        public Color3 CurrentFillColor => Series.Color;
        public Action DefaultAction => Action.ChangeFillColor;
        public bool CanExplode => false;
        public string Name => Series.Name;
    }
}