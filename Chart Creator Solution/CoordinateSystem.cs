﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chart_Creator
{
    public class CoordinateSystem
    {
        public double Left { get; private set; }
        public double Right { get; private set; }
        public double Bottom { get; private set; }
        public double Top { get; private set; }
        public double AspectRatio
        {
            get
            {
                return (Right - Left) / (Top - Bottom);
            }
        }

        public double Width
        {
            get
            {
                return Math.Abs(Right - Left);
            }
        }

        public double Height
        {
            get
            {
                return Math.Abs(Top - Bottom);
            }
        }

        public CoordinateSystem(double l, double r, double b, double t)
        {
            Left = l;
            Bottom = b;
            Right = r;
            Top = t;
        }
    }
}
