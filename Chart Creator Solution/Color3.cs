﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chart_Creator
{
    public class Color3
    {
        public Color3(byte red, byte green, byte blue)
        {
            Red = red;
            Green = green;
            Blue = blue;
        }

        public byte Red { get; set; }
        public byte Green { get; set; }
        public byte Blue { get; set; }
    }
}
