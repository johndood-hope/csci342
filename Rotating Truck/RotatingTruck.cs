﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Rotating_Truck
{
    public partial class RotatingTruck : Form
    {
        Timer timer = new Timer();
        private int angle = 0;

        public RotatingTruck()
        {
            InitializeComponent();
        }

        private void drawingArea_Paint(object sender, PaintEventArgs e)
        {
            if (!drawingArea.IsHandleCreated)
                return;
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            GL.PushMatrix();
            {
                GL.Rotate(90, 0, 0, 1);
                GL.Scale(1, -1, 1);
                
                GL.Rotate(-angle, 0, 0, 1);
                GL.Translate(0, 5, 0);
                
                DrawingTools.DrawTruck();
            }
            GL.PopMatrix();

            Utilities.DrawArc(0, 0, 5, 0, 360);


            drawingArea.SwapBuffers();
        }

        private void drawingArea_Load(object sender, EventArgs e)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();

            GL.Ortho(-10, 10, -10, 10, -1, 1);

            timer.Enabled = true;
            timer.Interval = 10;
            timer.Tick += UpdateDrawing;
            SetViewport();
        }

        private void SetViewport()
        {
            int min = Math.Min(drawingArea.Width, drawingArea.Height);
            GL.Viewport((drawingArea.Width-min)/2, (drawingArea.Height-min)/2, min, min);
            GL.ClearColor(Color.White);
            GL.Color3(Color.Black);
        }

        private void UpdateDrawing(object sender, EventArgs e)
        {
            angle = (angle + 1)%360;
            drawingArea.Invalidate();
        }
    }
}
