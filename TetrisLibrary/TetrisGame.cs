﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;
using static TetrisLibrary.GridTools;

namespace TetrisLibrary
{
    public class TetrisGame
    {
        private PieceGenerater.Piece Current => pieceGenerater.Current;
        private PieceGenerater.Piece Next => pieceGenerater.Next;

        private List<Position> placedPieces;
        private readonly PieceGenerater pieceGenerater;

        public TetrisGame(bool testMode)
        {
            TestMode = testMode;
            pieceGenerater = new PieceGenerater(TestMode);
            LinesCleared = 0;
            placedPieces = new List<Position>();
        }

        private bool CanMove(int dx, int dy)
        {
            Position newCenter = Current.Center.MakeRelative(dx, dy);
            if (PointsClip(newCenter, Current.Relative, Position.Zero, placedPieces))
            {
                return false;
            }
            if (ClipsBorder(newCenter, Current.Relative, 12, 18))
            {
                return false;
            }
            return true;
        }

        private bool TryMove(int dx, int dy)
        {
            if (CanMove(dx, dy))
            {
                Current.Center = Current.Center.MakeRelative(dx, dy);
                if (!CanMove(0, -1)) //If there is a piece directly below this one
                    return false;
                return true;
            }
            return false;
        }

        private bool IsRowComplete(int row)
        {
            for (int col = 0; col < 12; col++)
            {
                if (!placedPieces.Contains(new Position(col, row)))
                {
                    return false;
                }
            }
            return true;
        }

        private void RemoveAndShiftAboveDown(int row)
        {
            List<Position> newPieces = new List<Position>();
            foreach (Position p in placedPieces)
            {
                if (p.Y > row)
                    newPieces.Add(p.MakeRelative(0, -1));
                if (p.Y < row)
                    newPieces.Add(p);
            }
            placedPieces = newPieces;
        }

        private void PlacePiece()
        {
            foreach (Position p in Current.Relative)
            {
                placedPieces.Add(p.MakeRelative(Current.Center.X, Current.Center.Y));
            }
            int clearedRows = 0;
            for (int row = 0; row < 18; row++)
            {
                if (IsRowComplete(row))
                {
                    RemoveAndShiftAboveDown(row);
                    row--; //repeat at this row
                    clearedRows++;
                }
            }
            LinesCleared += clearedRows;
            LineCleared?.Invoke();
            pieceGenerater.ShiftNext();
        }

        public void MoveDown()
        {
            if (!TryMove(0, -1))
            {
                PlacePiece();
            }

        }

        public void MoveAllDown()
        {
            while (TryMove(0, -1))
            {

            }
            PlacePiece();
        }

        public void MoveLeft()
        {
            TryMove(-1, 0);
        }

        public void MoveRight()
        {
            TryMove(1, 0);
        }

        public void TryRotation(int xx, int xy, int yx, int yy)
        {
            if (!Current.Rotateable) return;
            List<Position> newCoords = new List<Position>();
            foreach (Position p in Current.Relative)
            {
                newCoords.Add(p.MakeRotation(xx, xy, yx, yy));
            }
            if (PointsClip(Current.Center, newCoords, Position.Zero, placedPieces))
            {
                return;
            }
            if (ClipsBorder(Current.Center, newCoords, 12, 18))
            {
                return;
            }
            Current.Relative = newCoords;
            if (!CanMove(0, -1))
            {
                PlacePiece();

            }
        }

        public void RotateClockwise()
        {
            TryRotation(0, 1, -1, 0);

        }

        public void RotateCounterClockwise()
        {
            TryRotation(0, -1, 1, 0);
        }

        public void Reflect()
        {
            TryRotation(-1, 0, 0, 1);
        }

        //Current Preview Center
        private static readonly Position CPC = new Position(17, 15);
        //Next Preview Center
        private static readonly Position NPC = new Position(17, 8);


        private char[,] GetDrawingMask()
        {
            char[,] drawingMask = CreateMask(20, 20);
            //Setup board border
            ApplyBordersToMask(Position.Zero, 12, 18, drawingMask);
            //Setup board fill
            FillRegion(new Position(1, 1), 12, 18, '.', drawingMask);
            //Setup current preview border
            ApplyBordersToMask(CPC.MakeRelative(-2, -2), 3, 3, drawingMask);
            //Setup Next preview border
            ApplyBordersToMask(NPC.MakeRelative(-2, -2), 3, 3, drawingMask);
            //Setup current preview label
            ApplyTextToMask(CPC.MakeRelative(-2, 3), "This", drawingMask);
            //Setup next preview label
            ApplyTextToMask(NPC.MakeRelative(-2, 3), "Next", drawingMask);
            //Apply placed pieces
            ApplyPoints(new Position(1, 1), placedPieces, drawingMask);
            //Apply current
            ApplyPoints(Current.Center.MakeRelative(1, 1), Current.Relative, drawingMask);
            //Apply current preview
            ApplyPoints(CPC, Current.Preview, drawingMask);
            //Apply next preview
            ApplyPoints(NPC, Next.Preview, drawingMask);
            return drawingMask;
        }

        public override string ToString()
        {
            char[,] drawingMask = GetDrawingMask();
            StringBuilder sb = new StringBuilder();
            for (int row = drawingMask.GetLength(1) - 1; row >= 0; row--)
            {
                for (int col = 0; col < drawingMask.GetLength(0); col++)
                {
                    sb.Append(drawingMask[col, row]);
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }

        public void PrintBoard()
        {
            Console.Write(ToString());
        }

        public delegate void DrawingFunction(int row, int col, char c);

        public void Draw(DrawingFunction drawingFunction)
        {
            char[,] drawingMask = GetDrawingMask();
            for (int row = 0; row < drawingMask.GetLength(1); row++)
            {
                for (int col = 0; col < drawingMask.GetLength(0); col++)
                {
                    drawingFunction(row, col, drawingMask[col, row]);
                }
            }
        }

        public delegate void LineClearedHandler();

        public event LineClearedHandler LineCleared;

        public bool TestMode { get; }
        public int LinesCleared { get; private set; }
    }
}


