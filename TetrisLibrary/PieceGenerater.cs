﻿using System;
using System.Collections.Generic;
using static TetrisLibrary.GridTools;

namespace TetrisLibrary
{
    public class PieceGenerater
    {
        public Piece Current { get; private set; }
        public Piece Next { get; private set; }

        private bool TestMode { get; }

        public PieceGenerater(bool testMode)
        {
            TestMode = testMode;
            Current = GenerateNext();
            Next = GenerateNext();
        }

        private int nextTet = 0;
        private readonly Random r = new Random(12);

        public void ShiftNext()
        {
            Current = Next;
            Next = GenerateNext();
        }

        private Piece GenerateNext()
        {
            Piece piece = null;
            char[] colors = { '*', '&', '%', '#', '@' };
            char color = colors[r.Next(5)];
            if (nextTet == 0)
                piece = GenerateLine(color);
            if (nextTet == 1)
                piece = GenerateT(color);
            if (nextTet == 2)
                piece = Generate2(color);
            if (nextTet == 3)
                piece = GenerateGamma(color);
            if (nextTet == 4)
                piece = GenerateS(color);
            if (nextTet == 5)
                piece = GenerateSquare(color);
            if (nextTet == 6)
                piece = GenerateL(color);
            if (TestMode)
            {
                nextTet++;
                nextTet %= 4;
            }
            else
            {
                MoveRandom(piece);
                if(piece.Rotateable)
                    RotateRandom(piece);
                nextTet = r.Next(7);
            }
            return piece;

        }

        private Piece GenerateLine(char color)
        {
            return new Piece(
                new Position(1, TestMode ? 17 : 16),
                new List<Position>()
                {
                    new Position(-1, 0, color),
                    new Position(0, 0, color),
                    new Position(1, 0, color)
                },
                true);
        }

        private Piece GenerateT(char color)
        {
            return new Piece(
                new Position(1, 16),
                new List<Position>()
                {
                    new Position(-1, 0, color),
                    new Position(0, 0, color),
                    new Position(1, 0, color),
                    new Position(0, 1, color)
                },
                true);
        }

        private Piece Generate2(char color)
        {
            return new Piece(
                new Position(1, 16),
                new List<Position>()
                {
                    new Position(-1, 1, color),
                    new Position(0, 1, color),
                    new Position(0, 0, color),
                    new Position(1, 0, color)
                },
                true);
        }


        private Piece GenerateGamma(char color)
        {
            return new Piece(
                new Position(1, 16),
                new List<Position>()
                {
                    new Position(1, 1, color),
                    new Position(0, 1, color),
                    new Position(0, 0, color),
                    new Position(0, -1, color)
                },
                true);
        }

        private Piece GenerateS(char color)
        {
            return new Piece(
                new Position(1, 16),
                new List<Position>()
                {
                    new Position(1, 1, color),
                    new Position(0, 1, color),
                    new Position(0, 0, color),
                    new Position(-1, 0, color)
                },
                true);
        }

        private Piece GenerateSquare(char color)
        {
            return new Piece(
                new Position(1, 16),
                new List<Position>()
                {
                    new Position(-1, 1, color),
                    new Position(0, 1, color),
                    new Position(-1, 0, color),
                    new Position(0, 0, color)
                },
                false);
        }

        private Piece GenerateL(char color)
        {
            return new Piece(
                new Position(1, 16),
                new List<Position>()
                {
                    new Position(0, -1, color),
                    new Position(0, 0, color),
                    new Position(0, 1, color),
                    new Position(-1, 1, color),
                },
                true);

        }

        private void MoveRandom(Piece piece)
        {
            int n = r.Next(12);
            for (int i = 0; i < n; i++)
            {
                Position next = piece.Center.MakeRelative(1, 0);

                if (ClipsBorder(next, piece.Relative, 12, 18))
                {
                    continue;
                }
                piece.Center = next;

            }

        }

        private void RotateRandom(Piece piece)
        {
            int n = r.Next(4);
            for (int i = 0; i < n; i++)
            {
                List<Position> newPositions = new List<Position>();
                foreach (Position position in piece.Relative)
                {
                    newPositions.Add(position.MakeRotation(0, 1, -1, 0));
                }
                if (ClipsBorder(piece.Center, newPositions, 12, 18))
                {
                    return;
                }
                piece.Relative = newPositions;
            }
        }

        public class Piece
        {
            public List<Position> Preview { get; set; }
            public List<Position> Relative { get; set; }
            public Position Center { get; set; }
            public bool Rotateable { get; }

            public Piece(Position center, List<Position> preview, bool rotateable)
            {
                Center = center;
                Relative = Preview = preview;
                Rotateable = rotateable;
            }
        }
    }
}