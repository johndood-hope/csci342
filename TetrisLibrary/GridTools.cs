﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TetrisLibrary
{
    class GridTools
    {
        public static char[,] CreateMask(int width, int height)
        {
            char[,] drawingMask = new char[width,height];
            FillRegion(Position.Zero, width, height, ' ', drawingMask );
            return drawingMask;
        }

        public static void FillRegion(Position bottomLeft, int width, int height, char c, char[,] drawingMask)
        {
            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    drawingMask[col + bottomLeft.X, row + bottomLeft.Y] = c;
                }
            }
        }

        public static void ApplyBordersToMask(Position bottomLeft, int innerWidth, int innerHeight, char[,] drawingMask)
        {
            for (int row = bottomLeft.Y + 1; row <= bottomLeft.Y + innerHeight; row++)
            {
                drawingMask[bottomLeft.X, row] = '|';
                drawingMask[bottomLeft.X + innerWidth + 1, row] = '|';
            }
            for (int col = bottomLeft.X + 1; col <= bottomLeft.X + innerWidth; col++)
            {
                drawingMask[col, bottomLeft.Y] = '-';
                drawingMask[col, bottomLeft.Y + innerHeight + 1] = '_';
            }
        }

        public static void ApplyTextToMask(Position startingPosition, string text, char[,] drawingMask)
        {
            for(int i = 0; i < text.Length; i++)
            {
                drawingMask[startingPosition.X + i, startingPosition.Y] = text[i];
            }
        }

        public static void ApplyPoints(Position offset, List<Position> points, char[,] drawingMask)
        {
            foreach(Position point in points)
            {
                drawingMask[point.X + offset.X, point.Y + offset.Y] = point.Color;
            }
        }

        public static bool PointsClip(Position offsetA, List<Position> pointsA, Position offsetB, List<Position> pointsB)
        {
            foreach(Position pointA in pointsA)
            {
                foreach(Position pointB in pointsB)
                {
                    if(pointA.X+offsetA.X==pointB.X+offsetB.X && pointA.Y+offsetA.Y==pointB.Y+offsetB.Y)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool ClipsBorder(Position offset, List<Position> points, int width, int height)
        {
            foreach(Position point in points)
            {
                int x = point.X + offset.X;
                int y = point.Y + offset.Y;
                if (x < 0 || y < 0 || x >= width || y >= height)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
