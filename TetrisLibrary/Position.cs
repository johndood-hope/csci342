﻿namespace TetrisLibrary
{
    public class Position
    {
        public static readonly Position Zero = new Position(0,0);

        public int X { get; private set; }
        public int Y { get; private set; }

        public Position(int x, int y, char color = '*')
        {
            X = x;
            Y = y;
            Color = color;
        }

        public override int GetHashCode()
        {
            return X << 16 + Y;
        }

        public override bool Equals(object obj)
        {
            if (obj is Position)
            {
                Position p = (Position)obj;
                if (X == p.X && Y == p.Y)
                {
                    return true;
                }

                return false;

            }
            return false;
        }

        public Position MakeRelative(int dx, int dy)
        {
            return new Position(X + dx, Y + dy, Color);
        }

        public Position MakeRotation(int xx, int xy, int yx, int yy)
        {
            return new Position(X*xx + Y*xy, X*yx + Y*yy, Color);
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }

        public char Color { get; private set; }
    }
}