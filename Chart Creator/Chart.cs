﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using csci342.Text;
using OpenTK.Graphics.OpenGL;

namespace Chart_Creator
{
    public abstract class Chart : IChart
    {
        public delegate void LegendMarkerDrawer();

        public class LegendEntry
        {
            public string Name { get; }
            public Color Color { get;  }
            public LegendMarkerDrawer Drawer { get; }

            public LegendEntry(string name, Color color, LegendMarkerDrawer drawer)
            {
                Name = name;
                Color = color;
                Drawer = drawer;
            }
        }

        public abstract IEnumerable<LegendEntry> Entries { get; }

        private const int LegendMargin = 10;
        private const int LegendPadding = 5;
        private const int MarkerLeftMargin = 10;
        private const int MarkerNamePadding = 10;
        private const int NameRightMargin = 10;

        public int EntryHeight => FontGenerator.GetSize("A").Height;

        public int LegendWidth => Entries.Select(entry => FontGenerator.GetSize(entry.Name).Width).Max()
            + 2 * EntryHeight + MarkerLeftMargin + MarkerNamePadding + NameRightMargin;
        public int LegendHeight => Entries.Count() * (EntryHeight + LegendPadding)
            + 2 * LegendMargin - LegendPadding;

        public void DrawLegend()
        {
            GL.Color3(Color.Black);
            GL.Begin(PrimitiveType.LineLoop);
            {
                int w = LegendWidth, h = LegendHeight;
                GL.Vertex2(0, 0);
                GL.Vertex2(w, 0);
                GL.Vertex2(w, h);
                GL.Vertex2(0, h);
            }
            GL.End();
            GL.Translate(MarkerLeftMargin, LegendMargin, 0);
            foreach (var entry in Entries.Reverse())
            {
                GL.PushMatrix();
                {
                    GL.Scale(EntryHeight/2.0, EntryHeight/2.0, 1);
                    GL.Translate(2, 1, 0);
                    entry.Drawer();
                }
                GL.PopMatrix();
                GL.PushMatrix();
                {
                    GL.Translate(2 * EntryHeight + MarkerNamePadding, 0, 0);
                    GL.Color3(entry.Color);
                    TextDrawer.DrawString(entry.Name, 0, 0, ChartCreator.CourierNewFont);
                }
                GL.PopMatrix();
                GL.Translate(0, EntryHeight + LegendPadding, 0);

            }
        }

        public abstract void DrawPlot(double aspectRatio, double wppx, double wppy);
        public abstract string Title { get; }
        public abstract void ClearSelections();
        public abstract void TryClick(double x, double y, double aspectRatio, double wppx, double wppy);
        public abstract bool FillSelected();
        public abstract bool LineSelected();
        public abstract void SetColor(Color color);
        public abstract Color? SelectedColor();

    }
}