﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using csci342.Text;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Chart_Creator
{
    public partial class ChartCreator : Form
    {
        private IChart chart = new NoChart();

        private bool TitleSelected { get; set; }
        private Color TitleColor { get; set; } = Color.Black;

        public IChart Chart
        {
            get { return chart; }
            set
            {
                chart = value;
                TitleColor = Color.Black;
                TitleSelected = false;
                fillColorMenuItem.Enabled = false;
                colorMenuItem.Enabled = false;
                saveMenuItem.Enabled = chart.CanSave();
                if (!plotArea.IsHandleCreated)
                    return;
                var titleSize = FontGenerator.GetSize(chart.Title);
                plotArea.MinimumSize = new Size(Math.Max(titleSize.Width, chart.LegendWidth) + 10, titleSize.Height + chart.LegendHeight + 10);
                plotArea.Invalidate();
            }
        }
        public int ConsolasFont { get; private set; }
        public static int CourierNewFont { get; private set; }

        public ChartCreator()
        {
            InitializeComponent();
        }



        private void saveMenuItem_Click(object sender, EventArgs e)
        {
            //Chart = Chart is NoChart ? new DummyChart() : (IChart)new NoChart();
            if (saveChart.ShowDialog() == DialogResult.OK)
            {
                using (var writer = new StreamWriter(saveChart.FileName))
                {
                    foreach (string s in chart.Save())
                    {
                        writer.WriteLine(s);
                    }
                }
            }
        }

        private void openMenuItem_Click(object sender, EventArgs e)
        {
            if (openChart.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (var inputStream = new FileStream(openChart.FileName, FileMode.Open))
                    {
                        var input = new StreamReader(inputStream);
                        string line;
                        var lines = new List<string>();
                        while ((line = input.ReadLine()) != null)
                        {
                            lines.Add(line.Trim());
                        }
                        var newChart = ChartFactory.Create(lines);
                        if (newChart != null)
                        {
                            Chart = newChart;
                        }
                        else
                        {
                            MessageBox.Show(this, ChartFactory.LastError);
                        }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(this, $"Could not read file {openChart.FileName}");
                }
            }
        }

        private void colorMenuItem_Click(object sender, EventArgs e)
        {
            if (TitleSelected)
            {
                TitleColor = PromptForColor(TitleColor);

            }
            if (Chart.LineSelected())
            {
                Chart.SetColor(PromptForColor(Chart.SelectedColor() ?? Color.Black));
            }
            plotArea.Invalidate();
        }

        private void fillColorMenuItem_Click(object sender, EventArgs e)
        {
            if (Chart.FillSelected())
            {
                Chart.SetColor(PromptForColor(Chart.SelectedColor() ?? Color.Black));
            }
            plotArea.Invalidate();
        }

        private Color PromptForColor(Color existing)
        {
            colorDialog.Color = existing;
            return colorDialog.ShowDialog() == DialogResult.OK ? colorDialog.Color : existing;
        }

        private void plotArea_Click(object sender, EventArgs eventArgs)
        {
            var e = (MouseEventArgs)eventArgs;
            TitleSelected = false;
            Chart.ClearSelections();

            TryClick(e.X, e.Y);

            plotArea.Invalidate();

        }

        private void TryClick(int x, int y)
        {
            var titleSize = FontGenerator.GetSize(Chart.Title);
            if (y <= titleSize.Height)
            {
                if (x >= (plotArea.Width - titleSize.Width) / 2
                    && x <= (plotArea.Width - titleSize.Width) / 2 + titleSize.Width)
                {
                    // Handle title click
                    TitleSelected = true;
                }
            }
            else if (x < plotArea.Width - Chart.LegendWidth)
            {
                // Handle plot click
                PlotClick(x, plotArea.Height - y);
            }
            if (TitleSelected || Chart.LineSelected())
            {
                fillColorMenuItem.Enabled = false;
                colorMenuItem.Enabled = true;
            }
            else if (Chart.FillSelected())
            {
                fillColorMenuItem.Enabled = true;
                colorMenuItem.Enabled = false;
            }
            else
            {
                fillColorMenuItem.Enabled = false;
                colorMenuItem.Enabled = false;
            }
        }

        private void plotArea_DoubleClick(object sender, EventArgs e)
        {
            plotArea_Click(sender, e);
            fillColorMenuItem_Click(sender, e);
        }

        private void plotArea_Paint(object sender, PaintEventArgs e)
        {
            if (!plotArea.IsHandleCreated)
                return;

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            DrawTitle();

            DrawLegend();

            DrawChart();

            plotArea.SwapBuffers();


        }

        private void DrawTitle()
        {
            var titleSize = FontGenerator.GetSize(Chart.Title);
            GL.Viewport((plotArea.Width - titleSize.Width) / 2, plotArea.Height - titleSize.Height, titleSize.Width, titleSize.Height);

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, titleSize.Width, 0, titleSize.Height, -1, 1);

            GL.Color3(TitleColor);
            if (TitleSelected)
            {
                GL.Begin(PrimitiveType.Polygon);
                {
                    GL.Vertex2(1, 1);
                    GL.Vertex2(1, titleSize.Height - 1);
                    GL.Vertex2(titleSize.Width - 1, titleSize.Height - 1);
                    GL.Vertex2(titleSize.Width - 1, 1);
                }
                GL.End();
                GL.Color3(Color.White);
            }
            TextDrawer.DrawString(Chart.Title, 0, 0, CourierNewFont);
        }

        private void DrawLegend()
        {
            var titleSize = FontGenerator.GetSize(Chart.Title);
            GL.Viewport(plotArea.Width - Chart.LegendWidth, (plotArea.Height - titleSize.Height - Chart.LegendHeight) / 2, Chart.LegendWidth, Chart.LegendHeight);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, Chart.LegendWidth, 0, Chart.LegendHeight, -1, 1);

            Chart.DrawLegend();
        }

        private readonly int margin = 10;

        private Size PlotSize
        {
            get
            {
                var titleSize = FontGenerator.GetSize(Chart.Title);
                int pWidth = plotArea.Width - 2 * margin - Chart.LegendWidth;
                int pHeight = plotArea.Height - 2 * margin - titleSize.Height;
                return new Size(pWidth, pHeight);
            }
        }

        private void DrawChart()
        {
            var plotSize = PlotSize;
            double pWidth = plotSize.Width;
            double pHeight = plotSize.Height;
            GL.Viewport(margin, margin, plotSize.Width, plotSize.Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(-1, 1, -1, 1, -1, 1);

            Chart.DrawPlot(pWidth / pHeight, 2.0 / pWidth, 2.0 / pHeight);
        }

        public void PlotClick(double x, double y)
        {
            if (x < margin)
                return;
            if (y < margin)
                return;
            var plotSize = PlotSize;
            x -= margin;
            y -= margin;
            double pWidth = plotSize.Width;
            double pHeight = plotSize.Height;
            x = 2 * x / pWidth - 1;
            y = 2 * y / pHeight - 1;
            Chart.TryClick(x, y, pWidth / pHeight, 2.0 / pWidth, 2.0 / pHeight);
        }

        private void plotArea_Load(object sender, EventArgs e)
        {
            GL.ClearColor(Color.White);
            ConsolasFont = FontGenerator.LoadTexture("consolas");
            CourierNewFont = FontGenerator.LoadTexture("courier new");
            DrawingTools.EnableDefaultAntiAliasing();
        }

        private void plotArea_Resize(object sender, EventArgs e)
        {
            if (!plotArea.IsHandleCreated)
                return;
            plotArea.Invalidate();
        }
    }
}
