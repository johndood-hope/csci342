﻿using System.Drawing;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Chart_Creator
{
    public class NoChart : IChart
    {
        public string Title { get; } = "No chart loaded.";
        public int LegendWidth { get; } = 50;
        public int LegendHeight { get; } = 50;

        public void DrawLegend()
        {
            GL.Color3(Color.Black);
            GL.Begin(PrimitiveType.LineStrip);
            {
                GL.Vertex2(0, 0);
                GL.Vertex2(0, LegendHeight);
                GL.Vertex2(LegendWidth, 0);
                GL.Vertex2(0, 0);
                GL.Vertex2(LegendWidth, LegendHeight);
                GL.Vertex2(LegendWidth, 0);
                GL.Vertex2(LegendWidth, LegendHeight);
                GL.Vertex2(0, LegendHeight);
            }
            GL.End();

        }

        public void DrawPlot(double aspectRatio, double wppx, double wppy)
        {
            GL.Color3(Color.Black);
            GL.Begin(PrimitiveType.LineStrip);
            {
                GL.Vertex2(-1, -1);
                GL.Vertex2(-1, 1);
                GL.Vertex2(1, -1);
                GL.Vertex2(-1, -1);
                GL.Vertex2(1, 1);
                GL.Vertex2(1, -1);
                GL.Vertex2(1, 1);
                GL.Vertex2(-1, 1);
            }
            GL.End();
            if (aspectRatio < 1)
            {
                GL.Scale(1, aspectRatio, 1);
            }
            else
            {
                GL.Scale(1 / aspectRatio, 1, 1);
            }
            GL.Color3(Color.Blue);
            Utilities.DrawArc(0, 0, 1, 0, 360);
        }

        public void ClearSelections()
        {

        }

        public void TryClick(double x, double y, double aspectRatio, double wppx, double wppy)
        {
            
        }

        public bool FillSelected()
        {
            return false;
        }

        public bool LineSelected()
        {
            return false;
        }

        public void SetColor(Color color)
        {
            
        }

        public Color? SelectedColor()
        {
            return null;
        }
    }
}