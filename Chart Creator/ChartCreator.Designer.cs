﻿namespace Chart_Creator
{
    partial class ChartCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.fileMenuItem = new System.Windows.Forms.MenuItem();
            this.openMenuItem = new System.Windows.Forms.MenuItem();
            this.saveMenuItem = new System.Windows.Forms.MenuItem();
            this.formatMenuItem = new System.Windows.Forms.MenuItem();
            this.colorMenuItem = new System.Windows.Forms.MenuItem();
            this.fillColorMenuItem = new System.Windows.Forms.MenuItem();
            this.plotArea = new OpenTK.GLControl();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.openChart = new System.Windows.Forms.OpenFileDialog();
            this.saveChart = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.fileMenuItem,
            this.formatMenuItem});
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.Index = 0;
            this.fileMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.openMenuItem,
            this.saveMenuItem});
            this.fileMenuItem.Text = "File";
            // 
            // openMenuItem
            // 
            this.openMenuItem.Index = 0;
            this.openMenuItem.Text = "Open";
            this.openMenuItem.Click += new System.EventHandler(this.openMenuItem_Click);
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.Enabled = false;
            this.saveMenuItem.Index = 1;
            this.saveMenuItem.Text = "Save";
            this.saveMenuItem.Click += new System.EventHandler(this.saveMenuItem_Click);
            // 
            // formatMenuItem
            // 
            this.formatMenuItem.Index = 1;
            this.formatMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.colorMenuItem,
            this.fillColorMenuItem});
            this.formatMenuItem.Text = "Format";
            // 
            // colorMenuItem
            // 
            this.colorMenuItem.Enabled = false;
            this.colorMenuItem.Index = 0;
            this.colorMenuItem.Text = "Color";
            this.colorMenuItem.Click += new System.EventHandler(this.colorMenuItem_Click);
            // 
            // fillColorMenuItem
            // 
            this.fillColorMenuItem.Enabled = false;
            this.fillColorMenuItem.Index = 1;
            this.fillColorMenuItem.Text = "Fill Color";
            this.fillColorMenuItem.Click += new System.EventHandler(this.fillColorMenuItem_Click);
            // 
            // plotArea
            // 
            this.plotArea.BackColor = System.Drawing.Color.Black;
            this.plotArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plotArea.Location = new System.Drawing.Point(0, 0);
            this.plotArea.MinimumSize = new System.Drawing.Size(300, 200);
            this.plotArea.Name = "plotArea";
            this.plotArea.Size = new System.Drawing.Size(485, 427);
            this.plotArea.TabIndex = 0;
            this.plotArea.VSync = false;
            this.plotArea.Load += new System.EventHandler(this.plotArea_Load);
            this.plotArea.Click += new System.EventHandler(this.plotArea_Click);
            this.plotArea.Paint += new System.Windows.Forms.PaintEventHandler(this.plotArea_Paint);
            this.plotArea.DoubleClick += new System.EventHandler(this.plotArea_DoubleClick);
            this.plotArea.Resize += new System.EventHandler(this.plotArea_Resize);
            // 
            // colorDialog
            // 
            this.colorDialog.FullOpen = true;
            this.colorDialog.SolidColorOnly = true;
            // 
            // openChart
            // 
            this.openChart.AddExtension = false;
            this.openChart.Title = "Choose a Chart";
            // 
            // saveChart
            // 
            this.saveChart.Title = "Save Chart";
            // 
            // ChartCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 427);
            this.Controls.Add(this.plotArea);
            this.Menu = this.mainMenu;
            this.Name = "ChartCreator";
            this.Text = "Chart Creator";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MainMenu mainMenu;
        private System.Windows.Forms.MenuItem fileMenuItem;
        private System.Windows.Forms.MenuItem openMenuItem;
        private System.Windows.Forms.MenuItem saveMenuItem;
        private System.Windows.Forms.MenuItem formatMenuItem;
        private System.Windows.Forms.MenuItem colorMenuItem;
        private System.Windows.Forms.MenuItem fillColorMenuItem;
        private OpenTK.GLControl plotArea;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.OpenFileDialog openChart;
        private System.Windows.Forms.SaveFileDialog saveChart;
    }
}

