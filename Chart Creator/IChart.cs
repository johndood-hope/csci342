﻿using System.Drawing;

namespace Chart_Creator
{
    public interface IChart
    {
        string Title { get; }

        int LegendWidth { get; }
        int LegendHeight { get; }

        void DrawLegend();

        void DrawPlot(double aspectRatio, double wppx, double wppy);

        void ClearSelections();

        void TryClick(double x, double y, double aspectRatio, double wppx, double wppy);
        bool FillSelected();
        bool LineSelected();

        void SetColor(Color color);
        Color? SelectedColor();
        
    }
}