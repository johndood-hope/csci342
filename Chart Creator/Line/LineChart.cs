﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Chart_Creator.Line
{
    public class LineChart : Chart
    {

        public IEnumerable<Series> Series { get; }

        public LineChart(string title, IEnumerable<Series> series)
        {
            Title = title;
            Series = series;
        }

        public override string Title { get; }

        public override IEnumerable<LegendEntry> Entries => Series.Select(s => MakeEntry(s.Name, s.Color, s.DrawerPair.Normal));

        public override void DrawPlot(double aspectRatio, double wppx, double wppy)
        {
            GL.Translate(-1, -1, 0);
            GL.Scale(2, 2, 0);
            wppx /= 2;
            wppy /= 2;
            //Give us some space to draw tickmarks
            GL.Translate(wppx * 10, wppx * 5, 0);
            //Scale factor in x and y directions
            double sfx = 1.0 - wppx * 15;
            double sfy = 1.0 - wppy * 10;
            GL.Scale(sfx, sfy, 1);
            wppx /= sfx;
            wppy /= sfy;
            DrawAxis(wppx, wppy);

            double yMax = Series.Max(series => series.DataPoints.Max(dataPoint => dataPoint.Y));
            double yMin = Series.Min(series => series.DataPoints.Min(dataPoint => dataPoint.Y));
            double xMax = Series.Max(series => series.DataPoints.Max(dataPoint => dataPoint.X));
            double xMin = Series.Min(series => series.DataPoints.Min(dataPoint => dataPoint.X));
            GL.Scale(1 / (xMax - xMin), 1 / (yMax - yMin), 1);
            wppx *= xMax - xMin;
            wppy *= yMax - yMin;
            GL.Translate(-xMin, -yMin, 0);

            foreach (var series in Series)
            {
                series.Draw(wppx, wppy);
            }
        }

        private void DrawAxis(double wppx, double wppy)
        {
            GL.Color3(Color.Black);
            GL.Begin(PrimitiveType.LineLoop);
            {
                GL.Vertex2(0, 1);
                GL.Vertex2(0, 0);
                GL.Vertex2(1, 0);
                GL.Vertex2(1, 1);
            }
            GL.End();
            GL.Begin(PrimitiveType.Lines);
            {
                for (int i = 0; i <= 10; i++)
                {
                    GL.Vertex2(0, 0.1 * i);
                    GL.Vertex2(-10 * wppx, 0.1 * i);
                }
                for (int i = 0; i <= 10; i++)
                {
                    GL.Vertex2(0.1 * i, 0);
                    GL.Vertex2(0.1 * i, 10 * wppy);
                }
            }
            GL.End();
        }

        private static LegendEntry MakeEntry(string title, Color color, Series.MarkerDrawer drawerPair)
        {
            return new LegendEntry(
                title,
                color,
                () =>
                {
                    GL.Color3(color);
                    GL.Begin(PrimitiveType.Lines);
                    {
                        GL.Vertex2(-2, 0);
                        GL.Vertex2(2, 0);
                    }
                    GL.End();
                    drawerPair();
                }
            );
            
        }

        public override void ClearSelections()
        {
            foreach (var series in Series)
            {
                series.Selected = false;
            }
        }

        public override void TryClick(double x, double y, double aspectRatio, double wppx, double wppy)
        {
            x = (x + 1) / 2;
            y = (y + 1) / 2;
            wppx /= 2;
            wppy /= 2;
            x -= wppx * 10;
            y -= wppx * 5;
            double sfx = 1.0 - wppx * 15;
            double sfy = 1.0 - wppy * 10;
            x /= sfx;
            y /= sfy;
            wppx /= sfx;
            wppy /= sfy;

            double yMax = Series.Max(series => series.DataPoints.Max(dataPoint => dataPoint.Y));
            double yMin = Series.Min(series => series.DataPoints.Min(dataPoint => dataPoint.Y));
            double xMax = Series.Max(series => series.DataPoints.Max(dataPoint => dataPoint.X));
            double xMin = Series.Min(series => series.DataPoints.Min(dataPoint => dataPoint.X));

            x *= xMax - xMin;
            y *= yMax - yMin;
            wppx *= xMax - xMin;
            wppy *= yMax - yMin;
            x += xMin;
            y += yMin;
            Series.Any(series => series.TryClick(x, y, wppx, wppy));
        }

        public override bool FillSelected()
        {
            return false;
        }

        public override bool LineSelected()
        {
            return Series.Any(series => series.Selected);
        }

        public override void SetColor(Color color)
        {
            Series.First(series => series.Selected).Color = color;
        }

        public override Color? SelectedColor()
        {
            return
                Series.Where(series => series.Selected)
                    .Select(series => (Color?)series.Color)
                    .FirstOrDefault();
        }
    }
}