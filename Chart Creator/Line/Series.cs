﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Security.Policy;
using OpenTK.Graphics.OpenGL;

namespace Chart_Creator.Line
{
    public class Series
    {
        public delegate void MarkerDrawer();

        public class MarkerDrawerPair
        {
            public MarkerDrawer Normal { get; }
            public MarkerDrawer Selected { get; }

            public MarkerDrawerPair(MarkerDrawer normal, MarkerDrawer selected)
            {
                Normal = normal;
                Selected = selected;
            }

            public MarkerDrawerPair(MarkerDrawer both) : this(both, both)
            {

            }
        }

        private Series(Builder builder)
        {
            Name = builder.Name;
            DrawerPair = builder.DrawerPair;
            Color = builder.Color;
            DataPoints = new List<DataPoint>(builder.DataPoints);
        }

        public IEnumerable<DataPoint> DataPoints { get; }
        public MarkerDrawerPair DrawerPair { get; }
        public Color Color { get; set; }
        public string Name { get; }
        public bool Selected { get; set; }

        public void Draw(double wppx, double wppy)
        {
            if (Selected)
            {
                GL.Enable(EnableCap.LineStipple);
                GL.LineStipple(2, 0x0f0f);
            }
            GL.Color3(Color);
            GL.Begin(PrimitiveType.LineStrip);
            {
                foreach (var point in DataPoints)
                {
                    GL.Vertex2(point.X, point.Y);
                }
            }
            GL.End();
            GL.Disable(EnableCap.LineStipple);
            foreach (var point in DataPoints)
            {
                GL.PushMatrix();
                {
                    GL.Translate(point.X, point.Y, 0);
                    GL.Scale(wppx * 4, wppy * 4, 1);
                    if (Selected)
                        DrawerPair.Selected();
                    else
                        DrawerPair.Normal();
                }
                GL.PopMatrix();
            }
        }


        public class Builder
        {
            public string Name { get; set; }
            public Color Color { get; set; }
            public MarkerDrawerPair DrawerPair { get; set; }

            public void AddDataPoint(DataPoint dp)
            {
                _dps.Add(dp);
            }

            private readonly List<DataPoint> _dps = new List<DataPoint>();

            public IEnumerable<DataPoint> DataPoints
            {
                get
                {
                    var list = new List<DataPoint>(_dps);
                    list.Sort((p1, p2)=>p1.X.CompareTo(p2.X));
                    return list.AsReadOnly();
                }
            }

            public Series Build()
            {
                return new Series(this);
            }
        }

        public bool TryClick(double x, double y, double wppx, double wppy)
        {
            DataPoint a = null;
            foreach(var b in DataPoints)
            {
                if (a != null)
                {
                    if (OnLineSegment(a, b, x, y, wppx, wppy))
                    {
                        Selected = true;
                        return true;
                    }
                }
                a = b;
            }
            return false;
        }

        public bool OnLineSegment(DataPoint a, DataPoint b, double x, double y, double wppx, double wppy)
        {
            double cx = b.X - a.X, cy = b.Y - a.Y;
            cx /= wppx;
            cy /= wppy;
            double mag = Math.Sqrt(cx * cx + cy * cy);
            cx /= mag;
            cy /= mag;
            double nx = -cy, ny = cx;

            double p = (x - a.X)/wppx * cx + (y - a.Y)/wppy * cy;
            if (p < 0 || p > mag)
                return false;
            double d = Math.Abs((x - a.X)/wppx*nx + (y - a.Y)/wppy*ny);
            return d <= 3;
        }
    }
}