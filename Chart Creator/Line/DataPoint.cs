﻿namespace Chart_Creator.Line
{
    public class DataPoint
    {
        public double X { get; }
        public double Y { get; }

        public DataPoint(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}