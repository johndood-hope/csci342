﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using Chart_Creator.Line;
using Chart_Creator.Pie;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Chart_Creator
{
    public static class ChartFactory
    {
        public static string LastError { get; private set; } = "";

        public static IChart Create(IReadOnlyList<string> lines)
        {
            if (lines.Count < 2)
                return null;
            bool isPie = lines[0].Equals("p");
            bool isLine = lines[0].Equals("l");
            if (!isPie && !isLine)
            {
                LastError = "Chart was neither pie nor line";
                return null;
            }
            string title = lines[1];
            int dataStart = 2;
            var colors = (IEnumerable<Color>)new List<Color>(DefaultColors);
            if (lines[2].Equals("Colors"))
            {
                dataStart = 11;
                colors = lines.Where(((s, i) => i > 2 && i <= 10)).Select(s =>
                {
                    var rgb = new List<int>(s.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse));
                    return Color.FromArgb(rgb[0], rgb[1], rgb[2]);
                });
            }
            if (isPie)
            {
                var order = new List<string>();
                var counts = new Dictionary<string, int>();
                foreach (string wline in lines.Where((s, i) => i >= dataStart))
                {
                    var line = wline.Trim();
                    if (counts.ContainsKey(line))
                    {
                        counts[line]++;
                    }
                    else
                    {
                        order.Add(line);
                        counts[line] = 1;
                    }
                }
                var categories = order.Select((name, i) => new Category(name, counts[name], colors.ElementAt(i)));
                return new PieChart(title, categories);
            }
            if (isLine)
            {
                var seriesList = new List<Series>();
                Series.Builder seriesBuilder = null;
                foreach (string line in lines.Where((s, i) => i >= dataStart && !s.Trim().Equals("")))
                {
                    if (seriesBuilder == null)
                    {
                        var parts = line.Split(',');
                        if (parts.Length != 3)
                        {
                            LastError = $"Expected to find series header but found: {line}";
                            return null; 
                        }

                        if (!parts[0].Trim().Equals("DS"))
                        {
                            LastError = $"Expected to find \"DS\" in {line} but found {parts[0]}";
                            return null; 
                        }
                        seriesBuilder = new Series.Builder()
                        {
                            Name = parts[1].Trim(),
                            Color = colors.ElementAt(seriesList.Count),
                            DrawerPair = MarkerDrawerPairs[seriesList.Count]
                        };
                    }
                    else
                    {
                        var coords = line.Split(',').Select(s => double.Parse(s.Trim())).ToArray();

                        if (coords.Length == 1)
                        {
                            if (Math.Abs(coords[0] - (-1)) > 0.01)
                            {
                                LastError = $"Line with one entry (not -1): {line}";
                                return null;
                            }
                            seriesList.Add(seriesBuilder.Build());
                            seriesBuilder = null;
                        }
                        else if (coords.Length == 2)
                        {
                            seriesBuilder.AddDataPoint(new DataPoint(coords[0], coords[1]));
                        }
                        else
                        {
                            LastError = $"Line with more than two entries: {line}";
                            return null;
                        }
                    }
                }
                return new LineChart(title, seriesList);
            }
            LastError = "Unkown Error";
            return null;
        }

        public static IEnumerable<Color> DefaultColors { get; } = (new List<Color>
        {
            Color.FromArgb(69, 114, 167),
            Color.FromArgb(170, 70, 67),
            Color.FromArgb(137, 165, 78),
            Color.FromArgb(113, 88, 143),
            Color.FromArgb(65, 152, 175),
            Color.FromArgb(219, 132, 61),
            Color.FromArgb(147, 169, 207),
            Color.FromArgb(209, 147, 146),
        }).AsReadOnly();

        public static IReadOnlyList<Series.MarkerDrawerPair> MarkerDrawerPairs { get; } = (new List<Series.MarkerDrawerPair>
        {
            new Series.MarkerDrawerPair(DrawFilledSquare, DrawEmptySquare),
            new Series.MarkerDrawerPair(DrawFilledCircle, DrawEmptyCircle),
            new Series.MarkerDrawerPair(DrawFilledTriangle, DrawEmptyTriangle),
            new Series.MarkerDrawerPair(DrawFilledDiamond, DrawEmptyDiamond),
            new Series.MarkerDrawerPair(DrawPlus),
            new Series.MarkerDrawerPair(DrawX),
            new Series.MarkerDrawerPair(DrawStar),
            new Series.MarkerDrawerPair(DrawFilledHexagon, DrawEmptyHexagon)
        }).AsReadOnly();

        private static void DrawFilledSquare()
        {
            Utilities.DrawRectangle(0, 0, 2, 2, true);
        }

        private static void DrawEmptySquare()
        {
            Utilities.DrawRectangle(0, 0, 2, 2);
        }

        private static void DrawFilledCircle()
        {
            Utilities.DrawArc(0, 0, 1, 0, 360, true);
        }

        private static void DrawEmptyCircle()
        {
            Utilities.DrawArc(0, 0, 1, 0, 360);
        }

        private static void DrawTriangle(PrimitiveType primitiveType)
        {
            GL.Begin(primitiveType);
            {
                GL.Vertex2(0, 1);
                GL.Vertex2(-0.866, -0.5);
                GL.Vertex2(0.866, -0.5);
            }
            GL.End();
        }

        private static void DrawFilledTriangle()
        {
            DrawTriangle(PrimitiveType.Triangles);
        }

        private static void DrawEmptyTriangle()
        {
            DrawTriangle(PrimitiveType.LineLoop);
        }

        private static void DrawDiamond(PrimitiveType primitiveType)
        {
            GL.Begin(primitiveType);
            {
                GL.Vertex2(0, 1);
                GL.Vertex2(-1, 0);
                GL.Vertex2(0, -1);
                GL.Vertex2(1, 0);
            }
            GL.End();
        }

        private static void DrawFilledDiamond()
        {
            DrawDiamond(PrimitiveType.Quads);
        }

        private static void DrawEmptyDiamond()
        {
            DrawDiamond(PrimitiveType.LineLoop);
        }

        private static void DrawPlus()
        {
            GL.Begin(PrimitiveType.Lines);
            {
                GL.Vertex2(0, -1);
                GL.Vertex2(0, 1);
                GL.Vertex2(-1, 0);
                GL.Vertex2(1, 0);
            }
            GL.End();
        }

        private static void DrawX()
        {
            GL.Begin(PrimitiveType.Lines);
            {
                GL.Vertex2(-1, -1);
                GL.Vertex2(1, 1);
                GL.Vertex2(-1, 1);
                GL.Vertex2(1, -1);
            }
            GL.End();
        }

        private static void DrawStar()
        {
            GL.Begin(PrimitiveType.Lines);
            {
                GL.Vertex2(0, -1);
                GL.Vertex2(0, 1);
                GL.Vertex2(-0.866, -0.5);
                GL.Vertex2(0.866, 0.5);
                GL.Vertex2(0.866, -0.5);
                GL.Vertex2(-0.866, 0.5);
            }
            GL.End();
        }

        private static void DrawHexagon(PrimitiveType primitiveType)
        {
            GL.Begin(primitiveType);
            {
                GL.Vertex2(-1, 0);
                GL.Vertex2(-0.5, -0.866);
                GL.Vertex2(0.5, -0.866);
                GL.Vertex2(1, 0);
                GL.Vertex2(0.5, 0.866);
                GL.Vertex2(-0.5, 0.866);
            }
            GL.End();
        }

        private static void DrawFilledHexagon()
        {
            DrawHexagon(PrimitiveType.Polygon);
        }

        private static void DrawEmptyHexagon()
        {
            DrawHexagon(PrimitiveType.LineLoop);
        }
    }
}