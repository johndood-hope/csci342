﻿using System.Drawing;

namespace Chart_Creator.Pie
{
    public class Category
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public Color Color { get; set; }
        public bool Selected { get; set; }

        public Category(string name, int count, Color color)
        {
            Name = name;
            Count = count;
            Color = color;
        }
    }
}