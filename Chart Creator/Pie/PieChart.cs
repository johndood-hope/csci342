﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Xml.Serialization;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Chart_Creator.Pie
{
    public class PieChart : Chart
    {

        private int Total => Categories.Sum(c => c.Count);
        public IEnumerable<Category> Categories { get; }

        public PieChart(string title, IEnumerable<Category> categories)
        {
            Title = title;
            Categories = new List<Category>(categories).AsReadOnly();
        }

        public override string Title { get; }

        public override IEnumerable<LegendEntry> Entries => Categories.Select(c => MakeEntry(c.Name, c.Color));

        public override void DrawPlot(double aspectRatio, double wppx, double wppy)
        {
            if (aspectRatio < 1)
            {
                GL.Scale(1, aspectRatio, 1);
            }
            else
            {
                GL.Scale(1 / aspectRatio, 1, 1);
            }
            GL.Scale(0.95, 0.95, 1);

            LoopOverCategories((shareSoFar, share, category) =>
            {
                GL.Color3(category.Color);
                Utilities.DrawArc(0, 0, 1, 360 * shareSoFar, 360 * share, true);
                //Draw border
                GL.Color3(Color.Black);
                GL.Begin(PrimitiveType.LineStrip);
                {
                    GL.Vertex2(Math.Cos(Math.PI * 2 * shareSoFar), Math.Sin(Math.PI * 2 * shareSoFar));
                    GL.Vertex2(0, 0);
                    GL.Vertex2(Math.Cos(Math.PI * 2 * (share + shareSoFar)), Math.Sin(Math.PI * 2 * (share + shareSoFar)));
                }
                GL.End();
            });
            GL.Color3(Color.Black);
            Utilities.DrawArc(0, 0, 1, 0, 360);
            LoopOverCategories((shareSoFar, share, category) =>
            {
                if (category.Selected)
                {
                    DrawCircle(0, 0);
                    DrawCircle(Math.Cos(Math.PI * 2 * shareSoFar), Math.Sin(Math.PI * 2 * shareSoFar));
                    DrawCircle(Math.Cos(Math.PI * 2 * (share + shareSoFar)), Math.Sin(Math.PI * 2 * (share + shareSoFar)));
                }
            });
        }

        private delegate void LoopInteration(double shareSoFar, double share, Category catagory);

        private void LoopOverCategories(LoopInteration loopInteration)
        {
            double shareSoFar = 0;
            double total = Total;
            foreach (var category in Categories)
            {
                double share = category.Count / total;
                loopInteration(shareSoFar, share, category);
                shareSoFar += share;
            }
        }

        private static void DrawCircle(double cx, double cy)
        {
            GL.Color3(Color.White);
            Utilities.DrawArc(cx, cy, 0.05, 0, 360, true);
            GL.Color3(Color.Black);
            Utilities.DrawArc(cx, cy, 0.05, 0, 360);
        }

        private static LegendEntry MakeEntry(string title, Color color)
        {
            return new LegendEntry(
                title,
                color,
                () =>
                {
                    GL.Color3(color);
                    Utilities.DrawRectangle(0, 0, 4, 2, true);
                }
            );
        }

        public override void ClearSelections()
        {
            foreach (var category in Categories)
            {
                category.Selected = false;
            }
        }

        public override void TryClick(double x, double y, double aspectRatio, double wppx, double wppy)
        {
            if (aspectRatio < 1)
            {
                y /= aspectRatio;
            }
            else
            {
                x *= aspectRatio;
            }
            x /= 0.95;
            y /= 0.95;
            double r = Math.Sqrt(x * x + y * y);
            if (r < 0.1 || r > 1)
            {
                return;
            }
            double clickShare = Math.Atan2(y, x) / (2 * Math.PI);
            if (clickShare < 0)
                clickShare += 1;
            LoopOverCategories((shareSoFar, share, catagory) =>
            {
                catagory.Selected = clickShare > shareSoFar && clickShare - shareSoFar < share;
            });
        }

        public override bool FillSelected()
        {
            return Categories.Any(catagory => catagory.Selected);
        }

        public override bool LineSelected()
        {
            return false;
        }

        public override void SetColor(Color color)
        {
            Categories.First(category => category.Selected).Color = color;
        }

        public override Color? SelectedColor()
        {
            return
                Categories.Where(category => category.Selected)
                    .Select(category => (Color?)category.Color)
                    .FirstOrDefault();
        }

    }
}