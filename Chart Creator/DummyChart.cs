﻿using System;
using System.Collections.Generic;
using System.Drawing;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Chart_Creator
{
    public class DummyChart : Chart
    {
        public override string Title => "My Descriptive Title";

        public override IEnumerable<LegendEntry> Entries { get; }

        public DummyChart()
        {
            Entries = new List<LegendEntry>
            {
                MakeDummyEntry("Item 1", Color.Red, Color.Green),
                MakeDummyEntry("Series Two", Color.Blue, Color.Yellow)
            };
        }

        public override void DrawPlot(double aspectRatio, double wppx, double wppy)
        {
            GL.Color3(Color.Purple);
            Utilities.DrawRectangle(0, 0, 1.5, 1.5, true);
        }

        private static LegendEntry MakeDummyEntry(string title, Color color, Color other)
        {
            return new LegendEntry(
                title,
                color,
                () =>
                {
                    GL.Color3(other);
                    Utilities.DrawRectangle(0, 0, 4, 2, true);
                }
            );
        }

        private bool selected;

        public override void ClearSelections()
        {
            selected = false;
        }

        public override void TryClick(double x, double y, double aspectRatio, double wppx, double wppy)
        {
            selected = true;
        }

        public override bool FillSelected()
        {
            return selected;
        }

        public override bool LineSelected()
        {
            return false;
        }

        public override void SetColor(Color color)
        {

        }

        public override Color? SelectedColor()
        {
            return Color.Purple;
        }
    }
}