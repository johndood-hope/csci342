﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Chart_Creator.Line;
using Chart_Creator.Pie;

namespace Chart_Creator
{
    public static class ChartSaver
    {
        private static Color[] Colors(this IChart chart)
        {
            Color[] colors = (chart as Chart).Entries.Select(entry => entry.Color).ToArray();
            return ChartFactory.DefaultColors.Select((c, i) => i < colors.Length ? colors[i] : c).ToArray();
        }

        private static IEnumerable<string> Repeat(this Category category)
        {
            for (int i = 0; i < category.Count; i++)
                yield return category.Name;
        }

        private static IEnumerable<string> YieldSeries(this Series series)
        {
            yield return $"DS,{series.Name},Primary";
            foreach (var dataPoint in series.DataPoints)
            {
                yield return $"{dataPoint.X}, {dataPoint.Y}";
            }
            yield return "-1";
        }

        public static IEnumerable<string> Save(this IChart chart)
        {
            if (CanSave(chart))
            {
                List<string> lines = new List<string>();
                var pieChart = chart as PieChart;
                var lineChart = chart as LineChart;

                if (pieChart != null)
                {
                    lines.Add("p");
                }
                if (lineChart != null)
                {
                    lines.Add("l");
                }

                lines.Add(chart.Title);

                Color[] colors = chart.Colors();
                if (ChartFactory.DefaultColors.Where((color, i) => !color.Equals(colors[i])).Any())
                {
                    lines.Add("Colors");
                    foreach (var color in colors)
                    {
                        lines.Add($"{color.R} {color.G} {color.B}");
                    }
                }

                if (pieChart != null)
                {
                    lines.AddRange(pieChart.Categories.SelectMany(Repeat));
                }
                if (lineChart != null)
                {
                    lines.AddRange(lineChart.Series.SelectMany(YieldSeries));
                }
                return lines;
            }
            return new string[0];
        }

        public static bool CanSave(this IChart chart)
        {
            return chart != null && (chart is PieChart || chart is LineChart);
        }
    }
}