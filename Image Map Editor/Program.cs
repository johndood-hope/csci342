﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Image_Map_Editor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length >= 2)
            {
                Console.Error.WriteLine("Incorrect number of arguments");
                Console.Error.WriteLine("usage: ImageMapEditor.exe [picture]");
                Console.Error.WriteLine();
                Console.Error.WriteLine("Where: ");
                Console.Error.WriteLine("\tpicture:\tName of image to load");
                Environment.Exit(-1);
            }
            string fileName = args.Length > 0 ? args[0] : null; 
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ImageMapEditor(fileName));
        }
    }
}
