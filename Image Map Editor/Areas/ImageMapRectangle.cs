﻿using System;
using System.Collections.Generic;
using System.Drawing;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Image_Map_Editor
{
    public class ImageMapRectangle : ImageMapArea
    {
        public static ImageMapRectangle Create(IList<int> coordinates)
        {
            if (coordinates.Count != 4)
            {
                return null;
            }
            return new ImageMapRectangle(coordinates[0], coordinates[1], coordinates[2], coordinates[3]);

        }

        public ImageMapPoint Point1 { get; }
        public ImageMapPoint Point2 { get; }

        public ImageMapRectangle(int x1, int y1, int x2, int y2)
        {
            Point1 = new ImageMapPoint(x1, y1);
            Point2 = new ImageMapPoint(x2, y2);
            DraggablePoints = new List<ImageMapPoint> { Point1, Point2 };
        }

        public string Href { get; set; }

        public string Alt { get; set; }

        public IReadOnlyList<ImageMapPoint> DraggablePoints { get; }

        public void Draw()
        {
            GL.Begin(PrimitiveType.LineLoop);
            {
                GL.Vertex2(Point1.X, Point1.Y);
                GL.Vertex2(Point2.X, Point1.Y);
                GL.Vertex2(Point2.X, Point2.Y);
                GL.Vertex2(Point1.X, Point2.Y);
            }
            GL.End();
            Utilities.DrawArc(Point1.X, Point1.Y, 3, 0, 360, true);
            Utilities.DrawArc(Point2.X, Point2.Y, 3, 0, 360, true);
        }

        public bool CloseEnoughToBorder(int x, int y)
        {
            if (Math.Abs(Point1.X - x) < 3 || Math.Abs(Point2.X - x) < 3)
            {
                if ((y >= Point1.Y && y <= Point2.Y) || (y <= Point1.Y && y >= Point2.Y))
                {
                    return true;
                }

            }
            if (Math.Abs(Point1.Y - y) < 3 || Math.Abs(Point2.Y - y) < 3)
            {
                if ((x >= Point1.X && x <= Point2.X) || (x <= Point1.X && x >= Point2.X))
                {
                    return true;
                }

            }
            return false;
        }

        public void Move(int dx, int dy)
        {
            Point1.X += dx;
            Point1.Y += dy;

            Point2.X += dx;
            Point2.Y += dy;
        }

        public string HtmlForm()
        {
            return
                $"    <area shape=\"rect\" coords=\"{Point1.X},{Point1.Y},{Point2.X},{Point2.Y}\" href=\"{Href}\" alt=\"{Alt}\" />\n";
        }
    }
}