﻿using System;
using AngleSharp.Html;

namespace Image_Map_Editor
{
    public class ImageMapPoint : ImageMapPrimitive
    {
        public int X { get; set; }
        public int Y { get; set; }

        public ImageMapPoint(int x, int y)
        {
            X = x;
            Y = y;
        }

        public bool IsCloseEnough(int x, int y)
            => Math.Sqrt(Math.Pow(X - x, 2) + Math.Pow(Y - y, 2)) < 3;
    }
}