﻿using System;
using System.Collections.Generic;
using System.Drawing;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Image_Map_Editor
{
    public class ImageMapCircle : ImageMapArea
    {
        public static ImageMapCircle Create(IList<int> coordinates)
        {
            if (coordinates.Count != 3)
                return null;
            var circle = new ImageMapCircle(coordinates[0], coordinates[1], coordinates[2]);
            return circle;
        }

        public ImageMapPoint Center { get; }
        public ImageMapPoint RadiusHandle { get; }

        public int R
        {
            get
            {
                int dx = Center.X - RadiusHandle.X;
                int dy = Center.Y - RadiusHandle.Y;
                return (int)Math.Sqrt(dx * dx + dy * dy);
            }
        }

        public string Href { get; set; }

        public string Alt { get; set; }

        public bool CloseEnoughToBorder(int x, int y)
        {
            double dx = Center.X - x, dy = Center.Y - y;
            double r = Math.Sqrt(dx * dx + dy * dy);
            return Math.Abs(R - r) < 3 || r < 5;
        }

        public ImageMapCircle(int x, int y, int r)
        {
            Center = new ImageMapPoint(x, y);
            RadiusHandle = new ImageMapPoint(x + r, y);
            DraggablePoints = new List<ImageMapPoint> { RadiusHandle };
        }

        public IReadOnlyList<ImageMapPoint> DraggablePoints { get; }

        public void Draw()
        {
            Utilities.DrawArc(Center.X, Center.Y, R, 0, 360);
            Utilities.DrawArc(RadiusHandle.X, RadiusHandle.Y, 3, 0, 360, true);
            GL.LineWidth(1f);
            GL.Begin(PrimitiveType.Lines);
            {
                GL.Vertex2(Center.X - 5, Center.Y);
                GL.Vertex2(Center.X + 5, Center.Y);
                GL.Vertex2(Center.X, Center.Y - 5);
                GL.Vertex2(Center.X, Center.Y + 5);
            }
            GL.End();
        }

        public void Move(int dx, int dy)
        {
            Center.X += dx;
            Center.Y += dy;
            RadiusHandle.X += dx;
            RadiusHandle.Y += dy;
        }

        public string HtmlForm()
        {
            return $"    <area shape=\"circle\" coords=\"{Center.X},{Center.Y},{R}\" alt=\"{Alt}\" href=\"{Href}\"/>\n";
        }
    }
}