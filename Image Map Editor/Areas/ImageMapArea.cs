﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace Image_Map_Editor
{
    public interface ImageMapArea : ImageMapPrimitive
    {
        IReadOnlyList<ImageMapPoint> DraggablePoints { get; }

        void Draw();

        string Href { get; set; }
        string Alt { get; set; }

        bool CloseEnoughToBorder(int x, int y);

        void Move(int dx, int dy);

        string HtmlForm();
    }
}