﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Image_Map_Editor
{
    public class ImageMapPolygon : ImageMapArea
    {
        public static ImageMapPolygon Create(IList<int> coordinates)
        {
            if (coordinates.Count < 6)
                return null;
            if (coordinates.Count % 2 == 1)
                return null;
            var polygon = new ImageMapPolygon();
            for (int i = 0; i < coordinates.Count / 2; i++)
            {
                polygon.AddPoint(new ImageMapPoint(coordinates[2*i], coordinates[2*i + 1]));
            }
            return polygon;
        }

        public IReadOnlyList<ImageMapPoint> DraggablePoints => Points.AsReadOnly();

        private List<ImageMapPoint> Points { get; }

        public void AddPoint(ImageMapPoint point)
        {
            Points.Add(point);
        }

        public void DeletePoint(ImageMapPoint point)
        {
            Points.Remove(point);
        }

        public ImageMapPolygon()
        {
            Points = new List<ImageMapPoint>();
        }

        public string Href { get; set; }

        public string Alt { get; set; }

        public void Draw()
        {
            GL.Begin(PrimitiveType.LineLoop);
            {
                foreach (var point in DraggablePoints)
                {
                    GL.Vertex2(point.X, point.Y);
                }
            }
            GL.End();
            foreach (var point in DraggablePoints)
            {
                Utilities.DrawArc(point.X, point.Y, 3, 0, 360, true);
            }
        }

        public bool CloseEnoughToBorder(int x, int y)
        {
            for (int i = 0; i < Points.Count; i++)
            {
                var p1 = Points[i];
                var p2 = Points[(i + 1)%Points.Count];
                double dx = p2.X - p1.X, dy = p2.Y - p1.Y;
                double mag = Math.Sqrt(dx*dx + dy*dy);
                double perpProjection = (-dy*(x - p1.X) + dx*(y - p1.Y))/mag;
                if (Math.Abs(perpProjection) < 3)
                {
                    double projection = (dx*(x - p1.X) + dy*(y - p1.Y))/mag;
                    if (projection >= 0 && projection <= mag)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void Move(int dx, int dy)
        {
            foreach (var point in Points)
            {
                point.X += dx;
                point.Y += dy;
            }
        }

        public string HtmlForm()
        {
            var sb = new StringBuilder();
            sb.Append("    <area shape=\"poly\" coords=\"");
            for (int i = 0; i < Points.Count - 1; i++)
            {
                var point = Points[i];
                sb.Append($"{point.X},{point.Y},");
            }
            sb.Append($"{Points.Last().X},{Points.Last().Y}");
            sb.Append($"\" href=\"{Href}\" alt=\"{Alt}\" />\n");
            return sb.ToString();
        }
    }
}