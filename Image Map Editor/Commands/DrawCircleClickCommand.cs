﻿using System;
using System.Collections.Generic;
using System.Drawing;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Image_Map_Editor
{
    public class DrawCircleClickCommand : IClickCommand
    {
        private ImageMapPoint center;

        public void Draw(int currentX, int currentY, ImageMapEditor ime)
        {
            if (center != null)
            {
                GL.Color3(Color.Gray);
                GL.LineWidth(3f);
                int dx = currentX - center.X;
                int dy = currentY - center.Y;
                double r = Math.Sqrt(dx*dx + dy*dy);
                Utilities.DrawArc(center.X, center.Y, r, 0, 360);
            }
        }

        public void Execute(int mouseX, int mouseY, ImageMapEditor ime)
        {
            if (center == null)
            {
                center = new ImageMapPoint(mouseX, mouseY);
            }
            else
            {
                int dx = mouseX - center.X;
                int dy = mouseY - center.Y;
                double r = Math.Sqrt(dx * dx + dy * dy);
                ime.ImageMap.AddArea(new ImageMapCircle(center.X, center.Y, (int)r));
                Reset(ime);
            }
        }

        public void Reset(ImageMapEditor ime)
        {
            center = null;

        }
    }
}