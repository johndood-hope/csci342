﻿using System.Security.Cryptography.X509Certificates;

namespace Image_Map_Editor
{
    public interface IClickCommand
    {
        void Draw(int currentX, int currentY, ImageMapEditor ime);

        void Execute(int mouseX, int mouseY, ImageMapEditor ime);

        void Reset(ImageMapEditor ime);
    }
}