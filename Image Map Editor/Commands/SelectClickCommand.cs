﻿using System;
using System.Drawing;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Image_Map_Editor
{
    public class SelectClickCommand : IClickCommand
    {
        public void Draw(int currentX, int currentY, ImageMapEditor ime)
        {
            GL.Color3(Color.DeepPink);
            GL.LineWidth(3f);

            if (ime.ImageMap.Selected is ImageMapPoint)
            {
                var point = (ImageMapPoint) ime.ImageMap.Selected;
                Utilities.DrawRectangle(point.X, point.Y, 10, 10);
            }
            (ime.ImageMap.Selected as ImageMapArea)?.Draw();
          

            var hovering = ime.ImageMap.GetPrimitiveUnder(currentX, currentY);
            if (hovering == ime.ImageMap.Selected)
                return;
            GL.Color3(Color.Gray);
            GL.LineWidth(3f);
            if (hovering is ImageMapPoint)
            {
                var point = (ImageMapPoint) hovering;
                Utilities.DrawRectangle(point.X, point.Y, 10, 10);
            }
            (hovering as ImageMapArea)?.Draw();
        }

        public void Execute(int mouseX, int mouseY, ImageMapEditor ime)
        {
            ime.ImageMap.Selected = ime.ImageMap.GetPrimitiveUnder(mouseX, mouseY);
            ime.SelectedChanged();
        }

        public void Reset(ImageMapEditor ime)
        {
            ime.ImageMap.Selected = null;
        }
    }
}