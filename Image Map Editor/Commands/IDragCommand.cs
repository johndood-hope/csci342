﻿namespace Image_Map_Editor
{
    public interface IDragCommand
    {
        void Draw(int startX, int startY, int currentX, int currentY, ImageMapEditor ime);

        void Execute(int startX, int startY, int endX, int endY, ImageMapEditor ime);

    }
}