﻿using System.Drawing;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Image_Map_Editor
{
    public class DrawRectangleClickCommand : IClickCommand
    {
        private ImageMapPoint point1;

        public void Draw(int currentX, int currentY, ImageMapEditor ime)
        {
            if (point1 != null)
            {
                GL.Color3(Color.Gray);
                GL.LineWidth(3f);
                Utilities.DrawArc(point1.X, point1.Y, 3, 0, 360, true);
                GL.Begin(PrimitiveType.LineLoop);
                {
                    GL.Vertex2(point1.X, point1.Y);
                    GL.Vertex2(currentX, point1.Y);
                    GL.Vertex2(currentX, currentY);
                    GL.Vertex2(point1.X, currentY);
                }
                GL.End();
            }
        }

        public void Execute(int mouseX, int mouseY, ImageMapEditor ime)
        {
            if (point1 == null)
            {
                point1 = new ImageMapPoint(mouseX, mouseY);
            }
            else if(!point1.IsCloseEnough(mouseX, mouseY))
            {
                ime.ImageMap.AddArea(new ImageMapRectangle(point1.X, point1.Y, mouseX, mouseY));
                Reset(ime);
            }
        }

        public void Reset(ImageMapEditor ime)
        {
            point1 = null;
        }
    }
}