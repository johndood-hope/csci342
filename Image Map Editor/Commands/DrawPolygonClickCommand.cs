﻿using System.Collections.Generic;
using System.Drawing;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Image_Map_Editor
{
    public class DrawPolygonClickCommand : IClickCommand
    {
        private List<ImageMapPoint> points = new List<ImageMapPoint>();

        public void Draw(int currentX, int currentY, ImageMapEditor ime)
        {
            GL.Color3(Color.Gray);
            GL.LineWidth(3f);
            GL.Begin(PrimitiveType.LineStrip);
            {
                for (int i = 0; i < points.Count; i++)
                {
                    GL.Vertex2(points[i].X, points[i].Y);
                }
                GL.Vertex2(currentX, currentY);
            }
            GL.End();
            foreach (var point in points)
            {
                Utilities.DrawArc(point.X, point.Y, 3, 0, 360, true);
            }
            if (points.Count >= 3 && points[0].IsCloseEnough(currentX, currentY))
            {
                Utilities.DrawRectangle(points[0].X, points[0].Y, 10, 10, false);
            }

        }

        public void Execute(int mouseX, int mouseY, ImageMapEditor ime)
        {
            if (points.Count >= 3 && points[0].IsCloseEnough(mouseX, mouseY))
            {
                var polygon = new ImageMapPolygon();
                foreach (var point in points)
                {
                    polygon.AddPoint(point);
                }
                ime.ImageMap.AddArea(polygon);
                Reset(ime);
            }
            else
            {
                points.Add(new ImageMapPoint(mouseX, mouseY));
            }
        }

        public void Reset(ImageMapEditor ime)
        {
            points.Clear();
        }
    }
}