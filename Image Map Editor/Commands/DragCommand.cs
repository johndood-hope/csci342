﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using OpenTK.Graphics.OpenGL;

namespace Image_Map_Editor
{
    public class DragCommand : IDragCommand
    {
        public void Draw(int startX, int startY, int currentX, int currentY, ImageMapEditor ime)
        {
            var dragging = ime.ImageMap.GetPrimitiveUnder(startX, startY);

            GL.Color3(Color.DarkGray);
            GL.LineWidth(3f);

            var point = dragging as ImageMapPoint;
            if (point != null)
            {
                int oldX = point.X;
                int oldY = point.Y;
                point.X = currentX;
                point.Y = currentY;
                ImageMapArea containingArea = null;
                foreach (var testArea in ime.ImageMap.Areas)
                {
                    if (testArea.DraggablePoints.Contains(point))
                    {
                        containingArea = testArea;
                        break;
                    }
                }
                
                containingArea?.Draw();

                point.X = oldX;
                point.Y = oldY;
            }

            var area = dragging as ImageMapArea;
            if (area != null)
            {
                area.Move(currentX-startX, currentY-startY);
                area.Draw();
                area.Move(startX-currentX, startY-currentY);
            }
        }

        public void Execute(int startX, int startY, int endX, int endY, ImageMapEditor ime)
        {
            var dragging = ime.ImageMap.GetPrimitiveUnder(startX, startY);

            var point = dragging as ImageMapPoint;
            if (point != null)
            { 
                point.X = endX;
                point.Y = endY;
            }

            var area = dragging as ImageMapArea;
            if (area != null)
            {
                area.Move(endX - startX, endY - startY);
            }
        }
    }
}