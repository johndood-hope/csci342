﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms.VisualStyles;
using AngleSharp.Parser.Html;
using OpenTK.Graphics.OpenGL;

namespace Image_Map_Editor
{
    public class ImageMap
    {
        public static ImageMap CreateFromFileName(string imageMapName)
        {
            var im = new ImageMap();
            var parser = new HtmlParser();
            try
            {
                using (var htmlSource = new FileStream(imageMapName, FileMode.Open))
                {
                    var document = parser.Parse(htmlSource);
                    var areas = document.QuerySelectorAll("area");

                    foreach (var area in areas)
                    {
                        string href = area.GetAttribute("href");
                        string alt = area.GetAttribute("alt");
                        var coordinates = area.GetAttribute("coords").Split(',').Select(int.Parse).ToList();
                        string type = area.GetAttribute("shape");
                        ImageMapArea toCreate = null;
                        if (type.Equals("circle"))
                        {
                            toCreate = ImageMapCircle.Create(coordinates);
                            
                        }
                        else if (type.Equals("rect"))
                        {
                            toCreate = ImageMapRectangle.Create(coordinates);
                            
                        }
                        else if (type.Equals("poly"))
                        {
                            toCreate = ImageMapPolygon.Create(coordinates);
                            
                        }
                        if (toCreate != null)
                        {
                            toCreate.Href = href;
                            toCreate.Alt = alt;
                            im.AddArea(toCreate);
                        }
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                im = new ImageMap();
            }
            return im;
        }

        public IList<ImageMapArea> Areas { get; }
        public ImageMapPrimitive Selected { get; set; }

        private ImageMap()
        {
            Areas = new List<ImageMapArea>();
        }

        public void AddArea(ImageMapArea areaToAdd)
        {
            Areas.Add(areaToAdd);
        }

        public void RemoveArea(ImageMapArea areaToRemove)
        {
            Areas.Remove(areaToRemove);
        }

        public void Draw()
        {
            foreach(ImageMapArea area in Areas)
            {
                GL.Color3(Color.Black);
                GL.LineWidth(2f);
                area.Draw();
            }

        }

        public ImageMapPrimitive GetPrimitiveUnder(int x, int y)
        {
            foreach (var area in Areas)
            {
                foreach (var point in area.DraggablePoints)
                {
                    if (point.IsCloseEnough(x, y))
                    {
                        return point;
                    }
                }
            }
            foreach (var area in Areas)
            {
                if (area.CloseEnoughToBorder(x, y))
                {
                    return area;
                }
            }
            return null;
        }

        public string HtmlForm()
        {
            var sb = new StringBuilder();
            sb.Append("<map>\n");
            foreach (var area in Areas)
            {
                sb.Append(area.HtmlForm());
            }
            sb.Append("</map>\n");
            return sb.ToString();
        }
    }
}