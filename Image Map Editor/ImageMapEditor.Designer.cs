﻿namespace Image_Map_Editor
{
    partial class ImageMapEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imageArea = new OpenTK.GLControl();
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.fileMenuItem = new System.Windows.Forms.MenuItem();
            this.openImageMenuItem = new System.Windows.Forms.MenuItem();
            this.openImageMapMenuItem = new System.Windows.Forms.MenuItem();
            this.saveImageMapMenuItem = new System.Windows.Forms.MenuItem();
            this.quitMenuItem = new System.Windows.Forms.MenuItem();
            this.editMenuItem = new System.Windows.Forms.MenuItem();
            this.selectModeMenuItem = new System.Windows.Forms.MenuItem();
            this.drawCircleMenuItem = new System.Windows.Forms.MenuItem();
            this.drawRectangleMenuItem = new System.Windows.Forms.MenuItem();
            this.drawPolygonMenuItem = new System.Windows.Forms.MenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.altTextBox = new System.Windows.Forms.TextBox();
            this.altLabel = new System.Windows.Forms.Label();
            this.hrefTextBox = new System.Windows.Forms.TextBox();
            this.hrefLabel = new System.Windows.Forms.Label();
            this.openImageDialog = new System.Windows.Forms.OpenFileDialog();
            this.openImageMapDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveImageMapDialog = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageArea
            // 
            this.imageArea.BackColor = System.Drawing.Color.Black;
            this.imageArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageArea.Location = new System.Drawing.Point(0, 0);
            this.imageArea.Name = "imageArea";
            this.imageArea.Size = new System.Drawing.Size(284, 261);
            this.imageArea.TabIndex = 0;
            this.imageArea.VSync = false;
            this.imageArea.Load += new System.EventHandler(this.imageArea_Load);
            this.imageArea.Paint += new System.Windows.Forms.PaintEventHandler(this.imageArea_Paint);
            this.imageArea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.imageArea_KeyDown);
            this.imageArea.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imageArea_MouseDown);
            this.imageArea.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imageArea_MouseMove);
            this.imageArea.MouseUp += new System.Windows.Forms.MouseEventHandler(this.imageArea_MouseUp);
            // 
            // mainMenu
            // 
            this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.fileMenuItem,
            this.editMenuItem});
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.Index = 0;
            this.fileMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.openImageMenuItem,
            this.openImageMapMenuItem,
            this.saveImageMapMenuItem,
            this.quitMenuItem});
            this.fileMenuItem.Text = "File";
            // 
            // openImageMenuItem
            // 
            this.openImageMenuItem.Index = 0;
            this.openImageMenuItem.Text = "Open Image";
            this.openImageMenuItem.Click += new System.EventHandler(this.openImageMenuItem_Click);
            // 
            // openImageMapMenuItem
            // 
            this.openImageMapMenuItem.Index = 1;
            this.openImageMapMenuItem.Text = "Open Image Map";
            this.openImageMapMenuItem.Click += new System.EventHandler(this.openImageMapMenuItem_Click);
            // 
            // saveImageMapMenuItem
            // 
            this.saveImageMapMenuItem.Index = 2;
            this.saveImageMapMenuItem.Text = "Save Image Map";
            this.saveImageMapMenuItem.Click += new System.EventHandler(this.saveImageMapMenuItem_Click);
            // 
            // quitMenuItem
            // 
            this.quitMenuItem.Index = 3;
            this.quitMenuItem.Text = "Quit";
            this.quitMenuItem.Click += new System.EventHandler(this.quitMenuItem_Click);
            // 
            // editMenuItem
            // 
            this.editMenuItem.Index = 1;
            this.editMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.selectModeMenuItem,
            this.drawCircleMenuItem,
            this.drawRectangleMenuItem,
            this.drawPolygonMenuItem});
            this.editMenuItem.Text = "Edit";
            // 
            // selectModeMenuItem
            // 
            this.selectModeMenuItem.Index = 0;
            this.selectModeMenuItem.Text = "Select Mode";
            this.selectModeMenuItem.Click += new System.EventHandler(this.selectModeMenuItem_Click);
            // 
            // drawCircleMenuItem
            // 
            this.drawCircleMenuItem.Index = 1;
            this.drawCircleMenuItem.Text = "Draw Circle";
            this.drawCircleMenuItem.Click += new System.EventHandler(this.drawCircleMenuItem_Click);
            // 
            // drawRectangleMenuItem
            // 
            this.drawRectangleMenuItem.Index = 2;
            this.drawRectangleMenuItem.Text = "Draw Rectangle";
            this.drawRectangleMenuItem.Click += new System.EventHandler(this.drawRectangleMenuItem_Click);
            // 
            // drawPolygonMenuItem
            // 
            this.drawPolygonMenuItem.Index = 3;
            this.drawPolygonMenuItem.Text = "Draw Polygon";
            this.drawPolygonMenuItem.Click += new System.EventHandler(this.drawPolygonMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.altTextBox);
            this.panel1.Controls.Add(this.altLabel);
            this.panel1.Controls.Add(this.hrefTextBox);
            this.panel1.Controls.Add(this.hrefLabel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 235);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(284, 26);
            this.panel1.TabIndex = 1;
            // 
            // altTextBox
            // 
            this.altTextBox.Location = new System.Drawing.Point(170, 3);
            this.altTextBox.Name = "altTextBox";
            this.altTextBox.Size = new System.Drawing.Size(100, 20);
            this.altTextBox.TabIndex = 3;
            this.altTextBox.TextChanged += new System.EventHandler(this.altTextBox_TextChanged);
            // 
            // altLabel
            // 
            this.altLabel.AutoSize = true;
            this.altLabel.Location = new System.Drawing.Point(143, 6);
            this.altLabel.Name = "altLabel";
            this.altLabel.Size = new System.Drawing.Size(21, 13);
            this.altLabel.TabIndex = 2;
            this.altLabel.Text = "alt:";
            // 
            // hrefTextBox
            // 
            this.hrefTextBox.Location = new System.Drawing.Point(37, 3);
            this.hrefTextBox.Name = "hrefTextBox";
            this.hrefTextBox.Size = new System.Drawing.Size(100, 20);
            this.hrefTextBox.TabIndex = 1;
            this.hrefTextBox.TextChanged += new System.EventHandler(this.hrefTextBox_TextChanged);
            // 
            // hrefLabel
            // 
            this.hrefLabel.AutoSize = true;
            this.hrefLabel.Location = new System.Drawing.Point(3, 6);
            this.hrefLabel.Name = "hrefLabel";
            this.hrefLabel.Size = new System.Drawing.Size(28, 13);
            this.hrefLabel.TabIndex = 0;
            this.hrefLabel.Text = "href:";
            // 
            // openImageDialog
            // 
            this.openImageDialog.Filter = "Image Files|*.jpeg;*.jpg;*.png;*.bmp;*.gif|All Files|*.*";
            this.openImageDialog.Title = "Select an Image";
            // 
            // openImageMapDialog
            // 
            this.openImageMapDialog.FileName = "openFileDialog1";
            this.openImageMapDialog.Filter = "HTML Files (*.htm *.html)|*.htm; *.html|All Files (*.*)|*.*";
            this.openImageMapDialog.Title = "Select an Image Map";
            // 
            // saveImageMapDialog
            // 
            this.saveImageMapDialog.Filter = "HTML Files (*.htm *.html)|*.htm; *.html|All Files (*.*)|*.*";
            // 
            // ImageMapEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.imageArea);
            this.Menu = this.mainMenu;
            this.Name = "ImageMapEditor";
            this.Text = "Image Map Editor";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private OpenTK.GLControl imageArea;
        private System.Windows.Forms.MainMenu mainMenu;
        private System.Windows.Forms.MenuItem fileMenuItem;
        private System.Windows.Forms.MenuItem openImageMenuItem;
        private System.Windows.Forms.MenuItem openImageMapMenuItem;
        private System.Windows.Forms.MenuItem saveImageMapMenuItem;
        private System.Windows.Forms.MenuItem quitMenuItem;
        private System.Windows.Forms.MenuItem editMenuItem;
        private System.Windows.Forms.MenuItem selectModeMenuItem;
        private System.Windows.Forms.MenuItem drawCircleMenuItem;
        private System.Windows.Forms.MenuItem drawRectangleMenuItem;
        private System.Windows.Forms.MenuItem drawPolygonMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox altTextBox;
        private System.Windows.Forms.Label altLabel;
        private System.Windows.Forms.TextBox hrefTextBox;
        private System.Windows.Forms.Label hrefLabel;
        private System.Windows.Forms.OpenFileDialog openImageDialog;
        private System.Windows.Forms.OpenFileDialog openImageMapDialog;
        private System.Windows.Forms.SaveFileDialog saveImageMapDialog;
    }
}

