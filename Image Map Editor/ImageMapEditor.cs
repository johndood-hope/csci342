﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Image_Map_Editor
{
    public partial class ImageMapEditor : Form
    {
        public string CmdLineFileName { get; }
        private PictureDrawer pictureDrawer;
        private string imageFileName;

        public string ImageFileName
        {
            get { return imageFileName; }
            set
            {
                imageFileName = value;
                string shortName = imageFileName.Substring(0, imageFileName.LastIndexOf('.'));
                ImageMapFileName = shortName + ".html";
                pictureDrawer = new PictureDrawer(imageFileName);
                Width += pictureDrawer.Width - imageArea.Width;
                Height += pictureDrawer.Height - imageArea.Height;
                Utilities.SetWindow(0, pictureDrawer.Width, 0, pictureDrawer.Height);
                GL.Viewport(0, 0, imageArea.Width, imageArea.Height);
            }
        }

        private string imageMapFileName;

        public string ImageMapFileName
        {
            get { return imageMapFileName; }
            set
            {
                imageMapFileName = value;
                ImageMap = ImageMap.CreateFromFileName(imageMapFileName);
            }
        }

        public IDragCommand DragCommand { get; } = new DragCommand();
        private IClickCommand clickCommand;

        public IClickCommand ClickCommand
        {
            get { return clickCommand; }
            set
            {
                clickCommand?.Reset(this);
                clickCommand = value;
            }
        }

        public IClickCommand SelectCommand { get; } = new SelectClickCommand();
        public IClickCommand DrawCircle { get; } = new DrawCircleClickCommand();
        public IClickCommand DrawRectangle { get; } = new DrawRectangleClickCommand();
        public IClickCommand DrawPolygon { get; } = new DrawPolygonClickCommand();

        public ImageMap ImageMap { get; set; }

        public ImageMapEditor(string cmdlinefileName)
        {
            CmdLineFileName = cmdlinefileName;
            ClickCommand = SelectCommand;
            InitializeComponent();
        }

        private ImageMapArea oldArea;

        public void SelectedChanged()
        {

            var area = ImageMap.Selected as ImageMapArea;
            if (oldArea != null)
            {
                oldArea.Href = hrefTextBox.Text;
                oldArea.Alt = altTextBox.Text;
            }
            oldArea = area;
            if (area != null)
            {
                altTextBox.Text = area.Alt;
                altTextBox.Enabled = true;
                hrefTextBox.Text = area.Href;
                hrefTextBox.Enabled = true;
            }
            else
            {
                altTextBox.Enabled = false;
                altTextBox.Text = "";
                hrefTextBox.Enabled = false;
                hrefTextBox.Text = "";
            }
        }

        private void imageArea_Load(object sender, EventArgs e)
        {
            GL.ClearColor(Color.White);

            if (CmdLineFileName != null)
            {
                ImageFileName = CmdLineFileName;
            }
            else if (openImageDialog.ShowDialog() == DialogResult.OK)
            {
                ImageFileName = openImageDialog.FileName;
            }
            else
            {
                var oldTitle = openImageDialog.Title;
                openImageDialog.Title = "You Must " + oldTitle;
                if (openImageDialog.ShowDialog() == DialogResult.OK)
                {
                    ImageFileName = openImageDialog.FileName;
                    openImageDialog.Title = oldTitle;
                    
                }
                else
                {
                    Application.Exit();
                    return;
                }
            }

            SelectedChanged();
        }

        private void imageArea_Paint(object sender, PaintEventArgs e)
        {
            if (!imageArea.IsHandleCreated)
                return;
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            pictureDrawer.display();
            GL.MatrixMode(MatrixMode.Projection);
            GL.PushMatrix();
            {
                GL.Translate(0, imageArea.Height, 0);
                GL.Scale(1, -1, 1);
                ImageMap.Draw();
                if (mouseDown)
                {
                    DragCommand.Draw(downX, downY, mouseX, mouseY, this);
                }
                else
                {
                    ClickCommand.Draw(mouseX, mouseY, this);
                }
            }
            GL.PopMatrix();
            imageArea.SwapBuffers();
        }

        private bool mouseDown = false;
        private int downX, downY;
        private int mouseX, mouseY;

        private void imageArea_MouseDown(object sender, MouseEventArgs e)
        {
            imageArea.Focus();
            downX = e.X;
            downY = e.Y;
            mouseDown = true;
        }

        private void imageArea_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
            double dx = e.X - downX, dy = e.Y - downY;
            if (Math.Sqrt(dx * dx + dy * dy) < 3)
            {
                //Click
                ClickCommand.Execute(downX, downY, this);
            }
            else
            {
                //Drag
                DragCommand.Execute(downX, downY, e.X, e.Y, this);
            }
            imageArea.Invalidate();
        }

        private void imageArea_MouseMove(object sender, MouseEventArgs e)
        {
            mouseX = e.X;
            mouseY = e.Y;
            imageArea.Invalidate();
        }

        private void imageArea_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                var point = ImageMap.Selected as ImageMapPoint;
                if (point != null)
                {
                    foreach (var testArea in ImageMap.Areas)
                    {
                        if (testArea.DraggablePoints.Contains(point))
                        {
                            var polygon = testArea as ImageMapPolygon;
                            if (polygon != null)
                            {
                                polygon.DeletePoint(point);
                                ClickCommand.Reset(this);
                            }
                            break;
                        }
                    }
                }

                var area = ImageMap.Selected as ImageMapArea;
                if (area != null)
                {
                    ImageMap.RemoveArea(area);
                    ClickCommand.Reset(this);
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                ClickCommand = SelectCommand;
            }
            imageArea.Invalidate();
        }

        private void openImageMenuItem_Click(object sender, EventArgs e)
        {
            if (openImageDialog.ShowDialog() == DialogResult.OK)
            {
                ImageFileName = openImageDialog.FileName;
            }
        }

        private void openImageMapMenuItem_Click(object sender, EventArgs e)
        {
            if (openImageMapDialog.ShowDialog() == DialogResult.OK)
            {
                ImageMapFileName = openImageMapDialog.FileName;
            }
        }

        private void saveImageMapMenuItem_Click(object sender, EventArgs e)
        {
            saveImageMapDialog.FileName = ImageMapFileName;
            if (saveImageMapDialog.ShowDialog() == DialogResult.OK)
            {
                using (var output = new StreamWriter(saveImageMapDialog.FileName))
                {
                    output.Write(ImageMap.HtmlForm());
                    
                }
            }
        }

        private void quitMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void selectModeMenuItem_Click(object sender, EventArgs e)
        {
            ClickCommand = SelectCommand;
        }

        private void drawCircleMenuItem_Click(object sender, EventArgs e)
        {
            ClickCommand = DrawCircle;
        }

        private void drawRectangleMenuItem_Click(object sender, EventArgs e)
        {
            ClickCommand = DrawRectangle;
        }

        private void drawPolygonMenuItem_Click(object sender, EventArgs e)
        {
            ClickCommand = DrawPolygon;
        }

        public void hrefTextBox_TextChanged(object sender, EventArgs e)
        {
            var area = ImageMap.Selected as ImageMapArea;
            if (area != null)
            {
                area.Href = hrefTextBox.Text;
            }
        }

        public void altTextBox_TextChanged(object sender, EventArgs e)
        {
            var area = ImageMap.Selected as ImageMapArea;
            if (area != null)
            {
                area.Alt = altTextBox.Text;
            }
        }
    }


}
