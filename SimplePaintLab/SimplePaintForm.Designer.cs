﻿namespace SimplePaintLab
{
    partial class SimplePaintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.plotArea = new OpenTK.GLControl();
            this.SuspendLayout();
            // 
            // plotArea
            // 
            this.plotArea.BackColor = System.Drawing.Color.Black;
            this.plotArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plotArea.Location = new System.Drawing.Point(0, 0);
            this.plotArea.Name = "plotArea";
            this.plotArea.Size = new System.Drawing.Size(284, 261);
            this.plotArea.TabIndex = 0;
            this.plotArea.VSync = false;
            this.plotArea.Load += new System.EventHandler(this.plotArea_Load);
            this.plotArea.Paint += new System.Windows.Forms.PaintEventHandler(this.plotArea_Paint);
            this.plotArea.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.plotArea_KeyPress);
            this.plotArea.MouseClick += new System.Windows.Forms.MouseEventHandler(this.plotArea_MouseClick);
            this.plotArea.MouseMove += new System.Windows.Forms.MouseEventHandler(this.plotArea_MouseMove);
            this.plotArea.Resize += new System.EventHandler(this.plotArea_Resize);
            // 
            // SimplePaintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.plotArea);
            this.Name = "SimplePaintForm";
            this.Text = "Paint";
            this.ResumeLayout(false);

        }

        #endregion

        private OpenTK.GLControl plotArea;
    }
}

