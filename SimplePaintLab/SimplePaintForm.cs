﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace SimplePaintLab
{
    public partial class SimplePaintForm : Form
    {
        private bool controlLoaded = false;

        private readonly PolylineCollection pc = new PolylineCollection();
        private Polyline current = null;
        private Point2D mouseLoc;
        private bool previewMode = false;
        private bool PreviewMode
        {
            get { return previewMode; }
            set {
                Text = value ? "Paint : Line Preview Mode" : "Paint";
                previewMode = value;
            }
        }

        public SimplePaintForm()
        {
            InitializeComponent();
        }

        private void plotArea_Resize(object sender, EventArgs e)
        {
            if (!controlLoaded)
                return;
            SetViewport();
        }

        private void plotArea_Load(object sender, EventArgs e)
        {
            GL.ClearColor(Color.White);

            Utilities.SetWindow(0, 1, 1, 0);

            SetViewport();
            DrawingTools.enableDefaultAntiAliasing();
            controlLoaded = true;
        }

        private void SetViewport()
        {
            GL.Viewport(0, 0, plotArea.Width, plotArea.Height);
        }

        private void plotArea_Paint(object sender, PaintEventArgs e)
        {
            if (!controlLoaded)
                return;
            GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit);
            GL.Color3(Color.Black);

            pc.Draw(PrimitiveType.LineStrip);

            if (PreviewMode)
            {
                Point2D lastPoint = current?.LastPoint;
                if (lastPoint != null && mouseLoc != null)
                {
                    GL.Begin(PrimitiveType.Lines);
                    {
                        GL.Vertex2(lastPoint.X, lastPoint.Y);
                        GL.Vertex2(mouseLoc.X, mouseLoc.Y);
                    }
                    GL.End();
                }
            }


            plotArea.SwapBuffers();
        }

        private void plotArea_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 'q')
                Application.Exit();
            if (e.KeyChar == 'p')
                PreviewMode = !PreviewMode;
        }

        private void plotArea_MouseClick(object sender, MouseEventArgs e)
        {
            Point2D newPoint = ViewToWorld(e.X, e.Y);
            Console.WriteLine("Point clicked is ({0}, {1}) ({2}, {3})", e.X, e.Y, newPoint.X, newPoint.Y);
            if (current == null)
            {
                pc.AddPolyline(current = new Polyline());
            }
            current.AddPoint(newPoint);
            if (e.Button == MouseButtons.Right)
                current = null;
            plotArea.Invalidate();
        }

        private Point2D ViewToWorld(double x, double y)
        {
            return new Point2D(x/plotArea.Width, y/plotArea.Height);
        }

        private void plotArea_MouseMove(object sender, MouseEventArgs e)
        {
            if (!controlLoaded)
                return;
            mouseLoc = ViewToWorld(e.X, e.Y);
            plotArea.Invalidate();
        }
    }
}
