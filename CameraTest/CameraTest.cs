﻿using System;
using csci342;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace CameraTest
{
    class MatrixLoader : IMatrixLoader
    {
        public double[] m;
        public MatrixMode mode;

        public MatrixLoader()
        {
            m=new double[16];
        }

        public void MatrixMode(MatrixMode newMode)
        {
            mode = newMode;
        }

        public void LoadMatrix(double[] newValues)
        {
            Array.Copy(newValues, m, 16);
        }
    }

    [TestClass]
    public class CameraTest
    {
        private MatrixLoader matrixLoader;

        [TestInitialize]
        public void CreateMatrixLoader()
        {
            matrixLoader = new MatrixLoader();
        }

        [TestMethod]
        public void ConstructorSetsVectorsCorrectly()
        {
            var eye = new Point3D(5, 2, 3);
            var look = new Point3D(0, 2, 3);

            var camera = new Camera(eye, look, matrixLoader);

            var expectedN = new Vector3(1, 0, 0);
            var expectedV = new Vector3(0, 1, 0);
            var expectedU = new Vector3(0, 0, -1);
            Assert.AreEqual(expectedN, camera.N);
            Assert.AreEqual(expectedV, camera.V);
            Assert.AreEqual(expectedU, camera.U);
        }

        [TestMethod]
        public void ConstructorSetModelViewMatrixCorrectly()
        {
            var eye = new Point3D(5, 2, 3);
            var look = new Point3D(0, 2, 3);

            var camera = new Camera(eye, look, matrixLoader);

            Assert.AreEqual(MatrixMode.Modelview, matrixLoader.mode);
            double[] actualMatrix = matrixLoader.m;
            Assert.AreEqual(0, actualMatrix[0]);
            Assert.AreEqual(0, actualMatrix[4]);
            Assert.AreEqual(-1, actualMatrix[8]);
            Assert.AreEqual(3, actualMatrix[12]);

            Assert.AreEqual(0, actualMatrix[1]);
            Assert.AreEqual(1, actualMatrix[5]);
            Assert.AreEqual(0, actualMatrix[9]);
            Assert.AreEqual(-2, actualMatrix[13]);

            Assert.AreEqual(1, actualMatrix[2]);
            Assert.AreEqual(0, actualMatrix[6]);
            Assert.AreEqual(0, actualMatrix[10]);
            Assert.AreEqual(-5, actualMatrix[14]);

            Assert.AreEqual(0, actualMatrix[3]);
            Assert.AreEqual(0, actualMatrix[7]);
            Assert.AreEqual(0, actualMatrix[11]);
            Assert.AreEqual(1, actualMatrix[15]);
        }

        [TestMethod]
        public void ForwardMethodWorksCorrectly()
        {
            var eye = new Point3D(5, 2, 3);
            var look = new Point3D(0, 2, 3);

            var camera = new Camera(eye, look, matrixLoader);

            camera.Forward(5);

            Assert.AreEqual(0, camera.Eye.X);
            Assert.AreEqual(2, camera.Eye.Y);
            Assert.AreEqual(3, camera.Eye.Z);

            Assert.AreEqual(-5, camera.Look.X);
            Assert.AreEqual(2, camera.Look.Y);
            Assert.AreEqual(3, camera.Look.Z);

            double[] actualMatrix = matrixLoader.m;
            Assert.AreEqual(0, actualMatrix[0]);
            Assert.AreEqual(0, actualMatrix[4]);
            Assert.AreEqual(-1, actualMatrix[8]);
            Assert.AreEqual(3, actualMatrix[12]);

            Assert.AreEqual(0, actualMatrix[1]);
            Assert.AreEqual(1, actualMatrix[5]);
            Assert.AreEqual(0, actualMatrix[9]);
            Assert.AreEqual(-2, actualMatrix[13]);

            Assert.AreEqual(1, actualMatrix[2]);
            Assert.AreEqual(0, actualMatrix[6]);
            Assert.AreEqual(0, actualMatrix[10]);
            Assert.AreEqual(0, actualMatrix[14]);

            Assert.AreEqual(0, actualMatrix[3]);
            Assert.AreEqual(0, actualMatrix[7]);
            Assert.AreEqual(0, actualMatrix[11]);
            Assert.AreEqual(1, actualMatrix[15]);
        }

        [TestMethod]
        public void RightMethodWorksCorrectly()
        {
            var eye = new Point3D(5, 2, 3);
            var look = new Point3D(0, 2, 3);

            var camera = new Camera(eye, look, matrixLoader);

            camera.Right(-2);

            Assert.AreEqual(5, camera.Eye.X);
            Assert.AreEqual(2, camera.Eye.Y);
            Assert.AreEqual(5, camera.Eye.Z);

            Assert.AreEqual(0, camera.Look.X);
            Assert.AreEqual(2, camera.Look.Y);
            Assert.AreEqual(5, camera.Look.Z);

            double[] actualMatrix = matrixLoader.m;
            Assert.AreEqual(0, actualMatrix[0]);
            Assert.AreEqual(0, actualMatrix[4]);
            Assert.AreEqual(-1, actualMatrix[8]);
            Assert.AreEqual(5, actualMatrix[12]);

            Assert.AreEqual(0, actualMatrix[1]);
            Assert.AreEqual(1, actualMatrix[5]);
            Assert.AreEqual(0, actualMatrix[9]);
            Assert.AreEqual(-2, actualMatrix[13]);

            Assert.AreEqual(1, actualMatrix[2]);
            Assert.AreEqual(0, actualMatrix[6]);
            Assert.AreEqual(0, actualMatrix[10]);
            Assert.AreEqual(-5, actualMatrix[14]);

            Assert.AreEqual(0, actualMatrix[3]);
            Assert.AreEqual(0, actualMatrix[7]);
            Assert.AreEqual(0, actualMatrix[11]);
            Assert.AreEqual(1, actualMatrix[15]);
        }

        [TestMethod]
        public void UpMethodWorksCorrectly()
        {
            var eye = new Point3D(5, 2, 3);
            var look = new Point3D(0, 2, 3);

            var camera = new Camera(eye, look, matrixLoader);

            camera.Up(1);

            Assert.AreEqual(5, camera.Eye.X);
            Assert.AreEqual(3, camera.Eye.X);
            Assert.AreEqual(3, camera.Eye.X);

            Assert.AreEqual(0, camera.Look.X);
            Assert.AreEqual(3, camera.Look.Y);
            Assert.AreEqual(3, camera.Look.Y);

            double[] actualMatrix = matrixLoader.m;
            Assert.AreEqual(0, actualMatrix[0]);
            Assert.AreEqual(0, actualMatrix[4]);
            Assert.AreEqual(-1, actualMatrix[8]);
            Assert.AreEqual(3, actualMatrix[12]);

            Assert.AreEqual(0, actualMatrix[1]);
            Assert.AreEqual(1, actualMatrix[5]);
            Assert.AreEqual(0, actualMatrix[9]);
            Assert.AreEqual(-3, actualMatrix[13]);

            Assert.AreEqual(1, actualMatrix[2]);
            Assert.AreEqual(0, actualMatrix[6]);
            Assert.AreEqual(0, actualMatrix[10]);
            Assert.AreEqual(-5, actualMatrix[14]);

            Assert.AreEqual(0, actualMatrix[3]);
            Assert.AreEqual(0, actualMatrix[7]);
            Assert.AreEqual(0, actualMatrix[11]);
            Assert.AreEqual(1, actualMatrix[15]);
        }
    }
}
