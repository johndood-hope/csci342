﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Robot
{
    public partial class Robot : Form
    {
        public Robot()
        {
            InitializeComponent();
        }

        private void drawingArea_Load(object sender, EventArgs e)
        {
            DrawingTools.enableDefaultAntiAliasing();
            Utilities.SetWindow(-10, 10, -10, 10);
            GL.ClearColor(Color.White);
        }

        private void drawingArea_Paint(object sender, PaintEventArgs e)
        {
            if (!drawingArea.IsHandleCreated)
            {
                return;
            }

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Color3(Color.Black);

            GLU.Instance.Perspective(60, 1, 1, 40);
            var eye = new Point3D(-3, 2, 10);
            var look = new Point3D(0,0,0);
            GLU.Instance.LookAt(eye, look);
            
            GL.MatrixMode(MatrixMode.Modelview);

            GL.PushMatrix();
            {
                GL.Rotate(rotateAngle, 0, 1, 0);
                GL.Translate(0, 4, 0);

                GL.PushMatrix();
                {
                    DrawInseam();
                    DrawLeg();
                    GL.Scale(-1, 1, 1);
                    DrawLeg();
                }
                GL.PopMatrix();

                
                GL.Translate(0, -3, 0);
                GL.Rotate(waistRotate, 1, 0, 0);
                GL.Translate(0, 3, 0);

                DrawTorso();

                GL.PushMatrix();
                {
                    GL.Rotate(headRotate, 0, 1, 0);
                    DrawHead();
                }
                GL.PopMatrix();

                // Draw one arm and the redraw it rotated around
                GL.PushMatrix();
                {
                    // Right Arm
                    GL.Translate(0, -2, 0);
                    GL.Rotate(rightArmRotate, -1, 0, 0);
                    DrawArm(rightElbowRotate);
                }
                GL.PopMatrix();

                GL.PushMatrix();
                {
                    // Left arm
                    GL.Rotate(180, 0, 1, 0);
                    GL.Translate(0, -2, 0);
                    GL.Rotate(leftArmRotate, 1, 0, 0);
                    DrawArm(leftElbowRotate);
                }
                GL.PopMatrix();
            }
            GL.PopMatrix();

            drawingArea.SwapBuffers();
        }

        private void DrawLeg()
        {
            GL.PushMatrix();
            {
                GL.Translate(-1, -4, 0);
                GLU.Instance.DrawCube();
                for (int i = 0; i < 5; i++)
                {
                    GL.Translate(0, -1, 0);
                    GLU.Instance.DrawCube();
                }

                GL.Translate(0,0,1);
                GLU.Instance.DrawCube();
                GL.Translate(-1, 0, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0,0,-1);
                GLU.Instance.DrawCube();
            }
            GL.PopMatrix();
        }

        private void DrawArm(int elbowRotation)
        {
            GL.PushMatrix();
            {
                for (int i = 0; i < 2; i++)
                {
                    GL.Translate(-1, 0, 0);
                    GLU.Instance.DrawCube();
                }

                GL.Rotate(elbowRotation, 1, 0, 0);

                GL.Translate(-1, 0, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, 1, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, 1, 0);
                GLU.Instance.DrawCube();
            }
            GL.PopMatrix();
        }

        private void DrawInseam()
        {
            GL.PushMatrix();
            {
                GL.Translate(0, -3, 0);
                for (int i = 0; i < 4; i++)
                {
                    GL.Translate(0, -1, 0);
                    GLU.Instance.DrawCube();
                }
            }
            GL.PopMatrix();
        }

        private void DrawTorso()
        {
            GL.PushMatrix();
            {
                for (int i = 0; i < 3; i++)
                {
                    GL.Translate(0, -1, 0);
                    GLU.Instance.DrawCube();
                }
            }
            GL.PopMatrix();
        }

        private void DrawHead()
        {
            // Center of the head
            GLU.Instance.DrawCube();

            DrawHeadCorner();
            GL.PushMatrix();
            {
                GL.Rotate(90, 0, 1, 0);
                DrawHeadCorner();
                GL.Rotate(90, 0, 1, 0);
                DrawHeadCorner();
                GL.Rotate(90, 0, 1, 0);
                DrawHeadCorner();
            }
            GL.PopMatrix();
        }

        public void DrawHeadCorner()
        {
            GL.PushMatrix();
            {
                GL.Translate(-1, 0, 0);
                GLU.Instance.DrawCube();
                GL.Translate(0, 0, 1);
                GLU.Instance.DrawCube();
            }
            GL.PopMatrix();
        }

        private void drawingArea_Resize(object sender, EventArgs e)
        {
            GL.Viewport(0, 0, drawingArea.Width, drawingArea.Height);
            drawingArea.Invalidate();
        }

        // HELPER CODE

        #region Constants
        public const char BODY_POSITIVE = 'b';
        public const char BODY_NEGATIVE = 'B';
        public const char LEFT_ARM_POSITIVE = 'l';
        public const char LEFT_ARM_NEGATIVE = 'L';
        public const char LEFT_ELBOW_POSITIVE = 'e';
        public const char LEFT_ELBOW_NEGATIVE = 'E';
        public const char RIGHT_ELBOW_POSITIVE = 'x';
        public const char RIGHT_ELBOW_NEGATIVE = 'X';
        public const char RIGHT_ARM_POSITIVE = 'r';
        public const char RIGHT_ARM_NEGATIVE = 'R';
        public const char WAIST_POSITIVE = 'w';
        public const char WAIST_NEGATIVE = 'W';
        public const char HEAD_POSITIVE = 'h';
        public const char HEAD_NEGATIVE = 'H';
        #endregion

        #region Control Variables
        private int rotateAngle = 0;
        private int leftArmRotate = 0;
        private int rightArmRotate = 0;
        private int leftElbowRotate = 0;
        private int rightElbowRotate = 0;
        private int headRotate = 0;
        private int waistRotate = 0;
        #endregion


        private void HandleKeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case 'q':
                case 'Q':
                    Application.Exit();
                    break;

                //  Rotate left arm in positive direction
                case LEFT_ARM_POSITIVE:
                    leftArmRotate++;
                    break;

                //  Rotate left arm in negative direction
                case LEFT_ARM_NEGATIVE:
                    leftArmRotate--;
                    break;

                //  Rotate right arm in positive direction
                case RIGHT_ARM_POSITIVE:
                    rightArmRotate++;
                    break;

                //  Rotate right arm in negative direction
                case RIGHT_ARM_NEGATIVE:
                    rightArmRotate--;
                    break;

                //  Rotate left elbow in positive direction
                case LEFT_ELBOW_POSITIVE:
                    leftElbowRotate++;
                    break;

                //  Rotate left elbow in negative direction
                case LEFT_ELBOW_NEGATIVE:
                    leftElbowRotate--;
                    break;

                //  Rotate right elbow in positive direction
                case RIGHT_ELBOW_POSITIVE:
                    rightElbowRotate++;
                    break;

                //  Rotate right elbow in negative direction
                case RIGHT_ELBOW_NEGATIVE:
                    rightElbowRotate--;
                    break;

                //  Rotate head in positive direction
                case HEAD_POSITIVE:
                    headRotate++;
                    break;

                //  Rotate head in negative direction
                case HEAD_NEGATIVE:
                    headRotate--;
                    break;

                //  Rotate around neck in positive direction
                case WAIST_POSITIVE:
                    waistRotate++;
                    break;

                //  Rotate around neck in negative direction
                case WAIST_NEGATIVE:
                    waistRotate--;
                    break;

                //  Rotate the entire robot in positive direction
                case BODY_POSITIVE:
                    rotateAngle++;
                    break;

                //  Rotate the entire robot in negative direction
                case BODY_NEGATIVE:
                    rotateAngle--;
                    break;
            }

            drawingArea.Invalidate();
        }

        private void simpleOpenGlControl1_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.R) && ((e.Modifiers & Keys.Control) > 0))
            {
                Reset();
            }
        }

        public void Reset()
        {
            rotateAngle = 0;
            leftArmRotate = 0;
            rightArmRotate = 0;
            leftElbowRotate = 0;
            rightElbowRotate = 0;
            headRotate = 0;

            drawingArea.Invalidate();
        }
    }
}
