﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using csci342;

using OpenTK.Graphics.OpenGL;
namespace ChaosPlotter
{
    public partial class ChaosForm : Form
    {

        private double parseDouble(string text, double defaultValue)
        {
            double value;
            if (!double.TryParse(text, out value))
                value = defaultValue;
            return value;
        }

        private double parseDouble(string text, double defaultValue, double min, double max)
        {
            double value = parseDouble(text, defaultValue);
            if (value < min || value > max)
                value = defaultValue;
            return value;

        }

        private double f(double x)
        {
            return A * x * (1 - x);
        }

        
    }
}
