﻿namespace ChaosPlotter
{
    partial class ChaosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.numberIterations = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.points = new System.Windows.Forms.RadioButton();
            this.lines = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtA = new System.Windows.Forms.TextBox();
            this.lblA = new System.Windows.Forms.Label();
            this.plotArea = new OpenTK.GLControl();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numberIterations)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.numberIterations);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.btnNext);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Controls.Add(this.points);
            this.panel1.Controls.Add(this.lines);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtA);
            this.panel1.Controls.Add(this.lblA);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 302);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(315, 124);
            this.panel1.TabIndex = 0;
            // 
            // numberIterations
            // 
            this.numberIterations.Location = new System.Drawing.Point(214, 101);
            this.numberIterations.Name = "numberIterations";
            this.numberIterations.Size = new System.Drawing.Size(42, 20);
            this.numberIterations.TabIndex = 10;
            this.numberIterations.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(104, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Iterations per \"Next\"";
            // 
            // btnNext
            // 
            this.btnNext.Enabled = false;
            this.btnNext.Location = new System.Drawing.Point(55, 97);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(43, 23);
            this.btnNext.TabIndex = 8;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(6, 97);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(43, 23);
            this.btnStart.TabIndex = 7;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // points
            // 
            this.points.AutoSize = true;
            this.points.Location = new System.Drawing.Point(164, 77);
            this.points.Name = "points";
            this.points.Size = new System.Drawing.Size(141, 17);
            this.points.TabIndex = 6;
            this.points.TabStop = true;
            this.points.Text = "Display orbit using points";
            this.points.UseVisualStyleBackColor = true;
            this.points.CheckedChanged += new System.EventHandler(this.points_CheckedChanged);
            // 
            // lines
            // 
            this.lines.AutoSize = true;
            this.lines.Checked = true;
            this.lines.Location = new System.Drawing.Point(164, 54);
            this.lines.Name = "lines";
            this.lines.Size = new System.Drawing.Size(134, 17);
            this.lines.TabIndex = 5;
            this.lines.TabStop = true;
            this.lines.Text = "Display orbit using lines";
            this.lines.UseVisualStyleBackColor = true;
            this.lines.CheckedChanged += new System.EventHandler(this.lines_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Display Options";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(164, 29);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(117, 20);
            this.textBox1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Initial Value (between 0 and 1)";
            // 
            // txtA
            // 
            this.txtA.Location = new System.Drawing.Point(164, 3);
            this.txtA.Name = "txtA";
            this.txtA.Size = new System.Drawing.Size(117, 20);
            this.txtA.TabIndex = 1;
            this.txtA.TextChanged += new System.EventHandler(this.txtA_TextChanged);
            // 
            // lblA
            // 
            this.lblA.AutoSize = true;
            this.lblA.Location = new System.Drawing.Point(3, 6);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(111, 13);
            this.lblA.TabIndex = 0;
            this.lblA.Text = "Value of A (0 < A <=4)";
            // 
            // plotArea
            // 
            this.plotArea.BackColor = System.Drawing.Color.Black;
            this.plotArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plotArea.Location = new System.Drawing.Point(0, 0);
            this.plotArea.Name = "plotArea";
            this.plotArea.Size = new System.Drawing.Size(315, 302);
            this.plotArea.TabIndex = 1;
            this.plotArea.VSync = false;
            this.plotArea.Load += new System.EventHandler(this.plotArea_Load);
            this.plotArea.Paint += new System.Windows.Forms.PaintEventHandler(this.plotArea_Paint);
            this.plotArea.Resize += new System.EventHandler(this.plotArea_Resize);
            // 
            // ChaosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(315, 426);
            this.Controls.Add(this.plotArea);
            this.Controls.Add(this.panel1);
            this.Name = "ChaosForm";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numberIterations)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblA;
        private System.Windows.Forms.TextBox txtA;
        private OpenTK.GLControl plotArea;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numberIterations;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.RadioButton points;
        private System.Windows.Forms.RadioButton lines;
        private System.Windows.Forms.Label label2;
    }
}

