﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using csci342;

using OpenTK.Graphics.OpenGL;
namespace ChaosPlotter
{
    public partial class ChaosForm : Form
    {
        private bool controlLoaded = false;
        private IList<Point2D> orbit = new List<Point2D>();
        

        public ChaosForm()
        {
            InitializeComponent();
        }

        private void plotArea_Load(object sender, EventArgs e)
        {
            controlLoaded = true;
            GL.ClearColor(Color.White);
            GL.Color3(Color.Black);

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, 1, 0, 1, -1, 1);

            GL.PointSize(5);
            SetViewport();

        }

        private void plotArea_Resize(object sender, EventArgs e)
        {
            if (!controlLoaded)
                return;

            SetViewport();

        }

        private void SetViewport()
        {
            int min = plotArea.Width < plotArea.Height ? plotArea.Width : plotArea.Height;

            GL.Viewport((plotArea.Width - min) / 2, (plotArea.Height - min) / 2, min, min);
        }

        private void plotArea_Paint(object sender, PaintEventArgs e)
        {
            if (!controlLoaded)
                return;
            GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit);
            GL.Color3(Color.Black);
            //Function
            GL.Begin(PrimitiveType.LineStrip);
            {
                for(double x = 0; x < 1; x+=0.001)
                {
                    GL.Vertex2(x, f(x));
                }
            }
            GL.End();
            //Orbit
            if (!isLines)
                GL.Color3(Color.Red);
            GL.Begin(isLines ? PrimitiveType.LineStrip : PrimitiveType.Points);
            {
                Point2D point;
                for (int i = 0; i < orbit.Count; i+= isLines ? 1 : 2) 
                {
                    point = orbit[i + (isLines ? 0 : 1)];
                    GL.Vertex2(point.X, point.Y);
                }
            }
            GL.End();
            GL.Color3(Color.LightGray);
            GL.Begin(PrimitiveType.LineStrip);
            {
                GL.Vertex2(0, 0);
                GL.Vertex2(1, 1);
            }
            GL.End();
            double delta=0.0001;
            GL.Color3(Color.Blue);
            GL.Begin(PrimitiveType.LineLoop);
            {
                GL.Vertex2(delta, delta);
                GL.Vertex2(1 - delta, delta);
                GL.Vertex2(1 - delta, 1 - delta);
                GL.Vertex2(delta, 1 - delta);

            }
            GL.End();
            plotArea.SwapBuffers();
        }

        double A
        {
            get
            {
                return parseDouble(txtA.Text, 4.0, 0, 4.0);
            }
        }

        private void txtA_TextChanged(object sender, EventArgs e)
        {
            plotArea.Invalidate();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            orbit.Clear();
            btnNext.Enabled = true;
            double x = parseDouble(textBox1.Text, 0.5, 0, 1);
            orbit.Add(new Point2D(x, 0.0));
            orbit.Add(new Point2D(x, f(x)));
            plotArea.Invalidate();

        }

        private double NextCount
        {
            get
            {
                return (double) numberIterations.Value;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Console.WriteLine(NextCount);
            for (int i = 0; i < NextCount; i++)
            {
                Point2D lastPoint = orbit[orbit.Count - 1];

                orbit.Add(new Point2D(lastPoint.Y, lastPoint.Y));
                orbit.Add(new Point2D(lastPoint.Y, f(lastPoint.Y)));
            }
            plotArea.Invalidate();
        }

        private bool isLines = true;

        private void lines_CheckedChanged(object sender, EventArgs e)
        {
            isLines = true;
            plotArea.Invalidate() ;
        }

        private void points_CheckedChanged(object sender, EventArgs e)
        {
            isLines = false;
            plotArea.Invalidate();
        }
    }
}
