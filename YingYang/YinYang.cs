﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;
using Utilities = MyGraphicsLibrary.Utilities;

namespace YingYang
{
    public partial class YinYang : Form
    {
        private bool controlLoaded = false;
        private bool mercedes = false;

        public YinYang()
        {
            InitializeComponent();
        }

        private void plotArea_Load(object sender, EventArgs e)
        {
            GL.ClearColor(Color.White);
            
            Utilities.SetWindow(-1.1, 1.1, -1.1, 1.1);
            DrawingTools.enableDefaultAntiAliasing();

            SetViewport();
            controlLoaded = true;
        }

        private void plotArea_Resize(object sender, EventArgs e)
        {
            if (!controlLoaded)
                return;
            SetViewport();
            plotArea.Invalidate();
        }

        private void SetViewport()
        {
            int mindim = plotArea.Width < plotArea.Height ? plotArea.Width : plotArea.Height;
            GL.Viewport((plotArea.Width - mindim) / 2, (plotArea.Height - mindim) / 2, mindim, mindim);
        }

        private void plotArea_Paint(object sender, PaintEventArgs e)
        {
            if (!controlLoaded)
                return;

            GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit);

            if (!mercedes)
            {
                GL.Color3(Color.Black);
                Utilities.DrawArc(0, 0, 1, 0, 180, true);
                Utilities.DrawArc(-0.5, 0, 0.5, 180, 180, true);
                GL.Color3(Color.White);
                Utilities.DrawArc(0.5, 0, 0.5, 0, 180, true);
                Utilities.DrawArc(-0.5, 0, 0.1, 0, 360, true);
                GL.Color3(Color.Black);
                Utilities.DrawArc(0.5, 0, 0.1, 0, 360, true);
                Utilities.DrawArc(0, 0, 1, 0, 360);
            }
            else
            {
                GL.Color3(Color.Black);
                Utilities.DrawArc(0, 0, 1.05, 0, 380);
                Utilities.DrawArc(0, 0, 1, 0, 380);
                GL.Begin(PrimitiveType.LineStrip);
                {
                    Utilities.PlaceVertex(1, 90);
                    Utilities.PlaceVertex(0.1, 150);
                    Utilities.PlaceVertex(1, 210);
                    Utilities.PlaceVertex(0.1, 270);
                    Utilities.PlaceVertex(1, 330);
                    Utilities.PlaceVertex(0.1, 30);
                    Utilities.PlaceVertex(1, 90);
                }
                GL.End();
            }
            plotArea.SwapBuffers();
        }

        private void plotArea_KeyPress(object sender, KeyPressEventArgs e)
        {
            mercedes = !mercedes;
            plotArea.Invalidate();
        }
    }
}
