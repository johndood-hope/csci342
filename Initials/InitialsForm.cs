﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using MyGraphicsLibrary;
using OpenTK.Graphics.OpenGL;

namespace Initials
{
    public partial class InitialsForm : Form
    {
        private bool controlLoaded = false;

        private PolylineCollection polylineCollection;

        public InitialsForm()
        {
            InitializeComponent();
            SetupPolylines();
        }

        public void SetViewport()
        {
            GL.Viewport(0,0,plotArea.Width, plotArea.Height);
        }

        private void SetupPolylines()
        {
            Polyline j, c, d;
            j = new Polyline();
            c = new Polyline();
            d = new Polyline();
            j.AddPoint(new Point2D(0, 0));
            j.AddPoint(new Point2D(1, 0));
            j.AddPoint(new Point2D(0.5, 0));
            j.AddPoint(new Point2D(0.5, -0.75));
            j.AddPoint(new Point2D(0.25, -1));
            j.AddPoint(new Point2D(0, -0.75));
            c.AddPoints(
                2.5, 0,
                1.75, 0,
                1.5, -0.25,
                1.5, -0.75,
                1.75, -1,
                2.5, -1);
            d.AddPoints(
                3, 0, 
                3.75, 0,
                4, -0.5,
                3.75, -1,
                3, -1,
                3, 0);
            polylineCollection = new PolylineCollection(j,c,d);
        }

        private void glControl1_Load(object sender, EventArgs e)
        {
            controlLoaded = true;
            GL.ClearColor(Color.White);

            Utilities.SetWindow(-0.5, 4.5, -1.5, 0.5);

            SetViewport();
        }

        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if (!controlLoaded)
                return;
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Color3(Color.Black);
            polylineCollection.Draw(PrimitiveType.LineStrip);
            
            
            plotArea.SwapBuffers();
        }

        private void glControl1_Resize(object sender, EventArgs e)
        {
            if (!controlLoaded)
                return;
            SetViewport();

        }
    }
}
