﻿using System.Collections.Generic;
using csci342;
using System.Drawing;
using MyGraphicsLibrary;
using OpenTK.Graphics.ES11;
using GL = OpenTK.Graphics.OpenGL.GL;
using PrimitiveType = OpenTK.Graphics.OpenGL.PrimitiveType;

namespace mygraphicslib
{
    public interface ICurve
    {
        Point3D GetAt(double t);

        void Draw();
        void DrawTo(double tMax, double pointRadius);
        void DrawConstruction(double tMax, double pointRadius);
    }

    public class BezierCurve : ICurve
    {
        private IList<Point3D> ControlPoints { get; }

        public BezierCurve(IEnumerable<Point3D> controlPoints)
        {
            ControlPoints = new List<Point3D>(controlPoints);
        }

        private static Point3D GetAt(double t, Point3D p0, Point3D p1)
        {
            return new Point3D(
                p0.X + (p1.X - p0.X) * t,
                p0.Y + (p1.Y - p0.Y) * t,
                p0.Z + (p1.Z - p0.Z) * t
                );
        }

        private static Point3D GetAt<T>(double t, int start, int count, IList<T> points) where T : Point3D
        {
            if (count == 2)
            {
                return GetAt(t, points[start], points[start + 1]);
            }
            return GetAt(t,
                GetAt(t, start, count - 1, points),
                GetAt(t, start + 1, count - 1, points));
        }

        public static Point3D GetAt<T>(double t, IList<T> controlPoints) where T : Point3D
        {
            return GetAt(t, 0, controlPoints.Count, controlPoints);
        }

        public Point3D GetAt(double t)
        {
            return GetAt(t, 0, ControlPoints.Count, ControlPoints);
        }

        private void DrawTo(double tMax, double pointRadius, bool drawEndMarker)
        {
            GL.Begin(PrimitiveType.LineStrip);
            {
                for (double t = 0; t <= tMax; t += 0.01)
                {
                    var point = GetAt(t);
                    GL.Vertex2(point.X, point.Y);
                }
            }
            GL.End();
            if (drawEndMarker)
            {
                
            }
        }

        public void Draw()
        {
            DrawTo(1, 0, false);
        }

        public void DrawTo(double tMax, double pointRadius)
        {
            DrawTo(tMax, pointRadius, true);
            var point = GetAt(tMax);
            Utilities.DrawArc(point.X, point.Y, pointRadius, 0, 360, true);
        }

        private static void DrawConstruction(double tMax, Stack<Color> colors, IList<Point3D> points, double pointRadius)
        {
            while (points.Count >= 2)
            {
                GL.Color3(colors.Peek());
                GL.Begin(PrimitiveType.LineStrip);
                {
                    foreach (var point in points)
                    {
                        GL.Vertex2(point.X, point.Y);
                    }
                }
                GL.End();

                foreach (var point in points)
                {
                    Utilities.DrawArc(point.X, point.Y, pointRadius, 0, 360, true);
                }

                if (colors.Count > 1)
                {
                    colors.Pop();
                }
                var newPoints = new List<Point3D>();
                for (int i = 0; i < points.Count - 1; i++)
                {
                    newPoints.Add(GetAt(tMax, points[i], points[i + 1]));
                }
                points = newPoints;

            }
        }

        public void DrawConstruction(double tMax, double pointRadius)
        {
            var colors = new Stack<Color>(new[]
            {
                Color.Green,
                Color.Blue,
                Color.Black,
            });
            DrawConstruction(tMax, colors, ControlPoints, pointRadius);
        }

    }
}