﻿using System;
using csci342;
using OpenTK.Graphics.OpenGL;

namespace MyGraphicsLibrary
{
    public static class GLUExtensions
    {
        public static void DrawCube(this GLU glu, bool solid = false)
        {
            var vertexType = solid ? PrimitiveType.Polygon : PrimitiveType.LineLoop;

            double p = 0.5;
            double n = -0.5;
            // Z = -1
            GL.Begin(vertexType);
            {
                GL.Vertex3(n, n, n);
                GL.Vertex3(n, p, n);
                GL.Vertex3(p, p, n);
                GL.Vertex3(p, n, n);
            }
            GL.End();
            // Z = 1
            GL.Begin(vertexType);
            {
                GL.Vertex3(n, n, p);
                GL.Vertex3(n, p, p);
                GL.Vertex3(p, p, p);
                GL.Vertex3(p, n, p);
            }
            GL.End();
            // X = -1
            GL.Begin(vertexType);
            {
                GL.Vertex3(n, n, n);
                GL.Vertex3(n, n, p);
                GL.Vertex3(n, p, p);
                GL.Vertex3(n, p, n);
            }
            GL.End();
            // X = 1
            GL.Begin(vertexType);
            {
                GL.Vertex3(p, n, n);
                GL.Vertex3(p, n, p);
                GL.Vertex3(p, p, p);
                GL.Vertex3(p, p, n);
            }
            GL.End();
            // Y = -1
            GL.Begin(vertexType);
            {
                GL.Vertex3(n, n, n);
                GL.Vertex3(n, n, p);
                GL.Vertex3(p, n, p);
                GL.Vertex3(p, n, n);
            }
            GL.End();
            // Y = 1
            GL.Begin(vertexType);
            {
                GL.Vertex3(n, p, n);
                GL.Vertex3(n, p, p);
                GL.Vertex3(p, p, p);
                GL.Vertex3(p, p, n);
            }
            GL.End();


        }

        private static void SolidHemisphere(int verticalDivisions, int horizontalDivisions)
        {
            //Draw top
            GL.Begin(PrimitiveType.TriangleFan);
            {
                GL.Normal3(0, 1, 0);
                GL.Vertex3(0, 0.5, 0);
                double y = 0.5 - 0.5 / verticalDivisions;
                double r = Math.Sqrt(0.5 * 0.5 - y * y);
                for (int i = 0; i <= horizontalDivisions; i++)
                {
                    double x = r * Math.Cos(i * 2 * Math.PI / horizontalDivisions);
                    double z = r * Math.Sin(i * 2 * Math.PI / horizontalDivisions);
                    GL.Normal3(x, y, z);
                    GL.Vertex3(x, y, z);
                }
            }
            GL.End();
            //Draw remaining strips
            for (int i = 1; i < verticalDivisions; i++)
            {
                //Draw a strip 
                GL.Begin(PrimitiveType.QuadStrip);
                {
                    double y1 = 0.5 - i * 0.5 / verticalDivisions;
                    double y2 = 0.5 - (i + 1) * 0.5 / verticalDivisions;
                    double r1 = Math.Sqrt(0.5 * 0.5 - y1 * y1);
                    double r2 = Math.Sqrt(0.5 * 0.5 - y2 * y2);
                    for (int j = 0; j <= horizontalDivisions; j++)
                    {
                        double angle = j * 2 * Math.PI / horizontalDivisions;
                        double x1 = r1 * Math.Cos(angle);
                        double x2 = r2 * Math.Cos(angle);
                        double z1 = r1 * Math.Sin(angle);
                        double z2 = r2 * Math.Sin(angle);
                        GL.Normal3(x1, y1, z1);
                        GL.Vertex3(x1, y1, z1);
                        GL.Normal3(x2, y2, z2);
                        GL.Vertex3(x2, y2, z2);
                    }
                }
                GL.End();
            }
        }

        public static void SolidSphere(this GLU glu, double radius, int verticalDivisions, int horizontalDivisions)
        {

            GL.PushMatrix();
            {
                GL.Scale(radius, radius, radius);
                GL.Enable(EnableCap.Normalize);
                //Do actual drawing here
                verticalDivisions = verticalDivisions / 2;
                SolidHemisphere(verticalDivisions, horizontalDivisions);
                GL.Scale(1, -1, 1);
                SolidHemisphere(verticalDivisions, horizontalDivisions);
            }
            GL.PopMatrix();


        }

        public static void WireSphere(this GLU glu, double radius, int verticalDivisions, int horizontalDivisions)
        {
            GL.PushMatrix();
            {
                GL.Enable(EnableCap.Normalize);
                GL.Scale(radius, radius, radius);


                double dy = 1.0 / verticalDivisions;
                //Draw vertical slices
                for (int i = 1; i < verticalDivisions; i++)
                {
                    GL.Begin(PrimitiveType.LineLoop);
                    {
                        double y = 0.5 - i * dy;
                        double r = Math.Sqrt(0.5 * 0.5 - y * y);
                        for (int j = 0; j < horizontalDivisions; j++)
                        {
                            double angle = j * 2 * Math.PI / horizontalDivisions;
                            double x = r * Math.Cos(angle);
                            double z = r * Math.Sin(angle);
                            GL.Normal3(x, y, z);
                            GL.Vertex3(x, y, z);
                        }
                    }
                    GL.End();
                }
                //Draw turning slices
                for (int j = 0; j < horizontalDivisions; j++)
                {
                    GL.Begin(PrimitiveType.LineStrip);
                    {
                        GL.Normal3(0, 0.5, 0);
                        GL.Vertex3(0, 0.5, 0);
                        double angle = j * 2 * Math.PI / horizontalDivisions;
                        double nx = Math.Cos(angle);
                        double nz = Math.Sin(angle);
                        for (int i = 1; i < verticalDivisions; i++)
                        {
                            double y = 0.5 - i * dy;
                            double r = Math.Sqrt(0.5 * 0.5 - y * y);
                            double x = r * nx;
                            double z = r * nz;
                            GL.Normal3(x, y, z);
                            GL.Vertex3(x, y, z);
                        }
                        GL.Normal3(0, -0.5, 0);
                        GL.Vertex3(0, -0.5, 0);
                    }
                    GL.End();
                }

            }
            GL.PopMatrix();
        }

        private static void DrawCircularFace(PrimitiveType pType, double radius, double z, double nr, double nz, int numSlices)
        {
            GL.Begin(pType);
            {
                for (int j = 0; j < numSlices; j++)
                {
                    double angle = j * 2 * Math.PI / numSlices;
                    double nx = Math.Cos(angle);
                    double ny = Math.Sin(angle);
                    double x = radius * nx;
                    double y = radius * ny;
                    GL.Normal3(nx * nr, ny * nr, nz);
                    GL.Vertex3(x, y, z);
                }
            }
            GL.End();
        }

        public static void WireCylinder(this GLU glu, double baseRadius = 1, double topRadius = 1, double height = 1,
            int numSlices = 30, int numStacks = 30)
        {
            for (int j = 0; j < numSlices; j++)
            {
                double angle = j * 2 * Math.PI / numSlices;
                double nx = Math.Cos(angle);
                double ny = Math.Sin(angle);
                GL.Begin(PrimitiveType.LineStrip);
                {
                    for (int i = 0; i <= numStacks; i++)
                    {
                        double z = i * height / numStacks;
                        double r = baseRadius + i * (topRadius - baseRadius) / numStacks;
                        double x = r * nx;
                        double y = r * ny;
                        GL.Normal3(-1 * nx * height, -1 * ny * height, baseRadius - topRadius);
                        GL.Vertex3(x, y, z);
                    }
                }
                GL.End();
            }
            for (int i = 0; i <= numStacks; i++)
            {
                double z = i * height / numStacks;
                double r = baseRadius + i * (topRadius - baseRadius) / numStacks;
                DrawCircularFace(PrimitiveType.LineStrip, r, z, -1 * height, baseRadius - topRadius, numSlices);
            }
        }

        public static void SolidCylinder(this GLU glu, double baseRadius = 1, double topRadius = 1, double height = 1,
            int numSlices = 30, int numStacks = 30)
        {
            DrawCircularFace(PrimitiveType.Polygon, baseRadius, 0, 0, 1, numSlices);
            for (int j = 0; j < numSlices; j++)
            {
                double angle = j * 2 * Math.PI / numSlices;
                double nx = Math.Cos(angle);
                double ny = Math.Sin(angle);
                GL.Begin(PrimitiveType.QuadStrip);
                {
                    for (int i = 0; i < numStacks; i++)
                    {
                        double z1 = i * height / numStacks;
                        double z2 = (i + 1) * height / numStacks;
                        double r1 = baseRadius + i * (topRadius - baseRadius) / numStacks;
                        double r2 = baseRadius + (i + 1) * (topRadius - baseRadius) / numStacks;
                        double x1 = r1 * nx;
                        double x2 = r2 * ny;
                        double y1 = r1 * ny;
                        double y2 = r2 * ny;
                        GL.Normal3(-1 * nx * height, -1 * ny * height, baseRadius - topRadius);
                        GL.Vertex3(x1, y1, z1);
                        GL.Vertex3(x2, y2, z2);

                    }
                }
                GL.End();
            }
            for (int i = 0; i <= numStacks; i++)
            {
                double z = i * height / numStacks;
                double r = baseRadius + i * (topRadius - baseRadius) / numStacks;
                GL.Begin(PrimitiveType.LineLoop);
                {
                    for (int j = 0; j < numSlices; j++)
                    {
                        double angle = j * 2 * Math.PI / numSlices;
                        double nx = Math.Cos(angle);
                        double ny = Math.Sin(angle);
                        double x = r * nx;
                        double y = r * ny;
                        GL.Normal3(-1 * nx * height, -1 * ny * height, baseRadius - topRadius);
                        GL.Vertex3(x, y, z);
                    }
                }
                GL.End();
            }
        }
    }
}