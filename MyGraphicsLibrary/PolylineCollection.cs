﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;

namespace MyGraphicsLibrary
{
    public class PolylineCollection : IEnumerable<Polyline>
    {
        private readonly IList<Polyline> polylines = new List<Polyline>();

        public PolylineCollection()
        {
            
        }

        public PolylineCollection(params Polyline[] plines) : this()
        {
            foreach (Polyline polyline in plines)
            {
                polylines.Add(polyline);
            }
        }

        public IEnumerator<Polyline> GetEnumerator()
        {
            return polylines.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) polylines).GetEnumerator();
        }

        public void AddPolyline(Polyline polyline)
        {
            polylines.Add(polyline);
        }

        public void Draw(PrimitiveType type)
        {
            foreach (Polyline polyline in polylines)
            {
                polyline.Draw(type);
            }
        }

        public Polyline LastLine
        {
            get
            {
                if (polylines.Count > 0)
                {
                    return polylines[polylines.Count - 1];
                }
                return null;
            }
        } 
    }
}
