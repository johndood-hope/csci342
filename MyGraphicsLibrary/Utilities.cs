﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using csci342;
using OpenTK.Graphics.OpenGL;

namespace MyGraphicsLibrary
{
    public class Utilities
    {
        public static void SetWindow(double left, double right, double bottom, double top)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(left, right, bottom, top, -1, 1);
        }

        public static void PlaceVertex(double r, double degrees, double cx = 0, double cy = 0)
        {
            GL.Vertex2(r * Math.Cos(D2R(degrees)) + cx, r * Math.Sin(D2R(degrees)) + cy);
        }

        public static void DrawArc(double cx, double cy, double r, double startDegrees, double sweepDegrees, bool filled = false)
        {
            double deltaDegrees = 4;

            PrimitiveType toDraw = filled ? PrimitiveType.Polygon : PrimitiveType.LineStrip;
            GL.Begin(toDraw);
            {
                for (double angle = 0; angle < sweepDegrees; angle += deltaDegrees)
                {
                    GL.Vertex2(r * Math.Cos(D2R(startDegrees + angle)) + cx, r * Math.Sin(D2R(startDegrees + angle)) + cy);
                }
                GL.Vertex2(r * Math.Cos(D2R(startDegrees + sweepDegrees)) + cx, r * Math.Sin(D2R(startDegrees + sweepDegrees)) + cy);
                if (filled)
                    GL.Vertex2(cx, cy);
            }
            GL.End();
        }

        public static void DrawRectangle(double cx, double cy, double w, double h, bool filled = false)
        {
            PrimitiveType toDraw = filled ? PrimitiveType.Polygon : PrimitiveType.LineStrip;
            GL.Begin(toDraw);
            {
                GL.Vertex2(cx - w / 2, cy + h / 2);
                GL.Vertex2(cx + w / 2, cy + h / 2);
                GL.Vertex2(cx + w / 2, cy - h / 2);
                GL.Vertex2(cx - w / 2, cy - h / 2);
                GL.Vertex2(cx - w / 2, cy + h / 2);
            }
            GL.End();
        }

        public static double D2R(double degrees)
        {
            return (float)(Math.PI / 180) * degrees;
        }



        public static void DrawRoundedRectangle(double cx, double cy, double w, double h, double g, bool filled = false)
        {
            double r = g * w;
            if (filled)
            {
                DrawRectangle(cx, cy, w - 2 * r, h, true);
                DrawRectangle(cx, cy, w, h - 2 * r, true);
            }
            else
            {
                GL.Begin(PrimitiveType.Lines);
                {
                    GL.Vertex2(cx - (w / 2 - r), cy + h / 2);
                    GL.Vertex2(cx + (w / 2 - r), cy + h / 2);
                    GL.Vertex2(cx + w / 2, cy + (h / 2 - r));
                    GL.Vertex2(cx + w / 2, cy - (h / 2 - r));
                    GL.Vertex2(cx - (w / 2 - r), cy - h / 2);
                    GL.Vertex2(cx + (w / 2 - r), cy - h / 2);
                    GL.Vertex2(cx - w / 2, cy + (h / 2 - r));
                    GL.Vertex2(cx - w / 2, cy - (h / 2 - r));

                }
                GL.End();
            }
            DrawArc(cx + (w / 2 - r), cy + (h / 2 - r), r, 0, 90, filled);
            DrawArc(cx + (w / 2 - r), cy - (h / 2 - r), r, -90, 90, filled);
            DrawArc(cx - (w / 2 - r), cy - (h / 2 - r), r, 180, 90, filled);
            DrawArc(cx - (w / 2 - r), cy + (h / 2 - r), r, 90, 90, filled);

        }
    }
}
