﻿using System;
using System.Collections;
using System.Collections.Generic;
using csci342;
using OpenTK.Graphics.OpenGL;

namespace MyGraphicsLibrary
{
    public class Polyline : IEnumerable<Point2D>
    {
        private readonly IList<Point2D> points = new List<Point2D>();

        public Polyline()
        {
            
        }

        public int Count
        {
            get { return points.Count; }
        }

        public Polyline(Point2D point) : this()
        {
            points.Add(point);
        }

        public void AddPoint(Point2D point)
        {
            points.Add(point);
        }

        public void AddPoint(double x, double y)
        {
            AddPoint(new Point2D(x, y));
        }

        public void AddPoints(params double[] vals)
        {
            if(vals.Length % 2 != 0)
                throw new ArgumentException("Odd number of values");
            for (int i = 0; i < vals.Length/2; i++)
            {
                AddPoint(new Point2D(vals[2*i], vals[2*i+1]));
            }
        }

        public IEnumerator<Point2D> GetEnumerator()
        {
            return points.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) points).GetEnumerator();
        }

        public void Draw(PrimitiveType type)
        {
            GL.Begin(type);
            {
                foreach (Point2D point in points)
                {
                   GL.Vertex2(point.X, point.Y); 
                }
            }
            GL.End();
        }

        public Point2D FirstPoint => points.Count > 0 ? points[0] : null;

        public Point2D LastPoint => points.Count > 0 ? points[points.Count - 1] : null;

        public void RemoveLast()
        {
            if (points.Count > 0)
            {
                points.RemoveAt(points.Count-1);
            }
        }
    }
}